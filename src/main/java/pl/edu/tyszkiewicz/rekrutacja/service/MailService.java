package pl.edu.tyszkiewicz.rekrutacja.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import pl.edu.tyszkiewicz.rekrutacja.Messages;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;

import java.util.UUID;

@Slf4j
@Service
public class MailService {

    @Value("${admission.mail.enabled}")
    Boolean mailEnabled;

    @Value("${admission.mail.from}")
    String from;
    @Value("${admission.mail.admin}")
    String adminMail;

    @Autowired
    MailSender mailSender;


    private void sendMail(String to, String subject, String body, Object... args) {
        if (mailEnabled){
            try {
                SimpleMailMessage message = new SimpleMailMessage();
                message.setTo(to);
                message.setFrom(from);
                message.setSubject(subject);
                message.setText(String.format(body, args));
                mailSender.send(message);
            } catch (Exception e) {
                log.error("Unable to send email !", e);
                log.error("Cannot send email to {} with subject {}", to, subject);
            }
        }else {
            log.warn("Mail Service is being disabled make sure to turn it on on production");
        }
    }


    public void notifyUserAboutRegister(String email, String name) {
        sendMail(email, Messages.REGISTER_MAIL_SUBJECT, Messages.REGISTER_MAIL_BODY, name);
    }

    public void notifyUserAboutRequestedPasswordReset(String email, String name, UUID uuid) {
        sendMail(email, Messages.RESET_PASS_MAIL_SUBJECT, Messages.RESET_PASS_MAIL_BODY, name, uuid);
    }

    public void notifyUserAboutPerformedPasswordReset(String email, String name, String generatedPassword) {
        sendMail(email, Messages.RESET_PASS_MAIL_SUBJECT, Messages.RESET_PASS_DONE_MAIL_BODY, name, generatedPassword);
    }
    public void notifyAdminAboutRegister(User userEntity) {
        sendMail(adminMail, from, "Odnotowano rejestracje w systemie rekrutacji", "Użytkownik: " +
                userEntity.getName() +
                userEntity.getSurname() +
                "\nAdres email:" +
                userEntity.getEmail() +
                "\nNumer telefonu:" +
                userEntity.getPhone() +
                "\nKierunek:" +
                userEntity.getSpecialization().getAdmission().getName() +
                "\nSpecjalizacja:" +
                userEntity.getSpecialization().getName() +
                "\n zarejestrował się w systemie.");
    }
    public void notifyAdminAboutPdfPrint(User userEntity) {
        sendMail(adminMail, from,"Odnotowano wydrukowanie formularza", "Użytkownik: " +
                userEntity.getName() +
                userEntity.getSurname() +
                "\n o Adres email:" +
                userEntity.getEmail() +
                "\n o Numer telefonu:" +
                userEntity.getPhone() +
                "\nKierunek:" +
                userEntity.getSpecialization().getAdmission().getName() +
                "\nSpecjalizacja:" +
                userEntity.getSpecialization().getName() +
                "\n wydrukował formularz w formacie pdf.");
    }
}
