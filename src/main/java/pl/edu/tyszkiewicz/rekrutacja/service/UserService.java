package pl.edu.tyszkiewicz.rekrutacja.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pl.edu.tyszkiewicz.rekrutacja.domain.*;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserIsLocked;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserNotFoundException;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserValidationException;
import pl.edu.tyszkiewicz.rekrutacja.repository.SpecializationRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.UserRepository;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import java.io.Serializable;
import java.util.*;

@Slf4j
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    SpecializationRepository specializationRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    public User updateUserInfo(User user, boolean fullValidation, UserStatus status, boolean adminEdits) throws UserNotFoundException, UserValidationException, UserIsLocked {
        User userEntity;

        if (adminEdits) {
            if (user.getId() == null) {
                userEntity = new User();
                userEntity.setCreated(user.getCreated());
            } else {
                userEntity = userRepository.findById(user.getId()).orElseThrow(UserNotFoundException::new);
            }
            if (user.getRole() != null) {
                userEntity.setRole(user.getRole());
            }
            if (user.getPassword() != null) {
                if (!user.getPassword().equals(userEntity.getPassword())) {
                    userEntity.setPassword(passwordEncoder.encode(user.getPassword()));
                }
            }
        } else {
            userEntity = userRepository.findById(user.getId()).orElseThrow(UserNotFoundException::new);
        }

        if (userEntity.getUserExtraInfo() != null && userEntity.getUserExtraInfo().getIsLocked()) {
            if (!adminEdits) {
                throw new UserIsLocked();
            }
        }
        if (user.getSpecialization() != null && !user.getSpecialization().getId()
                .equals(Optional.of(userEntity.getSpecialization())
                        .orElse(Specialization.builder().id(-1L).build())
                        .getId())) {
            userEntity.setSpecialization(
                    specializationRepository.findById(user.getSpecialization().getId())
                            .orElseThrow(UserValidationException::new));
        }
        setBaseInfo(user, userEntity);
        final UserExtraInfo userExtraInfo = user.getUserExtraInfo();
        Set<ConstraintViolation<Serializable>> violations = new HashSet<>();
        if (fullValidation) {
            if (user.getUserExtraInfo() == null
                    || userExtraInfo.getDocument() == null
                    || userExtraInfo.getLivingAddress() == null
                    || userExtraInfo.getHighSchoolInfo() == null
                    || userExtraInfo.getSurvey() == null
                    || userEntity.getSpecialization() == null) {
                throw new UserValidationException();
            }
        }
        String pesel = userExtraInfo.getPesel();
        if (pesel == null || pesel.trim() == "") {
            violations.addAll(validate(user.getUserExtraInfo().getDocument()));
        } else {
            if (pesel.length() != 11)
                throw new UserValidationException(Arrays.asList("userExtraInfo_pesel"));
        }


        violations.addAll(validate(user));

        if (!violations.isEmpty()) {
            throw new UserValidationException(violations);
        }
        String nationality = userExtraInfo.getNationality().trim().toUpperCase();
        if (!(nationality == "PL" || nationality.contains("POLS"))) {
            if (userExtraInfo.getPolishCard() == null) {
                throw new UserValidationException("userExtraInfo_polishCard");
            }
            if (userExtraInfo.getBirthPlace() == null || userExtraInfo.getBirthPlace().trim().length() < 2) {
                throw new UserValidationException("userExtraInfo_birthPlace");
            }
        }

        userEntity.setName(user.getName());
        userEntity.setSurname(user.getSurname());
        userEntity.setEmail(user.getEmail());
        userEntity.setPhone(user.getPhone());
        userEntity.setUserExtraInfo(updateExtraInfo(userExtraInfo, userEntity.getUserExtraInfo(), fullValidation));
        if (userEntity.getUserExtraInfo() != null) {
            userEntity.getUserExtraInfo().setUser(userEntity);
        }
        userEntity.setStatus(status);
        User updatedUser = userRepository.save(userEntity);
        log.info("Successfully updated user: {}", updatedUser.toString());
        return updatedUser;
    }

    private UserExtraInfo updateExtraInfo(UserExtraInfo request, UserExtraInfo entity, boolean fullValidation) {
        if (request == null) {
            return null;
        }
        if (entity == null) {
            entity = new UserExtraInfo();
        }
        entity.setBirthday(request.getBirthday());
        entity.setIsOurStudent(request.getIsOurStudent());
//        entity.setFamilyName(request.getFamilyName());
        entity.setSecondName(request.getSecondName());
//        entity.setFatherName(request.getFatherName());
        entity.setBirthPlace(request.getBirthPlace());
        entity.setPolishCard(request.getPolishCard());
//        entity.setMotherName(request.getMotherName());
        entity.setPromoCode(request.getPromoCode());
        entity.setNationality(request.getNationality());
        entity.setPesel(request.getPesel());
        entity.setSex(request.getSex());
        entity.setIsLocked(Optional.of(request.getIsLocked()).orElse(false));
        if (request.getCorrespondenceAddress() != null) {
            entity.setCorrespondenceAddress(updateAdress(request.getCorrespondenceAddress(), entity.getCorrespondenceAddress()));
        }
        entity.setLivingAddress(updateAdress(request.getLivingAddress(), entity.getLivingAddress()));
        entity.setDocument(updateDocument(request.getDocument(), entity.getDocument()));
        entity.setSurvey(updateSurvey(request.getSurvey(), entity.getSurvey()));
        entity.setHighSchoolInfo(updateHighSchoolInfo(request.getHighSchoolInfo(), entity.getHighSchoolInfo()));
        if (fullValidation) {
            entity.setIsLocked(true);
        }
        return entity;
//        entity.setGenericInfo(request.getGenericInfo());
    }

    private HighSchoolInfo updateHighSchoolInfo(@Valid HighSchoolInfo request, @Valid HighSchoolInfo entity) {
        if (request == null) {
            return null;
        }
        if (entity == null) {
            entity = new HighSchoolInfo();
        }
        entity.setCertificatePlace(request.getCertificatePlace());
        entity.setHighSchoolType(request.getHighSchoolType());
        entity.setHighSchoolName(request.getHighSchoolName());
        entity.setFinishingYear(request.getFinishingYear());
//        entity.setOke(request.getOke());
//        entity.setHighSchoolCertificateCity(request.getHighSchoolCertificateCity());
        entity.setHighSchoolCertificateNumber(request.getHighSchoolCertificateNumber());
//        entity.setGradeAverage(request.getGradeAverage());
        return entity;
    }

    private Survey updateSurvey(Survey request, Survey entity) {
        if (request == null) {
            return null;
        }
        if (entity == null) {
            entity = new Survey();
        }
        entity.setHowInfo(request.getHowInfo());
        entity.setHowInfoExtra(request.getHowInfoExtra());
        entity.setWhyChoose(request.getWhyChoose());
        return entity;
    }

    private Address updateAdress(Address request, Address entity) {
        if (request == null) {
            return null;
        }
        if (entity == null) {
            entity = new Address();
        }
//        entity.setCountry(request.getCountry());
//        entity.setVoivodeship(request.getVoivodeship());
//        entity.setPoviat(request.getPoviat());
//        entity.setCommune(request.getCommune());
        entity.setPostOffice(request.getPostOffice());
        entity.setZipCode(request.getZipCode());
        entity.setCity(request.getCity());
        entity.setStreet(request.getStreet());
        entity.setHouseNumber(request.getHouseNumber());
        entity.setAddressType(request.getAddressType());
        return entity;
    }

    private Document updateDocument(Document request, Document entity) {
        if (request == null) {
            return null;
        }
        if (entity == null) {
            entity = new Document();
        }
        entity.setType(request.getType());
        entity.setNumber(request.getNumber());
        entity.setIssuer(request.getIssuer());
//        entity.setIssued(request.getIssued());
        return entity;
    }

    private void setBaseInfo(User request, User entity) {
        if (request == null) {
            return;
        }
        request.setRole(entity.getRole());
        request.setCreated(entity.getCreated());
        request.setStatus(entity.getStatus());
    }

    public Set<ConstraintViolation<Serializable>> validate(Serializable someEntity) throws UserValidationException {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        return validator.validate(someEntity);
    }
}
