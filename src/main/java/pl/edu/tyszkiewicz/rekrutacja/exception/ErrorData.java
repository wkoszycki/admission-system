package pl.edu.tyszkiewicz.rekrutacja.exception;

import lombok.*;

import java.util.Objects;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorData {
    private String field;

    //Test equal, override equals() and hashCode()
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ErrorData e = (ErrorData) o;
        return  Objects.equals(field, e.field);
    }

    @Override
    public int hashCode() {
        return Objects.hash(field);
    }
}
