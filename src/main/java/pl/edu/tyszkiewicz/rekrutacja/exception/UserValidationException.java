package pl.edu.tyszkiewicz.rekrutacja.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserValidationException extends Exception {

    private ErrorList errors = new ErrorList();

    public UserValidationException(Set<ConstraintViolation<Serializable>> violations) {

        for (ConstraintViolation<Serializable> violation : violations) {
            add(violation.getPropertyPath().toString().replaceAll("\\.", "_"));
        }
    }

    public UserValidationException(Long numberOfUsers) {
        add("number_of_users_" + numberOfUsers);
    }

    public UserValidationException() {
        add("object_nulls");
    }


    public UserValidationException(String field) {
        add(field);
    }
    public UserValidationException(List<String> fields) {
        fields.forEach(f -> add(f));
    }

    void add(String field) {
        errors.add(field);
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public class ErrorList {
        private List<ErrorData> errorData = new ArrayList<>();

        void add(String field) {
            errorData.add(new ErrorData(field));
        }
    }
}
