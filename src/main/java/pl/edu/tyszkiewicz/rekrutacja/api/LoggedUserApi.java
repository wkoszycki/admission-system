package pl.edu.tyszkiewicz.rekrutacja.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;
import pl.edu.tyszkiewicz.rekrutacja.domain.UserStatus;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserIsLocked;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserNotFoundException;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserValidationException;
import pl.edu.tyszkiewicz.rekrutacja.security.common.UserContext;
import pl.edu.tyszkiewicz.rekrutacja.service.MailService;
import pl.edu.tyszkiewicz.rekrutacja.service.UserService;

import java.util.Optional;

@Slf4j
@RestController
public class LoggedUserApi {


    @Autowired
    UserService userService;

    @Autowired
    MailService mailService;

    @GetMapping("/user-api/test")
    public String echo() throws UserNotFoundException {
        return "OK";
    }

    @PutMapping("/user/edit")
    public ResponseEntity editUser(@RequestBody User user) throws UserNotFoundException, UserIsLocked {
        log.info("edit user with email" + user.getEmail());
        final Optional<UserContext> loggedUser = getLoggedUser();
        if (!loggedUser.isPresent()){
            throw new SecurityException("Api invoked by anonymous user!");
        }
        user.setId(loggedUser.get().getId());
        try {
            final User updateUserInfo = userService.updateUserInfo(user, false, UserStatus.ZAREJESTROWANY,false);
            log.info("Successfully edited user: {}", updateUserInfo.toString());
        } catch (UserValidationException v) {
            return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(v.getErrors());
        }
        return ResponseEntity.status(HttpStatus.OK)
                .build();
    }
    @PutMapping("/user/print")
    public ResponseEntity printPdf(@RequestBody User user) throws UserNotFoundException, UserIsLocked {
        log.info("print pdf user" + user.getName());
        final Optional<UserContext> loggedUser = getLoggedUser();
        if (!loggedUser.isPresent()){
            throw new SecurityException("Api invoked by anonymous user!");
        }
        user.setId(loggedUser.get().getId());
        try {
            final User updateUserInfo = userService.updateUserInfo(user, true, UserStatus.KWESTIONARIUSZ_WYPELNIONY,false);
            log.info("User has printed pdf: {}", updateUserInfo.toString());
            mailService.notifyAdminAboutPdfPrint(updateUserInfo);
        } catch (UserValidationException v) {
            return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(v.getErrors());
        }

        return ResponseEntity.status(HttpStatus.OK)
                .build();
    }

    Optional<UserContext> getLoggedUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof String) {
            log.warn("anonymous user invokes user-api");
            return Optional.empty();
        } else {
            // for the case authenticated user authentication
            UserContext userContext = (UserContext) principal;
            log.info("Logged user: {} with id: invokes api", userContext.getId(), userContext.getUsername());
            // do some thing
            return Optional.of(userContext);
        }
    }
}
