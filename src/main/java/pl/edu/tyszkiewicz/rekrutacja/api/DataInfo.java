package pl.edu.tyszkiewicz.rekrutacja.api;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataInfo {
    String type;
    String message;
    String fieldName;
}
