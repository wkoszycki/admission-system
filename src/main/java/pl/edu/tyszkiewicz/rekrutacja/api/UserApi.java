package pl.edu.tyszkiewicz.rekrutacja.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import pl.edu.tyszkiewicz.rekrutacja.conventers.DtoConverter;
import pl.edu.tyszkiewicz.rekrutacja.domain.ResetPassword;
import pl.edu.tyszkiewicz.rekrutacja.domain.Role;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;
import pl.edu.tyszkiewicz.rekrutacja.domain.UserStatus;
import pl.edu.tyszkiewicz.rekrutacja.dto.ResetPasswordDTO;
import pl.edu.tyszkiewicz.rekrutacja.dto.UserDTO;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserNotFoundException;
import pl.edu.tyszkiewicz.rekrutacja.repository.ResetPasswordRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.UserRepository;
import pl.edu.tyszkiewicz.rekrutacja.service.MailService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
public class UserApi {

    @Autowired
    MailService mailService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ResetPasswordRepository resetPasswordRepository;

    @Autowired
    DtoConverter dtoConverter;

    @Autowired
    PasswordEncoder passwordEncoder;


    @PostMapping("/user-api/reset-password")
    public String resetPassword(@RequestBody @Valid ResetPasswordDTO resetPasswordDTO) throws UserNotFoundException {
        Optional<User> user = userRepository.findByEmailAndPhoneNotInStatus(resetPasswordDTO.getResetEmail().toLowerCase(), resetPasswordDTO.getResetPhone(),UserStatus.ARCHIWALNY);
        if (!user.isPresent()) {
            throw new UserNotFoundException();
        } else {
            final User userEntity = user.get();
            resetPasswordRepository.deleteAllByUserId(userEntity.getId());
            ResetPassword resetPassword = new ResetPassword(userEntity);
            resetPasswordRepository.save(resetPassword);
            mailService.notifyUserAboutRequestedPasswordReset(userEntity.getEmail(),userEntity.getName(),resetPassword.getUuid());
        }
        return "OK";
    }

    @GetMapping("/user-api/reset-password/{uuid}")
    public void resetPassword(@PathVariable("uuid") UUID uuid, HttpServletResponse response) throws IOException {
        Optional<ResetPassword> info = resetPasswordRepository.findByUuid(uuid);
        if (info.isPresent() && info.get().getRequested().plusDays(1).isAfter(LocalDateTime.now())) {
            User user = info.get().getUser();
            String generatedPassword = UUID.randomUUID().toString();
            user.setPassword(passwordEncoder.encode(generatedPassword));
            userRepository.save(user);
            mailService.notifyUserAboutPerformedPasswordReset(user.getEmail(), user.getName(), generatedPassword);
            response.sendRedirect("/index.html?reset-password");
        } else {
            response.sendRedirect("/index.html?reset-password-failed");
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/user-api/register")
    public void registerUser(@RequestBody @Valid UserDTO user) {
        user.setRole(Role.USER);
        User userEntity = dtoConverter.convert(user);
        userEntity.setStatus(UserStatus.ZAREJESTROWANY);
        userEntity.setCreated(LocalDate.now());
        userRepository.save(userEntity);
        log.info("Successfully registered user: {}", userEntity.toString());
        mailService.notifyUserAboutRegister(userEntity.getEmail(), userEntity.getName());
        mailService.notifyAdminAboutRegister(userEntity);

    }

}
