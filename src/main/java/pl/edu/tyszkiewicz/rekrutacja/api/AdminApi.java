package pl.edu.tyszkiewicz.rekrutacja.api;

import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.tyszkiewicz.rekrutacja.conventers.DtoConverter;
import pl.edu.tyszkiewicz.rekrutacja.domain.*;
import pl.edu.tyszkiewicz.rekrutacja.dto.*;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserIsLocked;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserNotFoundException;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserValidationException;
import pl.edu.tyszkiewicz.rekrutacja.repository.AdmissionRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.SpecializationRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.UserRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.query.MyUserPredicatesBuilder;
import pl.edu.tyszkiewicz.rekrutacja.repository.query.SearchCriteria;
import pl.edu.tyszkiewicz.rekrutacja.service.UserService;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.*;

@Slf4j
@RestController
public class AdminApi {

    @Autowired
    UserService userService;
    @Autowired
    DtoConverter dtoConverter;

    @Autowired
    UserRepository userRepository;
    @Autowired
    SpecializationRepository specializationRepository;

    @Autowired
    AdmissionRepository admissionRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/admin/search")
    @ResponseBody
    public Iterable<User> search(@RequestBody SearchDTO search) {
        MyUserPredicatesBuilder builder = new MyUserPredicatesBuilder();
        for (SearchCriteria searchCriteria : search.getCriteria()) {
            Object value = searchCriteria.getValue();
            if (valueValid(value)) {
                builder.with(searchCriteria);
            }
        }
        final BooleanExpression booleanExpression = builder.build();
        if (booleanExpression == null) {
            log.info("SearchQuery: empty");
        } else {
            log.info("SearchQuery: {}", booleanExpression.toString());
        }

        return userRepository.findAll(booleanExpression, Sort.by("surname").ascending());
    }

    @PostMapping("/admin/count")
    @ResponseBody
    public long count(@RequestBody SearchDTO search) {
        MyUserPredicatesBuilder builder = new MyUserPredicatesBuilder();
        for (SearchCriteria searchCriteria : search.getCriteria()) {
            Object value = searchCriteria.getValue();
            if (valueValid(value)) {
                builder.with(searchCriteria);
            }
        }
        final BooleanExpression booleanExpression = builder.build();
        if (booleanExpression == null) {
            log.info("SearchQuery: empty");
        } else {
            log.info("SearchQuery: {}", booleanExpression.toString());
        }

        return userRepository.count(booleanExpression);
    }

    @Cacheable("countSpecializationsVsStudents")
    @GetMapping("/admin/countSpecializationsVsStudents/{dateStart}/{dateStop}")
    @ResponseBody
    public List<SpecializationsVsStudentsDTO> countSpecializationsVsStudents(
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            @PathVariable("dateStart")
                    LocalDate dateStart,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            @PathVariable("dateStop")
                    LocalDate dateStop) {
        MyUserPredicatesBuilder builder = new MyUserPredicatesBuilder().
                with("created", "date", ">", dateStart).
                with("created", "date", "<", dateStop).
                with("specialization", "object", "not_null", dateStop).
                with("role", "enum_Role", "=", Role.USER);
        // TODO: 31/08/2018 Czy jest potrzebny konkretny status np. Przyjety?
        final Iterable<User> users = userRepository.findAll(builder
                .build());

        Map<SpecMonth, Integer> aggregatedResult = new HashMap<>();
        for (User u : users) {
            SpecMonth key = new SpecMonth(u.getSpecialization().getId(), u.getSpecialization().getName() +" "+u.getSpecialization().getAdmission().getType(), YearMonth.from(u.getCreated()));
            aggregatedResult.put(key, aggregatedResult.getOrDefault(key, 0) + 1);
        }
        List<SpecializationsVsStudentsDTO> result = new ArrayList<>(aggregatedResult.size());
        aggregatedResult.forEach((specMonth, count) -> {
            result.add(new SpecializationsVsStudentsDTO(specMonth.specId, specMonth.specName, specMonth.yearMonth, count))
            ;
        });
        return result;
    }
    @Cacheable("countSurveyResults")
    @GetMapping("/admin/countSurveyResults/{dateStart}/{dateStop}")
    @ResponseBody
    public SurveryResultDTO countSurveyResults(
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            @PathVariable("dateStart")
                    LocalDate dateStart,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            @PathVariable("dateStop")
                    LocalDate dateStop) {
        MyUserPredicatesBuilder builder = new MyUserPredicatesBuilder().
                with("created", "date", ">", dateStart).
                with("created", "date", "<", dateStop).
                with("role", "enum_Role", "=", Role.USER);
        final Iterable<User> users = userRepository.findAll(builder
                .build());

        final SurveryResultDTO surveryResultDTO = new SurveryResultDTO();
        for (User u : users) {
            if (u.getUserExtraInfo() != null && u.getUserExtraInfo().getSurvey() != null) {
                final Survey survey = u.getUserExtraInfo().getSurvey();
                if (survey.getHowInfo() != null) {
                    surveryResultDTO.addHowInfo(survey.getHowInfo() - 1);
                }
                if (survey.getWhyChoose() != null) {
                    surveryResultDTO.addWhyChoose(survey.getWhyChoose() - 1);
                }
                if (survey.getHowInfoExtra() != null) {
                    surveryResultDTO.addHowInfoExtra(survey.getHowInfoExtra() - 1);
                }
            }
        }
        return surveryResultDTO;
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/admin/updateBulk")
    @ResponseBody
    public Iterable<User> updateBulk(@RequestBody @Valid BulkUpdateDTO bulk) {
        log.info("running bulk update operation: {}", bulk);
        List<User> usersToUpdate = new ArrayList<>();
        for (Long id : bulk.getIds()) {
            User u = userRepository.findById(id).orElseThrow(IllegalArgumentException::new);
            u.setStatus(bulk.getStatus());
            usersToUpdate.add(u);
        }
        final Iterable<User> saveAll = userRepository.saveAll(usersToUpdate);
        log.info("Successfully updated users for bulk operation");
        return saveAll;
    }

    @GetMapping("/admin/archive/{id}")
    public void archive(@PathVariable("id") Long id) throws UserNotFoundException {
        if (id != null) {
            final Optional<User> byId = userRepository.findById(id);
            if (!byId.isPresent()) {
                throw new UserNotFoundException();
            }
            final User user = byId.get();
            user.setStatus(UserStatus.ARCHIWALNY);
            userRepository.save(user);
        } else {
            throw new IllegalArgumentException("Id cannot be null");
        }
    }

    @PutMapping("/admin/edit")
    public ResponseEntity editUser(@RequestBody User user) throws UserNotFoundException, UserIsLocked {

        try {
            if (user.getStatus() == null) {
                user.setStatus(UserStatus.ZAREJESTROWANY);
            }
            final User updateUserInfo = userService.updateUserInfo(user, false, user.getStatus(), true);
            log.info("Successfully edited user: {}", updateUserInfo.toString());
            return ResponseEntity.status(HttpStatus.OK)
                    .body(updateUserInfo);
        } catch (UserValidationException v) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(v.getErrors());
        }
    }

    @PostMapping("/admin/addAdmission")
    public ResponseEntity addAdmission(@RequestBody AdmissionDTO admission) throws UserNotFoundException, UserIsLocked {

        try {
            if (admission.getSpecializations() == null || admission.getSpecializations().isEmpty()) {
                throw new UserValidationException();
            }
            for (SpecializationDTO specializationDTO : admission.getSpecializations()) {
                if (specializationDTO.getSpecializationStatus() != null && specializationDTO.getSpecializationStatus().equals(SpecializationStatus.ARCHIWALNY.toString())) {
                    Long userNumber = userRepository.countAllBySpecializationId(specializationDTO.getId(),UserStatus.ARCHIWALNY);
                    if(userNumber > 0){
                        throw new UserValidationException(userNumber);
                    }
                }
            }
            if (admission.getSpecializations() == null || admission.getSpecializations().isEmpty()) {
                throw new UserValidationException();
            }
            final Set<ConstraintViolation<Serializable>> validate = validate(admission);
            if (!validate.isEmpty()) {
                throw new UserValidationException(validate);
            }
            final Admission adEntity = admissionRepository.save(dtoConverter.convert(admission));
            for (Specialization specialization : adEntity.getSpecializations()) {
                specialization.setAdmission(adEntity);
            }
            final Admission updated = admissionRepository.save(adEntity);

            log.info("Successfully saved admission: {}", updated);
        } catch (UserValidationException v) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(v.getErrors());
        }
        return ResponseEntity.status(HttpStatus.OK)
                .build();
    }


    public Set<ConstraintViolation<Serializable>> validate(Serializable someEntity) throws UserValidationException {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        return validator.validate(someEntity);
    }


    private boolean valueValid(Object value) {
        if (value == null) {
            return false;
        }
        if (value instanceof String) {
            if (((String) value).trim().isEmpty()) {
                return false;
            }
        }
        if (value instanceof ArrayList) {
            if (((ArrayList) value).isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @ToString
    @AllArgsConstructor
    @EqualsAndHashCode
    class SpecMonth {
        Long specId;
        String specName;
        YearMonth yearMonth;
    }
}
