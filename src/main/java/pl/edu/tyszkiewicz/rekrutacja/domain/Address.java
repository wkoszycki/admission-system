package pl.edu.tyszkiewicz.rekrutacja.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Setter
@Getter
public class Address implements Serializable {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;

        Address address = (Address) o;

        return id.equals(address.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
//    @Pattern(regexp = "[A-Z]{3}", message = "Invalid country code")
//    @NotNull
//    private String country;
//    @NotNull
//    @Size(min = 2)
//    private String voivodeship;
//    @NotNull
//    @Size(min = 2)
//    private String poviat;
//    @NotNull
//    @Size(min = 2)
//    private String commune;
    @NotNull
    @Size(min = 2)
    private String postOffice;
    // TODO: 26/04/2018 only if polish nationality
    @NotNull
    @Pattern(regexp = "\\d{2}-\\d{3}")
    private String zipCode;
    @NotNull
    @Size(min = 2)
    private String city;
    private String street;
    @NotNull
    @Size(min = 1)
    private String houseNumber;
    private AddressType addressType;
}
