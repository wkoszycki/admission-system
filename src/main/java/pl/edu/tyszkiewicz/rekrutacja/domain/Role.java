package pl.edu.tyszkiewicz.rekrutacja.domain;

public enum Role {
    ADMIN, USER, SUPER_ADMIN
}
