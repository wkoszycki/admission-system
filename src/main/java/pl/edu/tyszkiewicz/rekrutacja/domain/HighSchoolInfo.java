package pl.edu.tyszkiewicz.rekrutacja.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Setter
@Getter
public class HighSchoolInfo implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Size(min = 2)
    @NotNull
    private String certificatePlace;
    @Size(min = 2)
    @NotNull
    private String highSchoolType;
    @Size(min = 2)
    @NotNull
    private String highSchoolName;
    @Pattern(regexp = "[0-9]{4}")
    @NotNull
    private String finishingYear;
//    @Size(min = 2)
//    @NotNull
//    private String oke;
//    @Size(min = 2)
//    @NotNull
//    private String highSchoolCertificateCity;
    @Size(min = 2)
    @NotNull
    private String highSchoolCertificateNumber;
//    @Size(min = 2)
//    @NotNull
//    private String gradeAverage;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HighSchoolInfo)) return false;

        HighSchoolInfo that = (HighSchoolInfo) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
