package pl.edu.tyszkiewicz.rekrutacja.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "admission")
@Setter
@Getter
public class Specialization implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private String name;
    @Enumerated(EnumType.STRING)
    private SpecializationStatus specializationStatus;
    @ManyToOne(fetch = FetchType.EAGER)
    private Admission admission;

    public Specialization(@NotNull Long id, @NotNull String name, SpecializationStatus specializationStatus) {
        this.id = id;
        this.name = name;
        this.specializationStatus = specializationStatus;
    }

}
