package pl.edu.tyszkiewicz.rekrutacja.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import pl.edu.tyszkiewicz.rekrutacja.DateDeserialize;
import pl.edu.tyszkiewicz.rekrutacja.validators.FormatChecks;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.function.BooleanSupplier;


@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "user")
@Setter
@Getter
public class UserExtraInfo implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    @JsonDeserialize(using = DateDeserialize.class)
    //@JsonFormat(pattern = "yyyy-MM-dd")
    @NotNull
    LocalDate birthday;
//    @Size(min = 2)
//    @NotNull
//    String familyName;
    String secondName;
//    @Size(min = 2)
//    @NotNull
//    String fatherName;
//    @Size(min = 2)
//    @NotNull
    String birthPlace;
    Boolean polishCard = false;
//    @Size(min = 2)
//    @NotNull
//    String motherName;
    @Pattern(groups = FormatChecks.class, regexp = "^$|[A-Z]{6}")
    String promoCode;

    @NotNull
    @Size(min = 2)
    String nationality;// "polskie",
    // TODO: 26/04/2018 if Polish nationality validate
    String pesel;// "33224455668", todo
    @NotNull
    Sex sex;// "nieokreslona",
    @OneToOne(cascade = CascadeType.ALL)
    @Valid
    Address livingAddress;
    @OneToOne(cascade = CascadeType.ALL)
    Document document;
    @OneToOne(cascade = CascadeType.ALL)
    @Valid
    Address correspondenceAddress;
    Boolean isOurStudent = false;// "Nie",
    @OneToMany(cascade = CascadeType.ALL)
    @Valid
    List<KeyValueEntity> genericInfo;
    // TODO: 27/04/2018 when its locked cant be persisted
    Boolean isLocked = false;
    @OneToOne(cascade = CascadeType.ALL)
    @Valid
    Survey survey;
    @OneToOne(cascade = CascadeType.ALL)
    @Valid
    HighSchoolInfo highSchoolInfo;
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserExtraInfo)) return false;

        UserExtraInfo that = (UserExtraInfo) o;

        if (!id.equals(that.id)) return false;
        if (livingAddress != null ? !livingAddress.equals(that.livingAddress) : that.livingAddress != null) return false;
        if (document != null ? !document.equals(that.document) : that.document != null) return false;
        if (survey != null ? !survey.equals(that.survey) : that.survey != null) return false;
        return user.equals(that.user);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (livingAddress != null ? livingAddress.hashCode() : 0);
        result = 31 * result + (document != null ? document.hashCode() : 0);
        result = 31 * result + (survey != null ? survey.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }
}
