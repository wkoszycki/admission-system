package pl.edu.tyszkiewicz.rekrutacja.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import pl.edu.tyszkiewicz.rekrutacja.DateDeserialize;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Setter
@Getter
public class Admission implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private String name;
    @NotNull
    @Enumerated
    private StudyType type;
    @JsonDeserialize(using = DateDeserialize.class)
    @NotNull
    private LocalDate startDate;
    @Valid
    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn
    private List<Specialization> specializations;
    @JsonDeserialize(using = DateDeserialize.class)
    @NotNull
    private LocalDate endDate;
    @JsonDeserialize(using = DateDeserialize.class)
    @NotNull
    private LocalDate inaugurationDate;
    private String academicYear;


    public Admission addSpecialization(Specialization specialization) {
        if (id == null) {
            throw new IllegalArgumentException("Admission needs to be persisted first");
        }
        if (specializations == null) {
            specializations = new ArrayList<>();
        }
        specializations.add(specialization);
        specialization.setAdmission(this);
        return this;
    }
}
