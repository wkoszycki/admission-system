package pl.edu.tyszkiewicz.rekrutacja.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Setter
@Getter
public class ResetPassword implements Serializable{
    // TODO: 25/04/2018 Add Scheduled cleaning job
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private UUID uuid;
    @OneToOne
    private User user;
    //@JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime requested;

    public ResetPassword(User user) {
        this.user = user;
        requested = LocalDateTime.now();
        uuid = UUID.randomUUID();
    }
}
