package pl.edu.tyszkiewicz.rekrutacja.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Setter
@Getter
public class Survey implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Max(7)
    @Min(1)
    private Integer howInfo;
    @Max(5)
    @Min(1)
    private Integer howInfoExtra;
    @Max(7)
    @Min(1)
    private Integer whyChoose;

}
