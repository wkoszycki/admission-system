package pl.edu.tyszkiewicz.rekrutacja.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import pl.edu.tyszkiewicz.rekrutacja.DateDeserialize;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;


@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Setter
@Getter
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Size(min = 2)
    private String name;
    @Size(min = 2, message = "Surname should have atleast 2 characters")
    private String surname;
    @Column
    @Email(message = "Name should have atleast 2 characters")
    private String email;
    private UserStatus status;
    private String password;
    @Pattern(regexp = "[0-9]{9}",message = "Invalid Phone")
    private String phone;
    @NotNull(message = "Role must not be null")
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;
    @OneToOne
    private Specialization specialization;
    private boolean active = true;
    @OneToOne(cascade = CascadeType.ALL)
    @Valid
    private UserExtraInfo userExtraInfo;
    @JsonDeserialize(using = DateDeserialize.class)
    @NotNull(message = "create must not be null")
    private LocalDate created;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        return getId() != null && Objects.equals(getId(), user.getId());
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
