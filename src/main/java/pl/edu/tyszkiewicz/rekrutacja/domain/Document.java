package pl.edu.tyszkiewicz.rekrutacja.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import pl.edu.tyszkiewicz.rekrutacja.DateDeserialize;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Setter
@Getter
public class Document implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    @Enumerated
    private DocumentType type;// "Dowód Osobisty",
    @Size(min = 2)
    @NotNull
    private String number;// "AAA 345678",
    @Size(min = 2)
    @NotNull
    private String issuer;// "Prezydent Kambodży",
//    @Past
//    @NotNull
//    @JsonDeserialize(using = DateDeserialize.class)
//    private LocalDate issued;// "11.12.2011",

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Document)) return false;

        Document document = (Document) o;

        return id.equals(document.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
