package pl.edu.tyszkiewicz.rekrutacja.domain;

public enum UserStatus {
    ZAREJESTROWANY,KWESTIONARIUSZ_WYPELNIONY,ARCHIWALNY,ZWERYFIKOWANY,PRZYJETY
}
