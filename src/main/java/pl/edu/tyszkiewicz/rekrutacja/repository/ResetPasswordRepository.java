package pl.edu.tyszkiewicz.rekrutacja.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.edu.tyszkiewicz.rekrutacja.domain.Admission;
import pl.edu.tyszkiewicz.rekrutacja.domain.ResetPassword;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;

import java.util.Optional;
import java.util.UUID;


@Repository
public interface ResetPasswordRepository extends CrudRepository<ResetPassword, Long> {

    Iterable<ResetPassword> findAllByUserIdOrderByRequestedDesc(Long userId);

    void deleteAllByUserId(Long userId);

    Optional<ResetPassword> findByUuid(UUID uuid);
}