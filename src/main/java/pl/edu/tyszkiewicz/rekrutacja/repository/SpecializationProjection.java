package pl.edu.tyszkiewicz.rekrutacja.repository;

import org.springframework.data.rest.core.config.Projection;
import pl.edu.tyszkiewicz.rekrutacja.domain.Admission;
import pl.edu.tyszkiewicz.rekrutacja.domain.Specialization;

@Projection(name = "inlineData", types=Specialization.class)
public interface SpecializationProjection {

    Long getId();
    String getName();
    Admission getAdmission();
}