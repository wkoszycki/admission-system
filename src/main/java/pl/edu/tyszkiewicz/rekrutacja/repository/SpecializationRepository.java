package pl.edu.tyszkiewicz.rekrutacja.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.edu.tyszkiewicz.rekrutacja.domain.Specialization;


@RepositoryRestResource(excerptProjection = SpecializationProjection.class)
public interface SpecializationRepository extends CrudRepository<Specialization, Long> {

}