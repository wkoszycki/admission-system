package pl.edu.tyszkiewicz.rekrutacja.repository.query;

import lombok.*;

@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SearchCriteria {
    private String key;
    private String type;
    private String operation;
    private Object value;
}