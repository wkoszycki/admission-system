package pl.edu.tyszkiewicz.rekrutacja.repository;

import org.springframework.data.rest.core.config.Projection;
import pl.edu.tyszkiewicz.rekrutacja.domain.Admission;
import pl.edu.tyszkiewicz.rekrutacja.domain.Specialization;
import pl.edu.tyszkiewicz.rekrutacja.domain.StudyType;

import java.time.LocalDate;
import java.util.List;

@Projection(name = "inlineData", types=Admission.class)
public interface AdmissionProjection{

    Long getId();
    String getName();
    StudyType getType();
    LocalDate getStartDate();
    List<Specialization> getSpecializations();
    LocalDate getEndDate();
    LocalDate getInaugurationDate();
    String getAcademicYear();
}