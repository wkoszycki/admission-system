package pl.edu.tyszkiewicz.rekrutacja.repository;

import org.springframework.data.repository.CrudRepository;
import pl.edu.tyszkiewicz.rekrutacja.domain.Address;

interface AddressRepository extends CrudRepository<Address, Long> {
}
