package pl.edu.tyszkiewicz.rekrutacja.repository.query;

import com.querydsl.core.types.dsl.*;
import pl.edu.tyszkiewicz.rekrutacja.DateDeserialize;
import pl.edu.tyszkiewicz.rekrutacja.domain.AddressType;
import pl.edu.tyszkiewicz.rekrutacja.domain.Role;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;
import pl.edu.tyszkiewicz.rekrutacja.domain.UserStatus;

import java.time.LocalDate;
import java.util.ArrayList;

public class MyUserPredicate {

    private SearchCriteria criteria;

    public MyUserPredicate(final SearchCriteria criteria) {
        this.criteria = criteria;
    }

    public BooleanExpression getPredicate() {
        if (criteria.getType() == null) {
            throw new IllegalArgumentException("Invalid search criteria type: null");
        }
        PathBuilder<User> entityPath = new PathBuilder<>(User.class, "user");
        switch (criteria.getType()) {
            case "object":
                SimplePath<Object> objectPath = entityPath.getSimple(criteria.getKey(), Object.class);
                if (criteria.getOperation().equalsIgnoreCase("not_null")) {
                    return objectPath.isNotNull();
                }
                throw new IllegalArgumentException("Unsupported operator:" + criteria.getOperation());
            case "date":
                DatePath<LocalDate> datePath = entityPath.getDate(criteria.getKey(), LocalDate.class);
                LocalDate localDate = DateDeserialize.parseDate(criteria.getValue().toString());
                if (criteria.getOperation().equalsIgnoreCase("<")) {
                    return datePath.loe(localDate);
                }
                if (criteria.getOperation().equalsIgnoreCase(">")) {
                    return datePath.goe(localDate);
                }
                throw new IllegalArgumentException("Unsupported operator:" + criteria.getOperation());
            case "string":
                StringPath path = entityPath.getString(criteria.getKey());
                String value = criteria.getValue().toString();
                if (criteria.getOperation().equalsIgnoreCase(":")) {
                    return path.containsIgnoreCase(value);
                }
                if (criteria.getOperation().equalsIgnoreCase("=")) {
                    return path.equalsIgnoreCase(value);
                }
                throw new IllegalArgumentException("Unsupported operator:" + criteria.getOperation());
            case "long":
                final NumberPath<Long> longPath = entityPath.getNumber(criteria.getKey(), Long.class);
                if (criteria.getOperation().equalsIgnoreCase("in")) {
                    ArrayList ids = (ArrayList) criteria.getValue();
                    Long[] longs = new Long[ids.size()];
                    for (int i = 0; i < ids.size(); i++) {
                        longs[i] = Long.parseLong(ids.get(i).toString());
                    }
                    return longPath.in(longs);
                }
                if (criteria.getOperation().equalsIgnoreCase("=")) {
                    return longPath.eq(Long.parseLong(criteria.getValue().toString()));
                }
                throw new IllegalArgumentException("Unsupported operator:" + criteria.getOperation());
            case "enum_UserStatus":
                final EnumPath<UserStatus> enumPath = entityPath.getEnum(criteria.getKey(), UserStatus.class);
                if (criteria.getOperation().equalsIgnoreCase("=")) {
                    return enumPath.eq(UserStatus.valueOf(criteria.getValue().toString()));
                }
                if (criteria.getOperation().equalsIgnoreCase("!=")) {
                    return enumPath.ne(UserStatus.valueOf(criteria.getValue().toString()));
                }
                throw new IllegalArgumentException("Unsupported operator:" + criteria.getOperation());
            case "enum_Role":
                final EnumPath<Role> roleEnumPath = entityPath.getEnum(criteria.getKey(), Role.class);
                if (criteria.getOperation().equalsIgnoreCase("=")) {
                    return roleEnumPath.eq(Role.valueOf(criteria.getValue().toString()));
                }
                if (criteria.getOperation().equalsIgnoreCase("!=")) {
                    return roleEnumPath.ne(Role.valueOf(criteria.getValue().toString()));
                }
                throw new IllegalArgumentException("Unsupported operator:" + criteria.getOperation());
            case "enum_AddressType":
                final EnumPath<AddressType> addressEnumPath = entityPath.getEnum(criteria.getKey(), AddressType.class);
                if (criteria.getOperation().equalsIgnoreCase("=")) {
                    return addressEnumPath.eq(AddressType.valueOf(criteria.getValue().toString()));
                }
                if (criteria.getOperation().equalsIgnoreCase("!=")) {
                    return addressEnumPath.ne(AddressType.valueOf(criteria.getValue().toString()));
                }
                throw new IllegalArgumentException("Unsupported operator:" + criteria.getOperation());
            default:
                throw new IllegalArgumentException("Invalid search criteria type:" + criteria.getType());
        }
    }

    public void setCriteria(final SearchCriteria criteria) {
        this.criteria = criteria;
    }

}