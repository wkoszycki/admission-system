package pl.edu.tyszkiewicz.rekrutacja.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;
import pl.edu.tyszkiewicz.rekrutacja.domain.UserStatus;

import java.util.Optional;


@RepositoryRestResource(excerptProjection = UserProjection.class)
public interface UserRepository extends CrudRepository<User, Long>, QuerydslPredicateExecutor<User> {

    Iterable<User> findByEmail(String email);

    @Query("select u from User u where u.email = :email AND u.status != :status")
    Optional<User> findByEmailNotInStatus(@Param("email") String email, @Param("status") UserStatus status);

    @Query("select u from User u where u.status != :status")
    Iterable<User> findAllNotInStatus(UserStatus status);

    @Query("select count(u) from User u where u.specialization.id = :specId and u.status != :status")
    Long countAllBySpecializationId(Long specId, UserStatus status);

    @Query("select u from User u where u.email = :email AND u.phone = :phone AND u.status != :status")
    Optional<User> findByEmailAndPhoneNotInStatus(@Param("email") String email, @Param("phone") String phone, @Param("status") UserStatus status);

}