package pl.edu.tyszkiewicz.rekrutacja.repository;

import org.springframework.data.rest.core.config.Projection;
import pl.edu.tyszkiewicz.rekrutacja.domain.*;

import java.time.LocalDate;

@Projection(name = "inlineData", types=User.class)
public interface UserProjection {

    Long getId();
    String getName();
    String getSurname();
    String getEmail();
    UserStatus getStatus();
    String getPassword();
    String getPhone();
    Role getRole();
    Specialization getSpecialization();
    boolean getActive();
    UserExtraInfo getUserExtraInfo();
    LocalDate getCreated();
}