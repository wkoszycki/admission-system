package pl.edu.tyszkiewicz.rekrutacja.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;
import pl.edu.tyszkiewicz.rekrutacja.domain.Admission;

import java.time.LocalDate;
import java.util.Optional;


@RepositoryRestResource(excerptProjection = AdmissionProjection.class)
public interface AdmissionRepository extends CrudRepository<Admission, Long> {

    @Query("select a from Admission a where a.startDate <= :today AND a.endDate >= :today")
    Iterable<Admission> findAllWhereDateInBetween(@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("today") LocalDate today);

    Optional<Admission> findByName(String name);
}