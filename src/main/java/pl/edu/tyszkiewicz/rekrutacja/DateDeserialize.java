package pl.edu.tyszkiewicz.rekrutacja;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateDeserialize extends JsonDeserializer<LocalDate> {

    private  static final ZoneId warsawZone = ZoneId.of("Europe/Warsaw");

    @Override
    public LocalDate deserialize(JsonParser paramJsonParser,
                                 DeserializationContext paramDeserializationContext)
            throws IOException {
// TODO: 30/04/2018 Zone parsing ?
        return parseDate(paramJsonParser.getText().trim());

    }

    public static LocalDate parseDate(String dateToParse){
        if (dateToParse.length() == 10) {
            if (dateToParse.contains("/")){
                return LocalDate.parse(dateToParse, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            }
            return LocalDate.parse(dateToParse, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }
        if (dateToParse.length() >= 23) {
            return ZonedDateTime.parse(dateToParse).withZoneSameInstant(warsawZone).toLocalDate();
        }
        return LocalDate.parse(dateToParse);
    }

//    public static void main(String[] args) {
//        String date = "2019-05-01T22:00:00.000Z";
//        System.out.println(date.length());
//        final ZonedDateTime zonedDateTime = ZonedDateTime.parse(date);
//        System.out.println("ZonedDateTime.parse(date); = " + zonedDateTime);
//
//        System.out.println("zonedDateTime warsaw = " +  zonedDateTime.withZoneSameInstant(warsawZone));
//        System.out.println("ZonedDateTime.parse(date); = " + zonedDateTime.withZoneSameInstant(warsawZone).toLocalDateTime());
//        System.out.println("ZonedDateTime.parse(date); = " + zonedDateTime.withZoneSameLocal(warsawZone).toLocalDateTime());
//        LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
//    }
}