package pl.edu.tyszkiewicz.rekrutacja;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import pl.edu.tyszkiewicz.rekrutacja.domain.*;
import pl.edu.tyszkiewicz.rekrutacja.repository.AdmissionRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.SpecializationRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.UserRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Slf4j
@SpringBootApplication
@ComponentScan(basePackages = {"pl.edu.tyszkiewicz.*"})
@EntityScan("pl.edu.tyszkiewicz.rekrutacja.domain")
@EnableAutoConfiguration
public class AdmissionApplication {



    public static void main(String[] args) {
        SpringApplication.run(AdmissionApplication.class, args);
        log.info("Admission app succesfully started");
    }


    @Bean
    public CommandLineRunner demoData(AdmissionRepository admissionRepository, UserRepository userRepository, SpecializationRepository specializationRepository) {
        return args -> {
            if (!admissionRepository.findAll().iterator().hasNext()) {
                createAdmission("Architektura Wnętrz stacjonarne", StudyType.STACJONARNE, Arrays.asList("Architektura wnętrz"), admissionRepository, specializationRepository);
                createAdmission("Architektura Wnętrz niestacjonarne", StudyType.NIESTACJONARNE, Arrays.asList("Architektura wnętrz"), admissionRepository, specializationRepository);
                createAdmission("Kosmetologia stacjonarne", StudyType.STACJONARNE, Arrays.asList("Wizaż i stylizacja","Trener zdrowego stylu życia","Odnowa biologiczna","Stylizacja paznokci"), admissionRepository, specializationRepository);
                createAdmission("Kosmetologia niestacjonarne", StudyType.NIESTACJONARNE, Arrays.asList("Wizaż i stylizacja","Trener zdrowego stylu życia","Odnowa biologiczna","Stylizacja paznokci"), admissionRepository, specializationRepository);

            }
            final Iterator<Admission> admissionIterator = admissionRepository.findAll().iterator();
            if (!userRepository.findByEmailNotInStatus("koszycki.wojciech@gmail.com", UserStatus.ARCHIWALNY).isPresent()) {
                userRepository.save(User.builder()
                        .email("koszycki.wojciech@gmail.com")
                        .name("Wojciech")
                        .surname("Koszycki")
                        .password("$2a$11$YMMkHOs43bQtzlSNUBKYvuATDwDfi85ze/KI5Ey5.KKk0n8WaCSSq")
                        .phone("786818039")
                        .role(Role.SUPER_ADMIN)
                        .created(LocalDate.now())
                        .status(UserStatus.ZAREJESTROWANY)
                        .active(true)
                        .build());
            }
            if (!userRepository.findByEmailNotInStatus("admin@tyszkiewicz.edu.pl", UserStatus.ARCHIWALNY).isPresent()) {
                userRepository.save(User.builder()
                        .email("admin@tyszkiewicz.edu.pl")
                        .name("Admin")
                        .surname("Admin")
                        .password("$2a$11$w34DSQQXFbTW0VDuUkcbfOLhtbbGl8FVaLp5Nsy2qB.KuBIhNSP8G")
                        .phone("111222333")
                        .role(Role.ADMIN)
                        .created(LocalDate.now())
                        .status(UserStatus.ZAREJESTROWANY)
                        .active(true)
                        .build());
            }

            if (!userRepository.findByEmailNotInStatus("mail@mail.com", UserStatus.ARCHIWALNY).isPresent()) {


                HighSchoolInfo highSchoolInfo = HighSchoolInfo.builder()
                        .certificatePlace("test data")
                        .highSchoolType("test data")
                        .highSchoolName("test data")
                        .finishingYear("2009")
//                        .oke("test data")
//                        .highSchoolCertificateCity("test data")
                        .highSchoolCertificateNumber("test data")
//                        .gradeAverage("test data")
                        .build();

                Address address = Address.builder()
//                        .country("POL")
//                        .voivodeship("test data voivodeship")
//                        .poviat("test data poviat")
//                        .commune("test data commune")
                        .postOffice("test data postOffice")
                        .zipCode("00-000")
                        .city("test data city")
                        .street("test data street")
                        .houseNumber("test data houseNumber")
                        .addressType(AddressType.MIASTO)
                        .build();

                Document document = Document.builder()
                        .type(DocumentType.DOWOD_OSOBISTY)
                        .number("test data number")
                        .issuer("test data issuer")
//                        .issued(LocalDate.of(2000, 9, 2))
                        .build();
                UserExtraInfo userExtraInfo = UserExtraInfo.builder()
                        .birthday(LocalDate.of(1998, 5, 2))
//                        .familyName("family name")
                        .secondName("second")
//                        .fatherName("fatherName")
                        .birthPlace("place of birth")
                        .polishCard(false)
//                        .motherName("Urszula")
                        .promoCode("AAABBB")
                        .nationality("Polskie")
                        .pesel("123881111000")
                        .sex(Sex.MEZCZYZNA)
                        .livingAddress(address)
                        .document(document)
                        .highSchoolInfo(highSchoolInfo)
                        .isOurStudent(false)
                        .isLocked(false)
                        .survey(Survey.builder()
                                .howInfo(2)
                                .howInfoExtra(3)
                                .whyChoose(2)
                                .build())
                        .build();

                User validUser = userRepository.save(User.builder()
                        .email("mail@mail.com")
                        .name("Joe")
                        .surname("Lampart")
//                    raw password is eightLetter8
                        .password("$2a$11$YMMkHOs43bQtzlSNUBKYvuATDwDfi85ze/KI5Ey5.KKk0n8WaCSSq")
                        .phone("111222333")
                        .role(Role.USER)
                        .created(LocalDate.now())
                        .status(UserStatus.ZAREJESTROWANY)
                        .specialization(admissionIterator.next().getSpecializations().get(0))
                        .active(true)
                        .userExtraInfo(userExtraInfo)
                        .build());
                User validUser2 = userRepository.save(User.builder()
                        .email("mail2@mail.com")
                        .name("Joe2")
                        .surname("Lampart")
//                    raw password is eightLetter8
                        .password("$2a$11$YMMkHOs43bQtzlSNUBKYvuATDwDfi85ze/KI5Ey5.KKk0n8WaCSSq")
                        .phone("111222333")
                        .role(Role.USER)
                        .created(LocalDate.now())
                        .status(UserStatus.PRZYJETY)
                        .specialization(admissionIterator.next().getSpecializations().get(0))
                        .active(true)
                        .build());
                userRepository.save(validUser);
                userRepository.save(validUser2);
            }


        };
    }

    Admission createAdmission(String admName, StudyType type, List<String> specs, AdmissionRepository admissionRepository, SpecializationRepository specializationRepository) {
        LocalDate startDate = LocalDate.of(2018, 4, 30);
        LocalDate endDate = LocalDate.of(2018, 10, 5);
        LocalDate inagurationDate = LocalDate.of(2018, 10, 6);
        final Admission adm = admissionRepository.save(Admission.builder()
                .name(admName)
                .startDate(startDate)
                .endDate(endDate)
                .inaugurationDate(inagurationDate)
                .type(type)
                .build());
        List<Specialization> specsEntities = new ArrayList<>();
        for (String specName : specs) {
            specsEntities.add(specializationRepository.save(Specialization.builder()
                    .name(specName)
                    .admission(adm)
                    .build()));
        }
        adm.setSpecializations(specsEntities);
        return admissionRepository.save(adm);
//        return adm;
    }

}
