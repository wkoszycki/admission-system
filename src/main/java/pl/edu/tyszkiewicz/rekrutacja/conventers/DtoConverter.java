package pl.edu.tyszkiewicz.rekrutacja.conventers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.edu.tyszkiewicz.rekrutacja.domain.*;
import pl.edu.tyszkiewicz.rekrutacja.dto.AdmissionDTO;
import pl.edu.tyszkiewicz.rekrutacja.dto.UserDTO;
import pl.edu.tyszkiewicz.rekrutacja.dto.UserEditDTO;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserNotFoundException;
import pl.edu.tyszkiewicz.rekrutacja.repository.SpecializationRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.UserRepository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DtoConverter {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    SpecializationRepository specializationRepository;
    @Autowired
    UserRepository userRepository;

    public User convert(UserDTO dto) {
        Optional<Specialization> specialization = specializationRepository.findById(dto.getSpecializationId());
        return User.builder()
                .name(dto.getName())
                .surname(dto.getSurname())
                .email(dto.getEmail().toLowerCase())
                .password(passwordEncoder.encode(dto.getPassword()))
                .phone(dto.getPhone())
                .role(dto.getRole())
                .specialization(specialization.orElseThrow(() -> new IllegalArgumentException("can't find specialization id")))
                .build();
    }

    public User convert(@Valid UserEditDTO user) throws UserNotFoundException {
        User userEntity = userRepository.findById(user.getId()).orElseThrow(UserNotFoundException::new);
        userEntity.setId(user.getId());
        userEntity.setName(user.getName());
        userEntity.setSurname(user.getSurname());
        userEntity.setEmail(user.getEmail().toLowerCase());
        userEntity.setPhone(user.getPhone());
        userEntity.setUserExtraInfo(convert(user.getUserExtraInfo()));

        return userEntity;
    }

    public Admission convert(@NotNull @Valid AdmissionDTO admissionDTO) {
        return Admission.builder()
                .id(admissionDTO.getId())
                .name(admissionDTO.getName())
                .type(admissionDTO.getType())
                .startDate(admissionDTO.getStartDate())
                .specializations(admissionDTO.getSpecializations()
                        .stream()
                        .map(s -> new Specialization(s.getId(), s.getName(), s.getSpecializationStatus() == null ? SpecializationStatus.ZAREJESTROWANY : SpecializationStatus.valueOf(s.getSpecializationStatus())))
                        .collect(Collectors.toList()))
                .endDate(admissionDTO.getEndDate())
                .inaugurationDate(admissionDTO.getInaugurationDate())
                .academicYear(admissionDTO.getAcademicYear())
                .build();
    }

    private UserExtraInfo convert(@NotNull @Valid UserExtraInfo userExtraInfo) {
        return null;
    }

//    public UserExtraInfoDTO convert(User user) {
//        final UserExtraInfoDTO.UserExtraInfoDTOBuilder builder = UserExtraInfoDTO.builder()
//                .academicYear(user.get)
//                .study(user.get)
//                .studyType(user.get)
//                .specialization(user.get)
//                .surname(user.getSurname())
//                .name(user.getName())
//                .secondName(user.get)
//                .fatherName(user.get)
//                .motherName(user.get)
//                .promoCode(user.get)
//                .nationality(user.get)
//                .pesel(user.get)
//                .sex(user.get)
//                .Pattern(user.get)
//                .phone(user.get)
//                .email(user.get)
//                .livingAddress(user.get)
//                .birthAddress(user.get)
//                .correspondenceAddress(user.get)
//                .isOurStudent(user.get);
//        return builder.build();
//    }
}
