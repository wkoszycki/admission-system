package pl.edu.tyszkiewicz.rekrutacja.security.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class LoginResponse implements Serializable {

    private String token;

    @JsonCreator
    public LoginResponse(@JsonProperty("token") String token) {
        this.token = token;
    }
}
