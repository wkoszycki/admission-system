package pl.edu.tyszkiewicz.rekrutacja.security.jwt.extractor;

public interface TokenExtractor {
    public String extract(String payload);
}
