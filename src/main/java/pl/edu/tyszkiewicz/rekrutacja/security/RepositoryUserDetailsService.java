package pl.edu.tyszkiewicz.rekrutacja.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;
import pl.edu.tyszkiewicz.rekrutacja.domain.UserStatus;
import pl.edu.tyszkiewicz.rekrutacja.repository.UserRepository;
import pl.edu.tyszkiewicz.rekrutacja.security.common.AuthUser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
public class RepositoryUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository dao;

    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        Optional<User> userEntity = dao.findByEmailNotInStatus(username.toLowerCase(), UserStatus.ARCHIWALNY);
        if (!userEntity.isPresent())
            throw new UsernameNotFoundException("User not found");

        return buildUserFromUserEntity(userEntity.get());
    }

    @Transactional(readOnly = true)
    AuthUser buildUserFromUserEntity(User userEntity) {

        String username = userEntity.getEmail();
        String password = userEntity.getPassword();
        boolean enabled = userEntity.isActive();
        boolean accountNonExpired = userEntity.isActive();
        boolean credentialsNonExpired = userEntity.isActive();
        boolean accountNonLocked = userEntity.isActive();

        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(userEntity.getRole().name()));

        AuthUser authUser = new AuthUser(username, password, enabled,
                accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        authUser.setFullName((userEntity.getName()) + " " + userEntity.getSurname());
        authUser.setId(userEntity.getId());
        authUser.setTest("test string");
        return authUser;
    }
}