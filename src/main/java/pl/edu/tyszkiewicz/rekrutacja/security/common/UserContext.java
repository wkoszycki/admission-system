package pl.edu.tyszkiewicz.rekrutacja.security.common;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.List;

public class UserContext implements Serializable
{
    private final String username;
    private final String fullName;
    private final Long id;
    private final List<GrantedAuthority> authorities;

    private UserContext(Long id,String username, String fullName, List<GrantedAuthority> authorities) {
        this.username = username;
        this.authorities = authorities;
        this.fullName = fullName;
        this.id = id;
    }

    public static UserContext create(Long id, String username, String fullName, List<GrantedAuthority> authorities) {
        if (StringUtils.isBlank(username)) throw new IllegalArgumentException("Username is blank: " + username);
        if (StringUtils.isBlank(fullName)) throw new IllegalArgumentException("Full name is blank: " + fullName);
        return new UserContext(id,username, fullName, authorities);
    }

    public String getUsername() {
        return username;
    }

    public List<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public String getFullName() {
        return fullName;
    }

    public Long getId() {
        return id;
    }
}

