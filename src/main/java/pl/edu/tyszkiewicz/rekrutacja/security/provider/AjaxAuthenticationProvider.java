package pl.edu.tyszkiewicz.rekrutacja.security.provider;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.edu.tyszkiewicz.rekrutacja.security.common.AuthUser;
import pl.edu.tyszkiewicz.rekrutacja.security.common.UserContext;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AjaxAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserDetailsService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        log.info("Do AjaxAuthenticationProvider : authenticate");

        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        AuthUser userDetails = (AuthUser) userService.loadUserByUsername(username);
        if (userDetails == null) {
            log.error("AjaxAuthentication fail: " + username + " - user not found");
            throw new UsernameNotFoundException("Authentication Failed. Username or Password not valid.");
        }

        if (!passwordEncoder.matches(password, userDetails.getPassword())) {
            log.error("AjaxAuthentication fail: " + username + " - password incorrect");
            throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
        }

        // To convert Role from USER / ADMIN to spring role
        List<GrantedAuthority> authorities = userDetails.getAuthorities().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getAuthority()))
                .collect(Collectors.toList());

        log.info("AjaxAuthentication success: " + username + " as " + authorities);
        UserContext userContext = UserContext.create(userDetails.getId(),userDetails.getUsername(), userDetails.getFullName(),  authorities);

        return new UsernamePasswordAuthenticationToken(userContext, null, userContext.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
