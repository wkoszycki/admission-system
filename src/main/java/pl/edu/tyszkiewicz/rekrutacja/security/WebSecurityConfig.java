package pl.edu.tyszkiewicz.rekrutacja.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import pl.edu.tyszkiewicz.rekrutacja.security.common.SkipPathRequestMatcher;
import pl.edu.tyszkiewicz.rekrutacja.security.filter.AjaxAuthenticationFilter;
import pl.edu.tyszkiewicz.rekrutacja.security.filter.JwtAuthenticationFilter;
import pl.edu.tyszkiewicz.rekrutacja.security.provider.AjaxAuthenticationProvider;
import pl.edu.tyszkiewicz.rekrutacja.security.provider.JwtAuthenticationProvider;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    RepositoryUserDetailsService userDetailsService;

    @Autowired
    RestAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    AjaxAuthenticationProvider ajaxAuthenticationProvider;

    @Autowired
    JwtAuthenticationProvider jwtAuthenticationProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable() // We don't need CSRF for JWT based authentication
                .exceptionHandling()
                .authenticationEntryPoint(this.authenticationEntryPoint)
                .and()
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .authorizeRequests()
                    .antMatchers("/user/**").hasAnyAuthority("USER")
                    .antMatchers("/admin/**").hasAnyAuthority("ADMIN","SUPER_ADMIN")
                .and()
                    .authorizeRequests()
                    .antMatchers("/login").permitAll() // Login end-point
                    .antMatchers("/user-api/**").permitAll() // reset end-point
                    .antMatchers("/user-api/users").permitAll() // Register end-point
                    .antMatchers("/user-api/register").permitAll() // Register end-point
                    .antMatchers("/user-api/reset-password/*").permitAll() // reset password
                    .antMatchers("/console").permitAll() // H2 Console Dash-board - only for testing

                .and()
                    .addFilterBefore(ajaxAuthenticationFilter(), BasicAuthenticationFilter.class)
                    .addFilterBefore(jwtAuthenticationFilter(), BasicAuthenticationFilter.class);
        
        http.headers().frameOptions().sameOrigin();
        http.headers().cacheControl().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(ajaxAuthenticationProvider);
        auth.authenticationProvider(jwtAuthenticationProvider);
    }

    @Bean
    public AjaxAuthenticationFilter ajaxAuthenticationFilter() {
        // only for angular js login function
        AjaxAuthenticationFilter ajaxAuthenticationFilter = new AjaxAuthenticationFilter("/login");
        ajaxAuthenticationFilter.setAuthenticationManager(this.authenticationManager);
        return ajaxAuthenticationFilter;
    }

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        // jwt authentication
        List<String> pathsToSkip = Arrays.asList("/login", "logout", "/user-api/register","/user-api/reset-password", "/user-api/reset-password/**");
        List<String> pathsToCheck = Arrays.asList("/user/**", "/admin/**");
        SkipPathRequestMatcher matcher = new SkipPathRequestMatcher(pathsToSkip, pathsToCheck);
        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter(matcher);
        jwtAuthenticationFilter.setAuthenticationManager(this.authenticationManager);
        return jwtAuthenticationFilter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
