package pl.edu.tyszkiewicz.rekrutacja.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import pl.edu.tyszkiewicz.rekrutacja.security.common.LoginRequest;
import pl.edu.tyszkiewicz.rekrutacja.security.common.LoginResponse;
import pl.edu.tyszkiewicz.rekrutacja.security.common.UserContext;
import pl.edu.tyszkiewicz.rekrutacja.security.jwt.JwtTokenFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Slf4j
public class AjaxAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    JwtTokenFactory tokenFactory;

    public AjaxAuthenticationFilter(String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl);
    }

    public AjaxAuthenticationFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
        super(requiresAuthenticationRequestMatcher);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        log.info("Do AjaxAuthenticationFilter : attemptAuthentication");

        if (!HttpMethod.POST.name().equals(request.getMethod())) {
            log.error("Authentication method not supported");
            throw new AuthenticationServiceException("Authentication method not supported");
        }
        try {
            LoginRequest loginRequest = objectMapper.readValue(request.getReader(), LoginRequest.class);
            if (StringUtils.isBlank(loginRequest.getEmail()) || StringUtils.isBlank(loginRequest.getPassword())) {
                throw new AuthenticationServiceException("Username or Password not provided");
            }
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword());
            return this.getAuthenticationManager().authenticate(token);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationServiceException("Username or Password not provided");

        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        log.info("Do AjaxAuthenticationFilter : successfulAuthentication");

        clearAuthenticationAttributes(request);

        UserContext userContext = (UserContext) authResult.getPrincipal();
        String token = tokenFactory.createAccessJwtToken(userContext);

        log.info("AjaxAuthentication success: " + userContext.getUsername() + ", response token: " + token);
        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        objectMapper.writeValue(response.getWriter(), new LoginResponse(token));
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        log.info("Do AjaxAuthenticationFilter : unsuccessfulAuthentication");

        clearAuthenticationAttributes(request);

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        log.error("AjaxAuthentication fail: ", failed);
        objectMapper.writeValue(response.getWriter(), failed);
    }

    /**
     * Removes temporary authentication-related data which may have been stored
     * in the session during the authentication process..
     *
     */
    protected final void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);

        if (session == null) {
            return;
        }

        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
}
