package pl.edu.tyszkiewicz.rekrutacja.security.provider;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import pl.edu.tyszkiewicz.rekrutacja.security.JwtSettings;
import pl.edu.tyszkiewicz.rekrutacja.security.common.UserContext;
import pl.edu.tyszkiewicz.rekrutacja.security.jwt.JwtAuthenticationToken;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    JwtSettings jwtSettings;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String token = (String) authentication.getCredentials();

        Jws<Claims> jwsClaims = parseClaims(jwtSettings.getTokenSigningKey(), token);
        String subject = jwsClaims.getBody().getSubject();
        List<String> scopes = jwsClaims.getBody().get("scopes", List.class);
        List<GrantedAuthority> authorities = scopes.stream()
                .map(authority -> new SimpleGrantedAuthority(authority))
                .collect(Collectors.toList());
        String fullName = jwsClaims.getBody().get("fullName", String.class);
        Object id = jwsClaims.getBody().get("id", Object.class);
        Long parsedId = null;
        if (id != null && id instanceof Integer) {
            parsedId = Long.valueOf(id.toString());
        }
        if (id != null && id instanceof Long) {
            parsedId = (Long) id;
        }
        log.info("JwtAuthentication, request token: " + token);
        log.info("JwtAuthentication success: " + subject + " as " + authorities);
        UserContext context = UserContext.create(parsedId, subject, fullName, authorities);

        return new JwtAuthenticationToken(context, context.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }

    public Jws<Claims> parseClaims(String signingKey, String token) {
        try {
            return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(token);
        } catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException ex) {
            log.error("Invalid JWT Token", ex);
            throw new BadCredentialsException("Invalid JWT token: ", ex);
        } catch (ExpiredJwtException expiredEx) {
            log.info("JWT Token is expired", expiredEx);
            throw new BadCredentialsException("JWT Token is expired", expiredEx);
        }
    }
}
