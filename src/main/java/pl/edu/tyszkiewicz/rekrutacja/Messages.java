package pl.edu.tyszkiewicz.rekrutacja;

public class Messages {
    // TODO: 25/04/2018 externalize
    public static final String REGISTER_MAIL_SUBJECT = "Rejestracja w Bielskiej Wyższej Szkole im. J.Tyszkiewicza";
    public static final String REGISTER_MAIL_BODY = "Witaj %s!\n" +
            "\n" +
            "Dziękujemy za rejestrację w systemie on-line Bielskiej Wyższej Szkoły im. J. Tyszkiewicza.\n" +
            "Zapraszamy do wypełnienia kwestionariusza oraz złożenia wymaganych dokumentów.\n" +
            "http://rekrutacja.tyszkiewicz.edu.pl/index.html?login\n" +
            "\n" +
            "Pozdrawiamy i Do zobaczenia!";

    public static String RESET_PASS_MAIL_SUBJECT = "Reset Hasła Bielska Wyższa Szkoła ";
    public static String RESET_PASS_MAIL_BODY = "Witaj %s!\n" +
            "\n" +
            "Wysłano żadanie zresetowania hasła do systemu \n" +
            "Aby zresetować hasło kliknij poniższy link będzie on aktywny 24h\n" +
            "http://rekrutacja.tyszkiewicz.edu.pl/user-api/reset-password/%s\n";
    public static String RESET_PASS_DONE_MAIL_BODY = "Witaj %s!\n" +
            "\n" +
            "Twoje nowe hasła do systemu to: \n" +
            "%s\n";

    private Messages() {
    }
}
