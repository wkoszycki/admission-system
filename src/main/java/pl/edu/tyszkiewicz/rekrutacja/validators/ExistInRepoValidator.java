package pl.edu.tyszkiewicz.rekrutacja.validators;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;
import pl.edu.tyszkiewicz.rekrutacja.domain.UserStatus;
import pl.edu.tyszkiewicz.rekrutacja.repository.SpecializationRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.UserRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

@Slf4j
public class ExistInRepoValidator implements ConstraintValidator<ExistInRepo, Object> {

    @Autowired
    UserRepository userRepository;
    @Autowired
    SpecializationRepository specializationRepository;

    private Class type;
    private Boolean ifExist;

    public void initialize(ExistInRepo constraintAnnotation) {
        this.type = constraintAnnotation.type();
        this.ifExist = constraintAnnotation.ifExistIsValid();
    }

    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) {
            return !ifExist;
        }
        switch (type.getCanonicalName()) {
            case "pl.edu.tyszkiewicz.rekrutacja.domain.User":
                final Optional<User> user = userRepository.findByEmailNotInStatus(value.toString().toLowerCase(), UserStatus.ARCHIWALNY);
                return ifExist == user.isPresent();
            case "pl.edu.tyszkiewicz.rekrutacja.domain.Specialization":
                return ifExist == specializationRepository.findById((Long) value).isPresent();
            default:
                throw new AssertionError("unable to validate object");
        }
    }
}