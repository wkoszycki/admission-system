package pl.edu.tyszkiewicz.rekrutacja.dto;

import java.time.LocalDate;

public class DocumentDTO {
    String documentType;// "Dowód Osobisty",
    String documentNumber;// "AAA 345678",
    String documentIssuer;// "Prezydent Kambodży",
    LocalDate documentExpireDate;// "11.12.2011",
}
