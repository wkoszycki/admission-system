package pl.edu.tyszkiewicz.rekrutacja.dto.dashboard;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.edu.tyszkiewicz.rekrutacja.domain.UserStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserStatusDTO {

    UserStatus status;
    Integer studentsCount;
}
