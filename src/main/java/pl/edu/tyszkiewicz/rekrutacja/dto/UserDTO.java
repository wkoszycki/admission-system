package pl.edu.tyszkiewicz.rekrutacja.dto;

import lombok.*;
import pl.edu.tyszkiewicz.rekrutacja.domain.Role;
import pl.edu.tyszkiewicz.rekrutacja.domain.Specialization;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;
import pl.edu.tyszkiewicz.rekrutacja.validators.BusinessLogicChecks;
import pl.edu.tyszkiewicz.rekrutacja.validators.ExistInRepo;
import pl.edu.tyszkiewicz.rekrutacja.validators.FormatChecks;
import pl.edu.tyszkiewicz.rekrutacja.validators.Matches;

import javax.validation.GroupSequence;
import javax.validation.constraints.*;

@GroupSequence(value = {UserDTO.class, FormatChecks.class, BusinessLogicChecks.class})
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Matches(groups = FormatChecks.class, field = "password", verifyField = "passwordRetype")
public class UserDTO {

    @NotNull(groups = FormatChecks.class)
    @Size(groups = FormatChecks.class, min = 2)
    private String name;
    @NotNull(groups = FormatChecks.class)
    @Size(groups = FormatChecks.class, min = 2)
    private String surname;
    @NotNull(groups = FormatChecks.class)
    @Email(groups = FormatChecks.class)
    @ExistInRepo(ifExistIsValid = false, type = User.class, groups = BusinessLogicChecks.class)
    private String email;
    /**
     * This regex will enforce these rules:
     * <p>
     * At least one upper case English letter, (?=.*?[A-Z])
     * At least one lower case English letter, (?=.*?[a-z])
     * At least one digit, (?=.*?[0-9])
     * Minimum eight in length .{8,} (with the anchors)
     */
    @NotNull(groups = FormatChecks.class)
    @Pattern(groups = FormatChecks.class, regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$")
    private String password;
    private String passwordRetype;
    @NotNull(groups = FormatChecks.class)
    @Pattern(groups = FormatChecks.class, regexp = "[0-9]{9}")
    private String phone;
    @NotNull(groups = FormatChecks.class)
    @ExistInRepo(ifExistIsValid = true, type = Specialization.class, groups = BusinessLogicChecks.class)
    @NotNull(groups = FormatChecks.class)
    private Long specializationId;
    private Role role;
    @AssertTrue(groups = FormatChecks.class)
    private boolean acceptTerms;

}
