package pl.edu.tyszkiewicz.rekrutacja.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import pl.edu.tyszkiewicz.rekrutacja.domain.Address;
import pl.edu.tyszkiewicz.rekrutacja.domain.KeyValueEntity;
import pl.edu.tyszkiewicz.rekrutacja.domain.StudyType;
import pl.edu.tyszkiewicz.rekrutacja.validators.BusinessLogicChecks;
import pl.edu.tyszkiewicz.rekrutacja.validators.FormatChecks;

import javax.validation.GroupSequence;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@GroupSequence(value = {UserExtraInfoDTO.class, FormatChecks.class, BusinessLogicChecks.class})
@AllArgsConstructor
//@NoArgsConstructor
@Builder
@Getter
@Setter
public class UserExtraInfoDTO {

//    String academicYear;
//    String study;
//    StudyType studyType;
//    String specialization;
//    @Size(groups = FormatChecks.class, min = 2)
//    String surname;
//    @Size(groups = FormatChecks.class, min = 2)
//    String name;
//    String secondName;
////    String fatherName;
////    String motherName;
//    @Past(groups = FormatChecks.class)
//    @JsonFormat(pattern = "dd-MM-yyyy")
//    LocalDate birthday;
//    @Pattern(groups = FormatChecks.class, regexp = "^$|[A-Z]{6}")
//    String promoCode;
//    String nationality;// "polskie",
//    String pesel;// "33224455668",
//    String sex;// "nieokreslona",
//    @Pattern(groups = FormatChecks.class, regexp = "[0-9]{9}")
//    String phone;// "512222333",
//    String email;// "majkel@dzekson.pl",
//    Address livingAddress;
//    Address birthAddress;
//    Address correspondenceAddress;
//    boolean isOurStudent;// "Nie",
//    List<KeyValueEntity> extraInfo;



}
