package pl.edu.tyszkiewicz.rekrutacja.dto;

import lombok.*;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;
import pl.edu.tyszkiewicz.rekrutacja.validators.BusinessLogicChecks;
import pl.edu.tyszkiewicz.rekrutacja.validators.ExistInRepo;
import pl.edu.tyszkiewicz.rekrutacja.validators.FormatChecks;

import javax.validation.GroupSequence;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@GroupSequence(value = {ResetPasswordDTO.class, FormatChecks.class, BusinessLogicChecks.class})
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ResetPasswordDTO {

    @NotNull
    @Email(groups = FormatChecks.class)
    @ExistInRepo(ifExistIsValid = true,groups = BusinessLogicChecks.class,type = User.class)
    private String resetEmail;
    @NotNull
    @Pattern(groups = FormatChecks.class, regexp = "[0-9]{9}")
    private String resetPhone;

}
