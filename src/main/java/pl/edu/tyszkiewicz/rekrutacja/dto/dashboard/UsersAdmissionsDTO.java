package pl.edu.tyszkiewicz.rekrutacja.dto.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UsersAdmissionsDTO {

    String admissionName;
    String admissionType;
    List<DataCount> dataCounts;
    @JsonIgnore
    Map<Integer, Integer> data;

    @Setter
    @Getter
    @AllArgsConstructor
    class DataCount {
        int month;
        int count;
    }

    public void set() {
        if (data != null) {
            if (dataCounts == null) {
                dataCounts = new ArrayList<>();
            }
            data.forEach((k, v) -> {
                dataCounts.add(new DataCount(k, v));
            });
        }
    }
}
