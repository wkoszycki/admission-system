package pl.edu.tyszkiewicz.rekrutacja.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import pl.edu.tyszkiewicz.rekrutacja.DateDeserialize;
import pl.edu.tyszkiewicz.rekrutacja.domain.StudyType;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class AdmissionDTO implements Serializable{

    private Long id;
    @NotNull
    private String name;
    @NotNull
    private StudyType type;
    @JsonDeserialize(using = DateDeserialize.class)
    @NotNull
    private LocalDate startDate;
    @Valid
    private List<SpecializationDTO> specializations;
    @JsonDeserialize(using = DateDeserialize.class)
    @NotNull
    private LocalDate endDate;
    @JsonDeserialize(using = DateDeserialize.class)
    @NotNull
    private LocalDate inaugurationDate;
    private String academicYear;

}
