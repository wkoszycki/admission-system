package pl.edu.tyszkiewicz.rekrutacja.dto;

import lombok.*;

import java.time.YearMonth;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SpecializationsVsStudentsDTO {

    private Long id;
    private String specName;
    private YearMonth yearMonth;
    private Integer userCount;
}
