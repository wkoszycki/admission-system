package pl.edu.tyszkiewicz.rekrutacja.dto;

import lombok.*;
import pl.edu.tyszkiewicz.rekrutacja.domain.UserStatus;
import pl.edu.tyszkiewicz.rekrutacja.validators.FormatChecks;

import javax.validation.constraints.NotNull;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class BulkUpdateDTO {

    @NotNull(groups = FormatChecks.class)
    private UserStatus status;

    @NotNull(groups = FormatChecks.class)
    private Iterable<Long> ids;

}
