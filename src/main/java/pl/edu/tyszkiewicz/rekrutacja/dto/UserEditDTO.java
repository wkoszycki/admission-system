package pl.edu.tyszkiewicz.rekrutacja.dto;

import lombok.*;
import pl.edu.tyszkiewicz.rekrutacja.domain.Specialization;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;
import pl.edu.tyszkiewicz.rekrutacja.domain.UserExtraInfo;
import pl.edu.tyszkiewicz.rekrutacja.validators.BusinessLogicChecks;
import pl.edu.tyszkiewicz.rekrutacja.validators.ExistInRepo;
import pl.edu.tyszkiewicz.rekrutacja.validators.FormatChecks;

import javax.validation.GroupSequence;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@GroupSequence(value = {UserEditDTO.class, FormatChecks.class, BusinessLogicChecks.class})
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class UserEditDTO {

    @NotNull
    private Long id;
    @Size(groups = FormatChecks.class, min = 2)
    private String name;
    @Size(groups = FormatChecks.class, min = 2)
    private String surname;
    @Email(groups = FormatChecks.class)
    @ExistInRepo(ifExistIsValid = true, type = User.class, groups = BusinessLogicChecks.class)
    private String email;
    @Pattern(regexp = "[0-9]{9}", message = "Invalid Phone")
    private String phone;
    @NotNull
    @ExistInRepo(ifExistIsValid = true, type = Specialization.class, groups = BusinessLogicChecks.class)
    private Long specializationId;
    @NotNull
    @Valid
    private UserExtraInfo userExtraInfo;
}
