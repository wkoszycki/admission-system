package pl.edu.tyszkiewicz.rekrutacja.dto;

import lombok.*;

import java.io.Serializable;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class SurveryResultDTO implements Serializable {

    private int[] howInfo;
    private int howInfoTotal;
    private int[] howInfoExtra;
    private int howInfoExtraTotal;
    private int[] whyChoose;
    private int whyChooseTotal;


    public void addHowInfo(int index) {
        if (howInfo == null) {
            howInfo = new int[7];
        }
        howInfoTotal++;
        howInfo[index] = howInfo[index] + 1;
    }

    public void addHowInfoExtra(int index) {
        if (howInfoExtra == null) {
            howInfoExtra = new int[5];
        }
        howInfoExtraTotal++;
        howInfoExtra[index] = howInfoExtra[index] + 1;
    }

    public void addWhyChoose(int index) {
        if (whyChoose == null) {
            whyChoose = new int[7];
        }
        whyChooseTotal++;
        whyChoose[index] = whyChoose[index] + 1;
    }
}
