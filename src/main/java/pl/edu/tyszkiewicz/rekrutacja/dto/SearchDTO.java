package pl.edu.tyszkiewicz.rekrutacja.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.edu.tyszkiewicz.rekrutacja.repository.query.SearchCriteria;

@NoArgsConstructor
@Getter
@Setter
public class SearchDTO {

    Iterable<SearchCriteria> criteria;

}
