package pl.edu.tyszkiewicz.rekrutacja.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class SpecializationDTO {

    private Long id;

    @NotBlank
    @NotNull
    private String name;
    private String specializationStatus;

}
