angular.module('recruit-user-app').factory('authInterceptor', ['$rootScope', '$timeout', '$q', function($rootScope, $timeout, $q){
    var authInterceptor = {
        request: function(config) {
            if (!config.headers['Content-Type']) {
                config.headers['Content-Type'] = 'application/json'
            }

            if (!config.headers['Accept']) {
                config.headers['Accept'] = 'application/json'
            }
            return config;
        },

        // responseError : function(response) {
        //     if (response.status == 401 || response.status == 403) {
        //         if (!$rootScope.isLoggedOut) {
        //             // only 1 request pop up message
        //             $rootScope.doLogout();
        //         }
        //     }
        //     else {
        //         return $q.reject(response);
        //     }
        // }
    };

    return authInterceptor;
}]);