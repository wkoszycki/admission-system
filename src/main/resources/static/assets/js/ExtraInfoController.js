angular.module('recruit-user-app').controller('ExtraInfoController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
$scope.user = {}
    $scope.showExtraAddress = false
    $scope.countries = countries


    function loadUser() {
        var path = '/api/users/' + $rootScope.getUser().id
        $http.get(path).
        success(function(data, status, headers, config) {
            if (data.userExtraInfo) {
                exInfo = data.userExtraInfo
                if (exInfo.birthday) {
                    exInfo.birthday = new Date(exInfo.birthday)
                }
                if(exInfo.polishCard == null) {
                exInfo.polishCard = false;
                }
//                if (exInfo.document && exInfo.document.issued) {
//                    exInfo.document.issued = new Date(exInfo.document.issued)
//                }
                exInfo.isOurStudent = "" + exInfo.isOurStudent
                if (exInfo.survey) {
                    survey = exInfo.survey
                    if (survey.howInfo) {
                        survey.howInfo = "" + survey.howInfo
                    }
                    if (survey.howInfoExtra) {
                        survey.howInfoExtra = "" + survey.howInfoExtra
                    }
                    if (survey.whyChoose) {
                        survey.whyChoose = "" + survey.whyChoose
                    }
                }
            }
            if (data._embedded.specialization) {
                data.specialization = data._embedded.specialization
                data.specialization.id = ""+data.specialization.id
            }

            $scope.user = data
            $scope.user.id = $rootScope.getUser().id
            $scope.showExtraAddress = (($scope.user.userExtraInfo != null && $scope.user.userExtraInfo.correspondenceAddress != null)) ? true : false;



        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError(userMessages["unexpected_error"] + " " + data)
            console.log('negative response status:' + status + ' data: ' + data);
        });
    };


    $scope.makePdf = function($event) {
        try {
            $scope.user.id = $rootScope.getUser().id
            $('[id^="error_"]').text('');
            $http({
                url: '/user/print',
                method: 'PUT',
                data: $scope.user,
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function(data, status, headers, config) {
                pdfMake.createPdf(fillPdf($scope.user)).download("BWS_kwestionariusz.pdf")
                $rootScope.notifySuccess(userMessages["pdf_ok"])
                $scope.user.userExtraInfo.isLocked = true
            }).error(function(data, status, headers, config) {
                if (data.errors) {
                    $rootScope.handleErrors(data.errors, "#error_", "Aby wydrukować kwestionariusz, należy wypełnić wszystkie sekcje formularza.")
                } else {
                    $rootScope.handleErrors(data, "#error_", "Aby wydrukować kwestionariusz, należy wypełnić wszystkie sekcje formularza.")
                }
            });

        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }

    }
    $scope.updateUser = function($event) {
        try {

            $scope.user.id = $rootScope.getUser().id
            $('[id^="error_"]').text('');
            $http({
                url: '/user/edit',
                method: 'PUT',
                data: $scope.user,
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function(data, status, headers, config) {
                $rootScope.notifySuccess(userMessages["update_ok"])
            }).error(function(data, status, headers, config) {
                if (data.errors) {
                    $rootScope.handleErrors(data.errors, "#error_")
                } else {
                    $rootScope.handleErrors(data, "#error_")
                }
            });


        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }

    }


    angular.element(document).ready(function() {
        try {
            loadUser()
        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }
    });
}]);