angular.module('recruit-user-app').controller('UserController', ['$scope', '$http', function ($scope, $http) {

    //Data objects
    $scope.loginData = {
        login: '',
        password: '',
    }

    $scope.resetData = {
        login: ''
    }

    $scope.user = {}

    function notifySuccess(message){
       	$.notify({
                	icon: 'ti-info-alt',
                	message: message

                },{
                    type: 'success',
                    timer: 4000
                });
    }
        function notifyError(message){
       	$.notify({
                	icon: 'ti-info-alt',
                	message: message

                },{
                    type: 'danger',
                    timer: 4000
                });
    }
    function loadUser() {
//        todo: get user id from session
        console.log('server call /api/users')
            $http.get('/api/users').
//            $http.get('/data/users.json').
                success(function (data, status, headers, config) {
        console.log('server call /api/users')
                            $scope.user = data._embedded.users[0];
                            if ($scope.user.userExtraInfo && $scope.user.userExtraInfo.birthday) {
                                $scope.user.userExtraInfo.birthday = new Date($scope.user.userExtraInfo.birthday);
                            }
                        }).
                error(function (data, status, headers, config) {
                                            notifyError(userMessages["unexpected_error"]+" "+data)
console.log('negative response status:' + status + ' data: ' + data);
                      });
    };

    $scope.updateUser = function ($event) {
                console.log('updateUser()');
                                $http({
                                          url: '/user/edit',
                                          method: 'PUT',
                                          data: $scope.user,
                                          headers: {
                                              "Content-Type": "application/json"
                                          }
                                      }).success(function (data, status, headers, config) {
                                    notifySuccess(userMessages["update_ok"])
                                }).error(function (data, status, headers, config) {
                                    console.log('negative response status:' + status + ' data: ' + data);
                                    for (i = 0; i < data.errors.length; i++) {
                                        $("#error_"+data.errors[i].field).text(userMessages[data.errors[i].codes[0]])
                                    }
                                    notifyError(userMessages["update_"+status])
                                });


    }


    angular.element(document).ready(function () {
        console.log('page loading completed');
        loadUser()
    });
    var userMessages = {};
    userMessages["unexpected_error"] = "Wystąpił nie oczekiwany błąd"
    userMessages["update_ok"] = "update ok msg";
    userMessages["update_400"] = "Nie prawidłowe dane";
    userMessages["Pattern.user.phone"] = "Numer telefonu powinien składać się z 9 cyfr";
    userMessages["Pattern.user.promoCode"] = "Nieprawidłowy kod promocyjny"
    userMessages["Size.user.name"] = "Nieprawidłowe imię"
    userMessages["NotNull.userEditDTO.userExtraInfo.birthAddress.voivodeship"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.livingAddress.commune"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.livingAddress.street"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.correspondenceAddress.zipCode"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.fatherName"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.birthPlace"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.sex"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.livingAddress.city"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.livingAddress.houseNumber"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.document.issuer"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.birthAddress.zipCode"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.birthday"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.document.number"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.correspondenceAddress.commune"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.livingAddress.poviat"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.correspondenceAddress.postOffice"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.correspondenceAddress.street"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.birthAddress.postOffice"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.correspondenceAddress.poviat"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.motherName"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.birthAddress.commune"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.birthAddress.street"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.livingAddress.voivodeship"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.document.issued"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.correspondenceAddress.voivodeship"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.birthAddress.houseNumber"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.livingAddress.postOffice"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.birthAddress.city"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.livingAddress.zipCode"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.birthAddress.poviat"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.correspondenceAddress.houseNumber"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.correspondenceAddress.city"] = "To pole jest wymagane"
    userMessages["NotNull.userEditDTO.userExtraInfo.familyName"] = "To pole jest wymagane"
    userMessages["Min.userEditDTO.userExtraInfo.survey.howInfoExtra"] = "Błędne dane"
    userMessages["Min.userEditDTO.userExtraInfo.survey.rate"] = "Błędne dane"
    userMessages["Min.userEditDTO.userExtraInfo.survey.howInfo"] = "Błędne dane"
    userMessages["Min.userEditDTO.userExtraInfo.survey.whyChoose"] = "Błędne dane"
    userMessages["Pattern.userEditDTO.userExtraInfo.birthAddress.zipCode"] = "Nie poprawny kod pocztowy 00-000"
    userMessages["Size.userEditDTO.userExtraInfo.familyName"] = "Błędne dane"
    userMessages["Pattern.userEditDTO.userExtraInfo.livingAddress.zipCode"] = "Nie poprawny kod pocztowy 00-000"
    userMessages["Size.userEditDTO.userExtraInfo.document.issuer"] = "Błędne dane"
    userMessages["Size.userEditDTO.userExtraInfo.motherName"] = "Błędne dane"
    userMessages["NotNull.userEditDTO.specializationId"] = "Wymagane pole"
    userMessages["Size.userEditDTO.userExtraInfo.birthPlace"] = "Błędne dane"
    userMessages["Size.userEditDTO.userExtraInfo.fatherName"] = "Błędne dane"
    userMessages["Size.userEditDTO.userExtraInfo.document.number"] = "Błędne dane"
    userMessages["Pattern.userEditDTO.userExtraInfo.correspondenceAddress.zipCode"] = "Nie poprawny kod pocztowy 00-000"
}]);