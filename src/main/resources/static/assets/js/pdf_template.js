function fillPdf(user) {
var userCopy = user;
userCopy.userExtraInfo.pesel = ((userCopy.userExtraInfo.pesel)? userCopy.userExtraInfo.pesel : " ");
userCopy.userExtraInfo.nationality = ((userCopy.userExtraInfo.nationality)? userCopy.userExtraInfo.nationality : " ")
userCopy.userExtraInfo.secondName = ((userCopy.userExtraInfo.secondName)? userCopy.userExtraInfo.secondName : " ")
    if (!userCopy.userExtraInfo.correspondenceAddress) {
        userCopy.userExtraInfo.correspondenceAddress = userCopy.userExtraInfo.livingAddress
    }
    var documentTypeLabel = ((userCopy.userExtraInfo.document.type == "DOWOD_OSOBISTY")? "DOWÓD OSOBISTY" : "PASZPORT")
    var ourStudent = ((userCopy.userExtraInfo.isOurStudent && userCopy.userExtraInfo.isOurStudent == 'true')? "TAK" : "NIE")
    var polishCard = ((userCopy.userExtraInfo.polishCard && userCopy.userExtraInfo.polishCard == 'true')? "TAK" : "NIE")
    var addressType = ((userCopy.userExtraInfo.livingAddress.addressType)? userCopy.userExtraInfo.livingAddress.addressType: " ")
    var street = ((userCopy.userExtraInfo.livingAddress.street)? userCopy.userExtraInfo.livingAddress.street: " ")
    var corrStreet = ((userCopy.userExtraInfo.correspondenceAddress.street)? userCopy.userExtraInfo.correspondenceAddress.street: " ")
//    var issuedDate = ((userCopy.userExtraInfo.document.issued)? userCopy.userExtraInfo.document.issued.format('isoDate'): " ")
    var birthdayDate = ((userCopy.userExtraInfo.birthday)? userCopy.userExtraInfo.birthday.format('isoDate'): " ")
        var labels = {};

        labels["howInfoLabel_1"] = "Szkoła"
        labels["howInfoLabel_2"] = "Rodzina/znajomi"
        labels["howInfoLabel_3"] = "Internet"
        labels["howInfoLabel_4"] = "Wizyta kogoś z Uczelni w Pani/Pana szkole"
        labels["howInfoLabel_5"] = "Ulotki/plakaty"
        labels["howInfoLabel_6"] = "Targi edukacyjne"
        labels["howInfoLabel_7"] = "Prasa"

        labels["howInfoExtraLabel_1"] = "Facebook"
        labels["howInfoExtraLabel_2"] = "Twitter"
        labels["howInfoExtraLabel_3"] = "Strona www Uczelni"
        labels["howInfoExtraLabel_4"] = "Strony regionalne poświęcone Bielsku-Białej"
        labels["howInfoExtraLabel_5"] = "Nie dotyczy"

        labels["whyChooseLabel_1"] = "Renoma Uczelni"
        labels["whyChooseLabel_2"] = "Ciekawy program studiów"
        labels["whyChooseLabel_3"] = "Duża liczba zajęć praktycznych"
        labels["whyChooseLabel_4"] = "Możliwość zdobycia popularnego i atrakcyjnego zawodu"
        labels["whyChooseLabel_5"] = "Dobra lokalizacja Uczelni"

        var surv = {}
        surv.howInfoLabel =labels["howInfoLabel_"+userCopy.userExtraInfo.survey.howInfo]
        surv.howInfoExtraLabel =labels["howInfoExtraLabel_"+userCopy.userExtraInfo.survey.howInfoExtra]
        surv.whyChooseLabel =labels["whyChooseLabel_"+userCopy.userExtraInfo.survey.whyChoose]

return {
        	content: [
        	  {
        	        table: {
        	            widths: [100, '*', '*'],
        	            body: [
        	               [
        	                 {image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QA2RXhpZgAASUkqAAgAAAABADIBAgAUAAAAGgAAAAAAAAAyMDEwOjAxOjA1IDA4OjQ0OjE4AP/bAEMACAYGBwYFCAcHBwkJCAoMFA0MCwsMGRITDxQdGh8eHRocHCAkLicgIiwjHBwoNyksMDE0NDQfJzk9ODI8LjM0Mv/bAEMBCQkJDAsMGA0NGDIhHCEyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/AABEIALAAiQMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AOZ8F+C7jxpeXVvb3cVsbeMOTIpOcnHauy/4URqf/QatP+/bUfAj/kN6t/17J/6FXudd1evOE3GLM4xTR4Z/wojU/wDoNWn/AH7aj/hRGp/9Bq0/79tXudFY/WqvcfIjwz/hRGp/9Bq0/wC/bUf8KI1P/oNWn/ftq9zoo+tVe4ciPDP+FEan/wBBq0/79tR/wojU/wDoNWn/AH7avc6KPrVXuHIjwz/hRGp/9Bq0/wC/bUf8KI1P/oNWn/ftq9zoo+tVe4ciPDP+FEan/wBBq0/79tR/wojU/wDoNWn/AH7avc6KPrVXuHIjwz/hRGp/9Bq0/wC/bUf8KI1P/oNWn/ftq9zoo+tVe4ciPDP+FEan/wBBq0/79tR/wojU/wDoNWn/AH7avc6KPrVXuHIj5o8Z/Dm78Gabb3lxfwXKzTeUFjQgg4Jzz9K4qvefjp/yK+nf9fv/ALI1eDV34ebnC8jOSsz1n4Ef8hvVv+vZP/Qq9zrwz4Ef8hvVv+vZP/Qq9zrgxX8VmkNgooornLCquo6lZ6TYyXt/cx29tGMtJIcAf4n2p97e2+nWM97dSCO3gQySOewAya8p1bRfEXxFittVk09IrAgtaQS3zRkJnhyoQ/MRg5zWlOCk7t2Qm7GxqfjrWbzTJ9R0iyi03Ro1ydU1MEeYO3lxDkk9s8GuQ0DVvHOqeIR5GpXV3fgE+RLiO3t0YYDzqvGcHIQZOfpg0rOK6uNej06S3n1O7S8aC3il1OSSEyIMu5JAICA9fU8Zwa27f4b+JrMOLUeQJG3uItYkXc3qcR8muu0IK2hGrM24h8QJ8Q5tIk8S3ZvvtdrH9tUYC7oXcgJnG3J+70NT+JtR8d6VrcSz6nPbXpUJD5BzbXYHdFPAk9VI57dgcqTSrmLxLcaLLBKdZ+12y+f/AGk5+YxOynfszwpxXQXXw38TXsQiu/8ASIwdwSXWZHGfXBjqm4pq9tvIWpu6N4416LSotSv7KLWNJPD3unqVmgI6iWE9x3xxj1ru9J1jT9csEvtNuo7i3foyHofQjqD7GvDNViudL1eSza3m06+EsSXPkanIkTq4ASctjJychmPcjuTXR6V4a8T+BpLnWbWwjnRVL3MCag0hmXqTtKDLDkg5z/I41KUWrrRlJs9eoqnpWp2utaVbalZPvt7hA6Hv7g+4OQfcVcrkatoWFFFFAHlnx0/5FfTv+v3/ANkavBq95+On/Ir6d/1+/wDsjV4NXq4T+EYz3PWfgR/yG9W/69k/9Cr3OvDPgR/yG9W/69k/9Cr3OuLFfxWXDYKKKK5yzifH7DULvQPDbybINTuy9yScBoYRvZSfc7fyrf1rWLbSfD1/eQzQ5tbZ5I0Vh1VSQAPqBXH/ABI0r+2PEfhuwZLRVuzPEJriJnCMFVgMBh97GKwNY+Fs2laNe6iH0qb7LA8xjFo4LBQSRnzPaumMYOMbv+rkNu7LfwpsoI9Rvb+5dBJa20NtGXYZ3uPNlOT33Nj6V6r9stf+fmH/AL7FeJaJ4THjS81G5i/sy3MTxttktncssiB1OQ47H9K2v+FOzf8APzpH/gFJ/wDHKqrGEpXlKwJu2iM+/ljPxtnkEimP+0bL5s8f8ezd69j+2Wv/AD8w/wDfYrwKXQPs/imfwoYNNLm8to/PEDhfnid87d/bOOtdV/wp2b/n50j/AMApP/jlOrGDtd20Emyz8VrC1vH0q8ikjeSd206QKwORICYyfZXUH8a6/wAIa5FqnhDSrye4j86S2USbnAJcDax/MGvM9X8F/wDCG3WkX0p0ycyahFGFS2dCoyWLZLnoF9KXw98NJte0Cz1XOlW4uU3iJrR2KjJxz5n4/jQ4wdNJvQLu52ngxk0rxR4j8Owsps45EvbQKQQqyD51GOgDdBXcV5l4B0T+wvHWs6eq2b/ZbSMSzW8TJ8zncFOWPYZr02sK1ubQuOwUUUVkM8s+On/Ir6d/1+/+yNXg1e8/HT/kV9O/6/f/AGRq8Gr1cJ/CMZ7nrPwI/wCQ3q3/AF7J/wChV7nXhnwI/wCQ3q3/AF7J/wChV7nXFiv4rLhsFFFRXUTz2k0MczwSSRsqyoAWjJGAwzxkda5yzA8b6Jca1oG6wO3U7KVbuzb/AKaJyB+IyPyqDTfHHhvWtDR73U7GzkmjKXFpdXCRvG3RlIYg9c81y3hvTPEWq3mqabqHjDVLbUtOmCSIiqVdGGUdcjOCK5jxhouu6Hdlb25ibzHPkai1vH5dxkk7ZSV+ST3PB79Ca6o00/cbIb6k/gnW7bwv4raKW+gl08k6fcXSyhk+UkwSlhxgrlPbbzXrf/CX+Gf+hi0j/wADY/8A4qvHrJYls2lsNS/4myKDJo+o2kKfaY+pCNt2v03Kec4HHSqOgw2V9fmDVNZezglfZb3X2OEJu7xyZX9249+PQ4wTpUpKb5mJO2hr3uq6c/xgn1FL+1ax/tCzP2kTKYsC3YE7s468da9Y/wCEv8M/9DFpH/gbH/8AFV45NpU8HjafwuuoBoDeWsYuPssW7Dwu5ONuO9V/ElrY6Xdm103XTd+S2Lu5a0h8qH/ZBC/O57KP6HBKnGdlfoCbRrfEPxHb+INdigsbyA2VuptILkyARPPMAHbd0KpGeueCR616EfF3hTw74cVLfWrC4jsbdUjhguUeSTaAAAAcknivMnSOaxibUdQaLUZAFtNF0+yhlmxgDMgC4V2xuPT6dh2PgTwHf2cq6p4hMJmBzBZiKM+X6M7BeWHoOB/KakYKCT6fiCbub3gXSbuz0q41PVF26pq0xu7hT/yzB+4n4Dt2ya6qiiuWUuZ3NErBRRRUgeWfHT/kV9O/6/f/AGRq8Gr3n46f8ivp3/X7/wCyNXg1erhP4RjPc9Z+BH/Ib1b/AK9k/wDQq9zrwz4Ef8hvVv8Ar2T/ANCr3OuLFfxWXDYKKKK5yzjb1fsHxZ0udAQNS06a3kx0JjIcE++DitjxdFeTeE9Sj0+BZ7toSIo2jWQMc9NrAg/iKzNd/wCSjeEP+ud9/wCgJXW1pJ25X/W4l1PnKytbHQZNuunUbqGAlrvR5rBIygYYDoTJ8q7iPmQDoAcAiulsvDWlW1/9stvB3iZ7WVCHtGkieCVSOM/Nk4JBBz1ANdd8UtIjufDv9sJEGudNO844MkLfLJGT6FST+FT/AAyv3ufCrWMshkfTLmSzDnqyLgof++WA/CumVVuHOiFHWx5+fB0H213Tw54qS0d0fyAYiw2qyhQ+7O3DYx1wOtXL3wxpl5dRSP4N8TxWsKBY7SJ41iB7t97OTxk55xXs9cr8RL6e08IzW9oxW71CWOxhI7NIcH/x3dWca8pSSG4pFT4d3OiX2nzTaF4dfTLVG8vz5Au6YjqA2SzAepOP1rtap6Vptvo+lWunWi7YLaMRoPXHc+56n61crCbTk2ilsFFFFSMKKKKAPLPjp/yK+nf9fv8A7I1eDV7z8dP+RX07/r9/9kavBq9XCfwjGe56z8CP+Q3q3/Xsn/oVe514Z8CP+Q3q3/Xsn/oVe51xYr+Ky4bBRRRXOWclrv8AyUbwh/1zvv8A0BK62uS13/ko3hD/AK533/oCV1tXPaPp+rEupl+JbKXUfC2rWVvGJJ57OWOJCQNzlCFGTwOcda8os9C8daOpkstP1W0eVIxMILu2dGdUCltpz6f/AF69roqoVXBWsDVzxw+IfH+mc3B1Z1H/AD8aMjoP954mz+OKh/4TK+8Ua1oNnqDaQi2epR3MsiTGHG3PBSbBzz2zXtNUNS0XS9YiMWpafbXS4x+9jDEfQ9R+FWq0esRcr7l4EMAQQQeQRS1xEngm/wBCY3Hg7VpbMLydOu2Mts/sM8p9RWj4f8XpqV8+karaNpmtxDLWspyJR/ejbow/zzjNZOGl4u479zpqKKKgYUUUUAeWfHT/AJFfTv8Ar9/9kavBq95+On/Ir6d/1+/+yNXg1erhP4RjPc9Z+BH/ACG9W/69k/8AQq9zrwz4Ekf23qwzz9mX/wBCr3OuLFfxWXDYKKKK5yzktd/5KN4Q/wCud9/6AldbXJa7/wAlG8If9c77/wBASutq57R9P1Yl1MjxXJJD4P1uWJ2SRLCdldTgqRG2CD2NeOaDD4q1q2MmlXWt3Ato4hIF1vy8s8YY4V1Ixya9h8X/APIla9/2Drj/ANFtXGfBz/kH6n/26/8Aoha2pS5abZL1ZiN4p8beGSG1FrtYQeRqdmHiHt58POfTIxXbeGfiFYa7cRWN3EbHUJFzGhkEkU/qY5Bw306/ka7BlV0KsoZWGCCMgivIPiJ4SsdIurO60sfZI7+YxvBHwkcwUskqAfdIIwcdvxoi4VdGrMHdHsFYXijwxbeJbBVLG3v4DvtLyPh4HHIIPpwMj+uKn8MX82q+FtKv7jBnntY5JCO7FRk/nWtWF3GWnQrdHOeDteuNa0yaHUYxFq1hMba9RehcdGHsw5/OujrjdOH2f4ta1HECEudNgmmx03qxUZ99tdlTqJJ6AgoooqBnlnx0/wCRX07/AK/f/ZGrwavePjqwHhnTVyNxvMgf8Ab/ABrwevVwn8JGM9z1H4SH+yviXqGmvxmGaADP8SOD/JTXvdeA6qw8KfHZbpvlhlu1lLdBtmGHP4Fm/Kvfq48Tq1Lui4dgooormLOS13/ko3hH/rnff+gJXW1xlyRf/F2wiTBGm6ZJM5HZpGCgfXAzXZ1c9oryEjG8X/8AIla9/wBg64/9FtXG/B5SthqYYEHFqefe3Sum+IN8th4D1hzy01u1uijks0nyAD/vqsT4VWpi07Vp/wDlmbz7PGc8MsUaoCPbg/lWkf4LE/iPQK8/+Kf/AB76B/2ER/6LavQK4b4owZ0TTLskKlrqcLyueixtlCf/AB4VFH40OWxs+Bv+RE0P/ryj/wDQRW7NNHbwSTTOscUal3djgKAMkmuL8E6/YWHw20+41S7jtFtFa2m81sFXRiu3Hc4A4HPNVJDqXxIlRFin07worBnZ/kl1DHIAH8Mf8/5U4Nybewk9C94GEmr3+teK5EZI9SlWKzVhg/Z4xtVvbccn8K7So4IYraCOCCNY4o1CIijAUDgACpKznLmdxpWCiiipGeN/Hm6GzRLQHkmWVh/3yB/WvNf+ER1P+4P1rr/ihI3iH4o2mjwsT5YhtPl5+ZzuJ/8AHx+Ve6f2XY/8+sf5V3qr7GnFdzPl5mzyf456IWj07XY1+7m1mPtyyfru/MV6D4G15fEfhCwvy+6cRiKf1Ei8N+fX6EVf8RaLD4h8P3ulT4C3EZUNj7rdVb8CAa8Y+FmuzeFfFt14a1XMKXMvlbW6Rzg4H4N0z3+Wsl+8o26x/Ie0j3qq2oXsem6fPeSpK6RLuKRIXdvQADkk1ZorlLPJ/C3iv7Bd6tq2raHrp1LUrgMyxWDMscSjEaAnGcDNdL/wsiy/6APiL/wXN/jXZ0VrKcZO7X4kpM8Y8c+INa1/7NLaaFqsNhbsZLaOS2bzJpwMCRlAO1EzkZ+8fxx0Hhzxbpnh3w9ZaVDofiJhbx4Z/wCzm+djyzde5JNdZpnivStY1y/0mxlaWexAMrqB5fpgHPJB4NYdz8VNAt7qZBBqU1nBKIZdQhtt1uj+hbOfyHPbNaXclycuwttbkn/CyLL/AKAPiL/wXN/jVDW/GWl67ol5pdxoPiLyrmIoT/Zx+U9j16g4P4V1es+JtK0PRk1W8uQbWXb5JjG4ylhlQoHXI5qn4f8AG2keIr+awthc297Cu9ra7hMb7eOQPxH51CtbmUR+Vzi/hx4f0fU997q2i3S6zC+9/tkTiJjwPMRSMZOATnJz0wMV6v0rkdZ+ImkaRqk+nC21G/ubdd1wLG38wQjr8xJGK3tF1qx8QaVFqWnTebby5wSMFSOoI7EUqvNL3mtAVloaFFFFZFBUF7eQ6fYXF7cttgt42lkb0UDJqevKPjT4pFppcXh62k/f3WJLnB+7GDwPxI/Ie9XTg5yUUJuyucx8MrWbxT8S7rXrlMrA0l0/cB3JCr+GSR/u179XEfCzw03h7whE88ey8vj58oPVRj5F/Ac/Umu3rTETUp6bIUVZBXkfxg8FPcx/8JPpsZ8+FQLtEHLKOkn1HQ+2PSvXKQgMpVgCCMEHvWdOo4S5kNq6scD8M/HkfifS1sL6UDV7ZAH3HmdR/GPf19+e9d/XhXjvwJf+ENV/4SbwyXjtI381li+9anvx3Q/p0PFdz4C+JNl4qhSyvWjtdXUcxZws3umf/Qev1FbVaSa9pT2/IlPozvKoa1pSa3pE+nS3Fxbxz7Q8lu+x8BgSAffGD7E1fornTs7os8y8H6TZ6T8T/FOl2EXkWsVnboiqeRmNMnPrkk59axIxq3hz4d614Uu/Dt7JLGkzC9SMfZjH94yFyeoAJA68Ada9E03w1NY+Oda8QNcRtFqEUSLEAdybFUcn/gNYF74D8RXCX2mJ4rk/sW9mMkiTRmSdVPVA5PTp3/DrnpVRN6vt+BFjAj0/U7z4e+CdXtLSW/8A7KuBNLaxjLvGH42juQFAH1qSfWJdV+Mfhu/i068sIpIHiX7VF5ckoCuSSvp82Bn0zXc6z4YuZfDNnpeganPpUtiUMEiOcMFGNr4+8D1Pv61R0Pwjqg8TjxJ4k1GC81CKEwW0dvGVjhU5yRnkk5P5n2wKpGzb8/xCzF8S61pHgnz5bCwjl13VXHl20K/PO/IDNjtkn6mrPw+8OXPhjwpFZ3rKbuWRp5lXGEZsfKMegA/HNYN18PvEL+LrvxFaeJoILuUssZeyWXyo+yruJAwOMgDv6muz0Gz1ay04xazqi6ldeYSJ1gWL5eMDavHrz71E2lCyd+41ualFFYnibxVpfhTTWu9RmAYg+VApy8p9AP69BWKTbsig8VeJrLwpokuo3bAsPlhhzhpX7KP6nsK8a8B+H7zx/wCMLjxDrIMlnFL5kpYfLJJ/DGP9kDGR6ADvVe3t9e+MHiszTE2+nwHBI5jtkP8ACv8Aec4/H2A4960jSLLQtLg06wiEVvCu1R3J7knuT1JrqdqEeVfE/wACPifkXqKKK5CwooooAQgMpVgCCMEHvXkvjT4QLcSvqfhcrb3AO9rPdtUnrmM/wn2PH0r1uirp1JQd4iaT3PDPD/xX1rw1cf2T4rsricRfKZGXbcIPcHhx+R9zXreh+KdE8Rwh9L1CGdsZaLO2Rfqp5FSa34d0nxFa/Z9VsYrhQPlZhh0/3WHI/CvL9Z+B7Rym48PasY2BysV1kFT7Ov8Ah+NbXpVN/df4E+8vM9korwpLj4s+FBteK7vbdf7yi6BH1GWA/EVKnxs12yby9T0G38wdQN8J/Js0vq0n8LTHzrqe4UV4z/wvv/qWv/J7/wC11DP8eLuT5bXw/EjHgeZcl+foFFH1Wr2DnR7ZVe9v7TTrZrm9uYbaBeskrhVH4mvEm8bfEzxH8umaZJbxt/HbWZAx/vvkD8xTrb4TeLPENyt14l1byT382U3Eo9hg7R+dP2Cj8ckhc19kbfin402Vqr2vh2H7XP0+0yqRGv8Aujq36D61zugfD3xF451H+2fE9zPBbOQS0vEso9EXoi/h9Aa9M8N/Dfw74aZJoLU3N4vIubn52B9VHRfwGfeuuodaMFakvmHK3uU9K0qx0XTorDTrdILaIYVF/mT3PuauUUVzN31ZYUUUUAf/2Q==',	width: 60, height: 60, rowSpan: 2, alignment: 'center'},
			                 {colSpan: 2, text: 'KWESTIONARIUSZ OSOBOWY KANDYDATA NA STUDIA\nw roku akademickim '+ userCopy._embedded.specialization.admission.academicYear, alignment: 'center', bold: 'true'}, '',

        	               ],
        	               [
        	                   '',
        	                 {colSpan: 2, text: 'BIELSKA WYŻSZA SZKOŁA im. J. TYSZKIEWICZA', alignment: 'center'},
        	                 'ab',
        	               ],
        	               [
        	                   {text: 'Kierunek studiów:', bold: 'true'},
			                   userCopy._embedded.specialization.admission.name,
        	                   {rowSpan:3, text: 'Wypełnia Biuro ds. studenckich:\n\nNumer albumu:\nData decyzji:'},
        	               ],
        	               [
        	                   {text: 'Forma studiów:', bold: 'true'},
			                   userCopy._embedded.specialization.admission.type,
        	                   '',
        	               ],
        	               [
        	                   {text: 'Preferowana specjalność:', bold: 'true'},
			                   userCopy._embedded.specialization.name,
        	                   '',
        	               ],
        	           ],
        	        }
        	    },
        	    {text: '\n'},
        	    {text: '\n'},
        		{
        		    table: {
        		        widths: ['*'],

        		        body: [
        		            ['Dane osobowe']
        		        ]
        		    },

        		},
        		{ layout: 'noBorders',
        		    table: {
        		        widths: ['*', '*'],
        		        body: [


        		['Nazwisko: ', ' '+userCopy.surname],
//        		['Nazwisko rodowe: ', ' '+userCopy.userExtraInfo.familyName],
        		['Imię: ', ' '+userCopy.name],
        		['Drugie imię: ', ' '+userCopy.userExtraInfo.secondName],
//        		['Imię ojca: ', ' '+userCopy.userExtraInfo.fatherName],
//        		['Imię matki: ', ' '+userCopy.userExtraInfo.motherName],
        		['Data urodzenia: ', ' '+birthdayDate],
        		['Państwo urodzenia: ', ' '+userCopy.userExtraInfo.birthPlace],
        		['Posiadam kartę Polaka: ', ' '+polishCard],
        		['Obywatelstwo: ', ' '+userCopy.userExtraInfo.nationality],
        		['PESEL: ', ' '+userCopy.userExtraInfo.pesel],
        		['Płeć: ', ' '+userCopy.userExtraInfo.sex],
        		['Dokument tożsamości: ', ' '+documentTypeLabel],
        		['Seria i numer: ', ' '+userCopy.userExtraInfo.document.number],
        		['Państwo wydające: ', ' '+userCopy.userExtraInfo.document.issuer],
//        		['Data wydania: ', ' '+issuedDate],
        		['Telefon kontaktowy: ', ' '+userCopy.phone],
        		['E-mail: ', ' '+userCopy.email],

        		        ]
        		    }

        		},
        		{text: '\n'},
        		{text: '\n'},
        		{
        		    table: {
        		        widths: ['*'],
        		        body: [
        		            ['Adres zamieszkania']
        		        ]
        		    }

        		},
        		{ layout: 'noBorders',
        		    table: {
        		        widths: ['*', '*'],
        		        body: [
//        		['Kraj: ', ' '+userCopy.userExtraInfo.livingAddress.country],
//        		['Województwo: ', ' '+userCopy.userExtraInfo.livingAddress.voivodeship],
//        		['Powiat: ', ' '+userCopy.userExtraInfo.livingAddress.poviat],
//        		['Gmina: ', ' '+userCopy.userExtraInfo.livingAddress.commune],
        		['Poczta: ', ' '+userCopy.userExtraInfo.livingAddress.postOffice],
        		['Kod pocztowy: ', ' '+userCopy.userExtraInfo.livingAddress.zipCode],
        		['Miejscowość: ', ' '+userCopy.userExtraInfo.livingAddress.city],
        		['Miejsce zamieszkania przed rozpoczęciem studiów: ', ' '+addressType],
        		['Ulica: ', ' '+street],
        		['Numer domu/nr mieszkania: ', ' '+userCopy.userExtraInfo.livingAddress.houseNumber],

        		        ]
        		    }

        		},
        		{text: '\n'},
        		{text: '\n'},
        		{text: '\n'},
        		{
        		    table: {
        		        widths: ['*'],
        		        body: [
        		            ['Adres do korespondencji - jeśli inny, niż adres zamieszkania']
        		        ]
        		    }

        		},
        		{ layout: 'noBorders',
        		    table: {
        		        widths: ['*', '*'],
        		        body: [
//        				['Kraj: ', ' '+userCopy.userExtraInfo.correspondenceAddress.country],
//                		['Województwo: ', ' '+userCopy.userExtraInfo.correspondenceAddress.voivodeship],
//                		['Powiat: ', ' '+userCopy.userExtraInfo.correspondenceAddress.poviat],
//                		['Gmina: ', ' '+userCopy.userExtraInfo.correspondenceAddress.commune],
                		['Poczta: ', ' '+userCopy.userExtraInfo.correspondenceAddress.postOffice],
                		['Kod pocztowy: ', ' '+userCopy.userExtraInfo.correspondenceAddress.zipCode],
                		['Miejscowość: ', ' '+userCopy.userExtraInfo.correspondenceAddress.city],
                		['Ulica: ', ' '+corrStreet],
                		['Numer domu/nr mieszkania: ', ' '+userCopy.userExtraInfo.correspondenceAddress.houseNumber],

        		        ]
        		    }

        		},
        		{text: '\n'},
        		{text: '\n'},
        		{
        		    table: {
        		        widths: ['*'],
        		        body: [
        		            ['Ukończona szkoła ponadgimnazjalna / średnia']
        		        ]
        		    }

        		},
        		{ layout: 'noBorders',
        		    table: {
        		        widths: ['*', '*'],
        		        body: [
        		['Miejscowość: ', ' '+userCopy.userExtraInfo.highSchoolInfo.certificatePlace],
        		['Rodzaj szkoły: ', ' '+userCopy.userExtraInfo.highSchoolInfo.highSchoolType],
        		['Pełna nazwa szkoły: ', ' '+userCopy.userExtraInfo.highSchoolInfo.highSchoolName],
        		['Rok ukończenia szkoły: ', ' '+userCopy.userExtraInfo.highSchoolInfo.finishingYear],
//        		['Okręgowa Komisja Egzaminacyjna: ', ' '+userCopy.userExtraInfo.highSchoolInfo.oke],
//        		['Miejscowośc wystawienia świadectwa dojrzałości: ', ' '+userCopy.userExtraInfo.highSchoolInfo.highSchoolCertificateCity],
        		['Numer świadectwa dojrzałości: ', ' '+userCopy.userExtraInfo.highSchoolInfo.highSchoolCertificateNumber],

        		        ]
        		    }

        		},
        		{text: '\n'},
        		{text: '\n'},
        		{
        		    table: {
        		        widths: ['*'],
        		        body: [
        		            ['Ankieta']
        		        ]
        		    }

        		},
        		{ layout: 'noBorders',
        		    table: {
        		        widths: ['*', '*'],
        		        body: [
        		['1. Czy podejmowałeś wcześniej studia w BWS\nim. J. Tyszkiewicza? ', ' '+ourStudent],
        		['2. Proszę wskazać główne źródło informacji o naszej Uczelni. ', ' '+ surv.howInfoLabel],
        		['3. Jeżeli Internet to jaki portal? ', ' '+surv.howInfoExtraLabel],
        		['4. Proszę podać najważniejszy powód wyboru naszej Uczelni ', ' '+surv.whyChooseLabel],
        		
        		        ]
        		    }

        		},
                         		{text: '\n'},
                         		{text: '\n'},
                         		{text: '\n'},
                         		{ layout: 'noBorders',
                                        		    table: {
                                        		        widths: ['*', '*'],
                                        		        body: [
                                        		[' ', '...........................'],
                                        		[' ', 'Podpis kandydata']
                                        		        ]
                                        		    }

                                        		}
        	]
}
}