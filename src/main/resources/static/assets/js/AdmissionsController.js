angular.module('recruit-user-app').controller('AdmissionsController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
    $scope.user = {}
    $scope.loadedAdmission = {}

    $scope.admission = {
                             "type" : "STACJONARNE",
                             "specializations" : [ {
                               "name" : ""
                             } ],
                           }

    $scope.admissionList = [];
    $scope.addSpecialization = function(){
        $scope.admission.specializations.push({specializationStatus: 'ZAREJESTROWANY'})
    }

    $scope.removeSpecialization = function(){
        if($scope.admission.specializations.length >1){
            $scope.admission.specializations.pop()
        }
    }

    $scope.archiveSpecialization = function(index){
        $scope.loadedAdmission.specializations[index].specializationStatus = 'ARCHIWALNY'
        $scope.updateAdmission(function(index){
            $scope.loadedAdmission.specializations[index].specializationStatus = 'ZAREJESTROWANY'
        }, index)
    }

    $scope.addAdmission = function(index){
            $http({
                url: '/admin/addAdmission',
                method: 'POST',
                data: $scope.admission,
                headers: {
                    "Content-Type": "application/json"
                }
            }).
            success(function(data, status, headers, config) {
                $rootScope.notifySuccess("Pomyślnie zapisano zmiany.")
                            loadAdmission()
            }).
            error(function(data, status, headers, config) {
                 if (data.errors) {
                     $rootScope.handleErrors(data.errors, "#error_")
                 } else {
                     $rootScope.handleErrors(data, "#error_")
                 }
            });
    }
    $scope.updateAdmission = function(_callback, index){
            $http({
                url: '/admin/addAdmission',
                method: 'POST',
                data: $scope.loadedAdmission,
                headers: {
                    "Content-Type": "application/json"
                }
            }).
            success(function(data, status, headers, config) {
                $rootScope.notifySuccess("Pomyślnie zapisano zmiany.")
                loadAdmission()
                    $("#edit-admission-modal").modal("hide")
            }).
            error(function(data, status, headers, config) {
                            if(_callback && typeof _callback === "function") {
                                _callback(index);
                            }
                 if(data.errorData){
                  if(data.errorData[0].field.indexOf('number_of_users') > -1){
                   $rootScope.notifyError('Dla danej specjalizacji istnieje: ' + data.errorData[0].field.substring(16) + ' aktywnych użytkowników ')
                   return;
                  }
                 }
                 if (data.errors) {
                     $rootScope.handleErrors(data.errors, "#error_modal_")
                 } else {
                     $rootScope.handleErrors(data, "#error_modal_")
                 }
            });
    }
    $scope.editAdmission = function(index){
        $scope.loadedAdmission = JSON.parse(JSON.stringify($scope.admissionList.admissions[index]))
        $scope.loadedAdmission.startDate= new Date($scope.loadedAdmission.startDate);
        $scope.loadedAdmission.endDate= new Date($scope.loadedAdmission.endDate);
        $scope.loadedAdmission.inaugurationDate= new Date($scope.loadedAdmission.inaugurationDate);
        $("#edit-admission-modal").modal("show")

    }

    function loadAdmission() {

        $http.get('/api/admissions/search/findAllWhereDateInBetween?today=' + new Date().format('isoDate')).
//                        $http.get('/data/admissions.json').
        success(function(data, status, headers, config) {
            $scope.admissionList = data._embedded
        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError("Wystąpił nie oczekiwany błąd")
        });
    };



    angular.element(document).ready(function() {
        try {
            $("#sidebar-nav").removeClass("active")
            $("#rekrutacje-li").addClass("active")

            loadAdmission()
        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }
    });

}]);