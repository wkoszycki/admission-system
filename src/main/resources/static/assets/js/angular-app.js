angular.module('recruit-user-app', ['angular-jwt']);

angular.module('recruit-user-app')
    .config(['$httpProvider', 'jwtOptionsProvider', function($httpProvider, jwtOptionsProvider) {
        // Please note we're annotating the function so that the $injector works when the file is minified
        jwtOptionsProvider.config({
            tokenGetter: function() {
                return Cookies.get('token') ? Cookies.get('token') : undefined;
            },
            authPrefix: ''
        });

        $httpProvider.interceptors.push('jwtInterceptor');
    }])
    .run(function($log, $rootScope, jwtHelper) {
        $rootScope.isLogged = false;

        $rootScope.doLogout = function() {
            Cookies.remove("token", {
                path: '/'
            })
            $rootScope.isLogged = false;

            if (window.location.pathname !== "/" && window.location.pathname !== "/index.html") {
                if (!$rootScope.isLoggedOut) {
                    $rootScope.isLoggedOut = true;
                    setTimeout(function() {
                        $rootScope.isLoggedOut = false;
                        window.location = "/";
                    }, 500);
                }
            }
        };

        $rootScope.$on('tokenHasExpired', function() {
            $rootScope.doLogout();
        });

        $rootScope.getUser = function() {
        if(token){
            return jwtHelper.decodeToken(token);
        }
                    return null;

        };

        $rootScope.isUser = function() {
        if(!token){
            return false;
        }
            var scopes = jwtHelper.decodeToken(token).scopes;
            return scopes.indexOf('USER') > -1;
        };

        $rootScope.isAdmin = function() {
                if(!token){
                    return false;
                }
            var scopes = jwtHelper.decodeToken(token).scopes;
            return scopes.indexOf('ADMIN') > -1 || scopes.indexOf('SUPER_ADMIN') > -1;
        };
        $rootScope.isSuperAdmin = function() {
                if(!token){
                    return false;
                }
            var scopes = jwtHelper.decodeToken(token).scopes;
            return scopes.indexOf('SUPER_ADMIN') > -1;
        };

        $rootScope.isAdminByToken = function(token) {
            var scopes = jwtHelper.decodeToken(token).scopes;
            return scopes.indexOf('ADMIN') > -1 || scopes.indexOf('SUPER_ADMIN') > -1;
        };

        $rootScope.notifySuccess = function(message) {
            $.notify({
                icon: 'ti-info-alt',
                message: message

            }, {
                type: 'success',
                z_index: 999999,
                timer: 4000
            });
        }
        $rootScope.notifyInfo = function(message) {
            $.notify({
                icon: 'ti-info-alt',
                message: message

            }, {
                type: 'info',
                z_index: 999999,
                timer: 4000
            });
        }

        $rootScope.cleanErrors = function() {
            $(".p_error").text("")
        }

        $rootScope.notifyError = function(message) {
            $.notify({
                icon: 'ti-face-sad',
                message: message

            }, {
                type: 'danger',
                z_index: 999999,
                timer: 4000
            });
        }
        $rootScope.sendErrrorToGA = function(err, line) {
            gtag('event', 'exception', {
                'description': line + " " + err,
                'fatal': false
            });
        }
        $rootScope.handleErrors = function(errors, prefix, errMsg) {
            if (!errors) {
                $rootScope.notifyError("Wystąpił nieznany błąd")
                return;
            }
            if (errMsg){
                $rootScope.notifyError(errMsg)
            } else {
                $rootScope.notifyError("Formularz zawiera błędne dane")
            }


            if (errors.errorData) {
                for (i = 0; i < errors.errorData.length; i++) {
                    if (errors.errorData[i].field) {
                        var errorMsg = ((errors.errorData[i].field.startsWith("NotNull")) ? userMessages["NotNull"] : userMessages[errors.errorData[i].field]);
                        var escapedDot = errors.errorData[i].field.replace(".","\\.").replace("[","_").replace("]","_")

                        $(prefix + escapedDot).text(errorMsg)
                    }
                }
                $("#error_" + errors.errorData[0].field).get(0).scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
                return;
            }

            for (i = 0; i < errors.length; i++) {
                if (errors[i].codes) {
                    var errorMsg = ((errors[i].codes[0].startsWith("NotNull")) ? userMessages["NotNull"] : userMessages[errors[i].codes[0]]);
                    var escapedDot = errors[i].field.replace(".","\\.").replace("[","_").replace("]","_")
                    $(prefix + escapedDot).text(errorMsg)
                }

            }

        }

        try {
            var token = Cookies.get("token");
            if (!!token === false) {
                $rootScope.doLogout();
                return;
            }

            var isExpired = jwtHelper.isTokenExpired(token);
            if (isExpired) {
                $rootScope.doLogout();
            }
            $rootScope.isLogged = true;
        } catch (error) {
            $log.error("Handled :" + error);
            $rootScope.doLogout();
        }

        try {
            if (window.location.pathname.indexOf("/user/") > -1) {
                if ($rootScope.isAdmin()) {
                    window.location = "/admin/index.html";
                } else if (!$rootScope.isUser()) {
                    $rootScope.doLogout();
                }
            }

            if (window.location.pathname.indexOf("/admin/") > -1) {
                if (!$rootScope.isAdmin()) {
                    $rootScope.doLogout();
                }
            }
            if ((window.location.pathname == "/" || window.location.pathname == "/index.html") && $rootScope.isLogged) {
                            if ($rootScope.isAdmin()) {
                                window.location = "/admin/index.html";
                            } else {
                               window.location = "/user/index.html";
                            }
            }
        } catch (error) {
            $log.error("Handled :" + error);
        }

        $rootScope.getDateTime = function(timeString) {
            return moment(timeString).isValid() ? moment(timeString) : null;
        }
    });