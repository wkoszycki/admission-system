angular.module('recruit-user-app').controller('AdminPanelController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
    $scope.user = {}
    $scope.countries = countries
    $scope.dateFrom = null
    $scope.dateTo = null
    $scope.isNewUser = false
    $scope.users = []
    $scope.selectAll = false
    $scope.bulkUpdate = {
        "status": "ZAREJESTROWANY",
        "ids": []
    }
    $scope.search = {
        "criteria": [{
                "key": "email",
                "type": "string",
                "operation": ":",
                "value": ""
            },
            {
                "key": "surname",
                "type": "string",
                "operation": ":",
                "value": ""
            },
            {
                "key": "status",
                "type": "enum_UserStatus",
                "operation": "!=",
                "value": "ARCHIWALNY"
            },
            {
                "key": "role",
                "type": "enum_Role",
                "operation": "=",
                "value": "ADMIN"
            },
            {
                "key": "role",
                "type": "enum_Role",
                "operation": "!=",
                "value": "SUPER_ADMIN"
            }
        ]
    }


    $scope.archiveUser = function(id) {
        var path = '/admin/archive/' + id
        $http.get(path).
        success(function(data, status, headers, config) {
            $rootScope.notifySuccess("Zarchiwizowano użytkownika, odśwież wyszukiwanie")
        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError(userMessages["unexpected_error"] + " " + data)
        });
    }
    $scope.addNewUser = function() {
          $scope.user = {created: new Date().format('isoDate'), role: 'ADMIN'}
          $scope.isNewUser = true
    }
    $scope.loadUser = function(id) {
        //        var path = '/data/user.json'
        var path = '/api/users/' + id
        $http.get(path).
        success(function(data, status, headers, config) {
            $scope.isNewUser = false
            $scope.user = data
            $scope.user.id = id
            $("#editModal").show()
        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError(userMessages["unexpected_error"] + " " + data)
        });
    };

    $scope.searchUsers = function() {
        var changed = false
        $http({
            url: '/admin/search',
            method: 'POST',
            data: $scope.search,
            headers: {
                "Content-Type": "application/json"
            }
        }).
        success(function(data, status, headers, config) {
            $rootScope.notifySuccess("Znaleziono: " + data.length + " użytkowników")
            $scope.users = data
        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError(userMessages["unexpected_error"] + " " + data)
            console.log('negative response status:' + status + ' data: ' + data);
        });
    };

    $scope.updateUser = function($event) {
    $rootScope.cleanErrors()
        try {
            $http({
                url: '/admin/edit',
                method: 'PUT',
                data: $scope.user,
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function(data, status, headers, config) {
                $rootScope.notifySuccess(userMessages["update_ok"])
                $('#editModal').modal('hide');
                $('#addNewModal').modal('hide');
//                $('body').removeClass('modal-open');
//                $('.modal-backdrop').remove();
            }).error(function(data, status, headers, config) {
                if (data.errors) {
                    $rootScope.handleErrors(data.errors, "#error_")
                } else {
                    $rootScope.handleErrors(data, "#error_")
                }
            });


        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }

    }


    angular.element(document).ready(function() {
        try {
            $("#sidebar-nav").removeClass("active")
            $("#students-li").addClass("active")
            $scope.searchUsers()
        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }
    });

}]);