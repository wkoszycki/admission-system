angular.module('recruit-user-app').controller('StudentsController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
    $scope.user = {}
    $scope.countries = countries
    $scope.dateFrom = null
    $scope.dateTo = null
    $scope.users = []
    $scope.selectAll = false
    $scope.bulkUpdate = {
        "status": "ZAREJESTROWANY",
        "ids": []
    }
    $scope.search = {
        "criteria": [{
                "key": "email",
                "type": "string",
                "operation": ":",
                "value": ""
            },
            {
                "key": "surname",
                "type": "string",
                "operation": ":",
                "value": ""
            },
            {
                "key": "userExtraInfo.pesel",
                "type": "string",
                "operation": ":",
                "value": ""
            },
            {
                "key": "userExtraInfo.livingAddress.city",
                "type": "string",
                "operation": ":",
                "value": ""
            },
            {
                "key": "specialization.id",
                "type": "long",
                "operation": "in",
                "value": []
            },
            {
                "key": "status",
                "type": "enum_UserStatus",
                "operation": "=",
                "value": ""
            },
            {
                "key": "role",
                "type": "enum_Role",
                "operation": "!=",
                "value": "ADMIN"
            },
            {
                "key": "created",
                "type": "date",
                "operation": ">",
                "value": ""
            },
            {
                "key": "created",
                "type": "date",
                "operation": "<",
                "value": ""
            },
            {
                "key": "role",
                "type": "enum_Role",
                "operation": "!=",
                "value": "SUPER_ADMIN"
            }
        ]
    }

    $scope.showExtraAddress = false

    $scope.admissionList = [];

    function loadAdmission() {

        $http.get('/api/admissions/search/findAllWhereDateInBetween?today=' + new Date().format('isoDate')).
        //                $http.get('/data/admissions.json').
        success(function(data, status, headers, config) {
            $scope.admissionList = data._embedded
        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError("Wystąpił nie oczekiwany błąd")
        });
    };

    $scope.onSelectAll = function() {
        $(".user_checked").prop("checked", $scope.selectAll);
    }
    $scope.updateBulk = function() {
        $scope.bulkUpdate.ids = []
        var elements = (document.getElementsByClassName('user_checked'));
                for (var i = 0; i < elements.length; i++) {
                    if(typeof(elements[i]) != 'undefined' && elements[i].checked){
                        $scope.bulkUpdate.ids.push(elements[i].getAttribute('aria-valuetext'))
                    }
                }
        if($scope.bulkUpdate.ids.length == 0){
            $rootScope.notifyInfo("Zaznacz użytkowników do zmiany.")
            return;
        }
          var cnf = confirm("Ta operacja zmieni status na:"+$scope.bulkUpdate.status + " dla: "+ $scope.bulkUpdate.ids.length+" użytkowników");
               if (cnf){
                $http({
                    url: '/admin/updateBulk',
                    method: 'PUT',
                    data: $scope.bulkUpdate,
                    headers: {
                        "Content-Type": "application/json"
                    }
                }).
                success(function(data, status, headers, config) {
                    $rootScope.notifySuccess("Pomyślnie zapisano zmiany.")
                    $scope.selectAll = false
                    $scope.bulkUpdate.ids =[]
                    $scope.searchUsers()

                }).
                error(function(data, status, headers, config) {
                    $rootScope.notifyError(userMessages["unexpected_error"] + " " + data)
                    console.log('negative response status:' + status + ' data: ' + data);
                    $scope.bulkUpdate.ids =[]
                });
                }

    }
    $scope.archiveUser = function(id) {
        var path = '/admin/archive/' + id
        $http.get(path).
        success(function(data, status, headers, config) {
            $rootScope.notifySuccess("Zarchiwizowano użytkownika, odśwież wyszukiwanie")
        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError(userMessages["unexpected_error"] + " " + data)
        });
    }
//    $scope.loadUser = function(id) {
//
//    }
    $scope.loadUser = function(id, _callback) {
        //        var path = '/data/user.json'
        var path = '/api/users/' + id
        $http.get(path).
        success(function(data, status, headers, config) {
            if (data.userExtraInfo) {
                exInfo = data.userExtraInfo
                if (exInfo.birthday) {
                    exInfo.birthday = new Date(exInfo.birthday)
                }
//                if (exInfo.document && exInfo.document.issued) {
//                    exInfo.document.issued = new Date(exInfo.document.issued)
//                }
                exInfo.isOurStudent = "" + exInfo.isOurStudent
                if (exInfo.survey) {
                    survey = exInfo.survey
                    if (survey.howInfo) {
                        survey.howInfo = "" + survey.howInfo
                    }
                    if (survey.howInfoExtra) {
                        survey.howInfoExtra = "" + survey.howInfoExtra
                    }
                    if (survey.whyChoose) {
                        survey.whyChoose = "" + survey.whyChoose
                    }
                }
            }
            if (data._embedded.specialization) {
                data.specialization = data._embedded.specialization
                data.specialization.id = "" + data.specialization.id
            }

            $scope.user = data
            $scope.user.id = id
            $scope.showExtraAddress = (($scope.user.userExtraInfo != null && $scope.user.userExtraInfo.correspondenceAddress != null)) ? true : false;
            $("#editModal").show()
            _callback();
        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError(userMessages["unexpected_error"] + " " + data)
        });
    };

    $scope.searchUsers = function() {
        var changed = false
        if ($scope.search.criteria[5].value == "ARCHIWALNY") {
              $scope.search.criteria[10] = {}
        }
        if ($scope.search.criteria[5].value == "PRZYJETY") {
              $scope.search.criteria[11] = {}
        }
        if ($scope.search.criteria[5].value == "no_selection") {
              $scope.search.criteria[5].value = ""
        }
        if ($scope.search.criteria[4].value[0]) {
            if ($scope.search.criteria[4].value[0] == "no_selection") {
                $scope.search.criteria[4].value = [];
            }
        }
        if ($scope.dateFrom) {
            $scope.search.criteria[7].value = new Date($scope.dateFrom)
        } else {
            $scope.search.criteria[7].value = null
        }
        if ($scope.dateTo) {
            $scope.search.criteria[8].value = new Date($scope.dateTo)
        } else {
            $scope.search.criteria[8].value = null
        }
        //        var path = '/api/users/'
        $http({
            url: '/admin/search',
            method: 'POST',
            data: $scope.search,
            headers: {
                "Content-Type": "application/json"
            }
        }).
        success(function(data, status, headers, config) {
            $rootScope.notifySuccess("Znaleziono: " + data.length + " użytkowników")
            $scope.users = data
                    $scope.search.criteria[10] = {
                        "key": "status",
                        "type": "enum_UserStatus",
                        "operation": "!=",
                        "value": "ARCHIWALNY"
                    }
                    $scope.search.criteria[11] = {
                        "key": "status",
                        "type": "enum_UserStatus",
                        "operation": "!=",
                        "value": "PRZYJETY"
                    }

        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError(userMessages["unexpected_error"] + " " + data)
            console.log('negative response status:' + status + ' data: ' + data);
                    $scope.search.criteria[10] = {
                        "key": "status",
                        "type": "enum_UserStatus",
                        "operation": "!=",
                        "value": "ARCHIWALNY"
                    }
                    $scope.search.criteria[11] = {
                        "key": "status",
                        "type": "enum_UserStatus",
                        "operation": "!=",
                        "value": "PRZYJETY"
                    }
        });
    };


    $scope.makePdf = function($event) {
        try {
            $scope.loadUser($scope.user.id, function(){
                pdfMake.createPdf(fillPdf($scope.user)).download("BWS_kwestionariusz.pdf")
            })

        } catch (e) {
            $rootScope.notifyError("Wystąpił błąd " + e)
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }

    }

    $scope.exportCsv = function(mode) {
        var csvData;
        var filename;
        if(mode ==='short'){
            csvData = "\ufeff" + convertToCsvMainFields($scope.users)
            filename = 'kandydaci_rekrutacja.csv';
        } else {
            csvData = "\ufeff" + convertToCsv($scope.users)
            filename = 'eksport_rekrutacja.csv';
        }
        var a = document.createElement("a");
        a.setAttribute('style', 'display:none;');
        document.body.appendChild(a);
        var blob = new Blob([csvData], {
            type: 'text/csv;charset=UTF-8'
        });
        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = filename
        a.click();
        return 'success';
    }

    function convertToCsvMainFields(users) {
        var separator = ';';
        var str = '';
        var row = "";
        var columns = [
            'imię',
            'nazwisko',
            'email',
            'telefon',
            'kierunek',
            'specjalizacja',
            'tryb'
        ]
        for (var i = 0; i < columns.length; i++) {
            //Now convert each value to string and comma-separated
            row += columns[i] + separator;
        }
        row = row.slice(0, -1);
        //append Label row with line break
        str += row + '\r\n';

        for (var i = 0; i < users.length; i++) {
            var u = users[i]
            var line = '';
            line += u.name + separator
            line += u.surname + separator
            line += u.email + separator
            line += u.phone + separator

            var spec = ((!u.specialization) ? {} : u.specialization);
            spec.admission = ((!spec.admission) ? {} : spec.admission);
            line += spec.admission.name + separator
            line += spec.name + separator
            line += spec.admission.type + separator

            str += line + '\r\n';
        }
        return str;
    }
    function convertToCsv(users) {

//        var array = typeof users != 'object' ? JSON.parse(users) : users;
        var separator = ';';
        var str = '';
        var row = "";
        var columns = [
            'imię',
            'nazwisko',
            'email',
            'telefon',
            'kierunek',
            'specjalizacja',
//            'tryb',
//            'Nazwisko rodowe',
            'Drugie imię',
//            'Imię ojca',
            'Państwo urodzenia',
            'Karta polaka',
//            'Imię matki',
            'Kod promocyjny',
            'Nardowość',
            'Pesel',
            'Płeć',
            'Typ dokumentu',
            'Numer dokumentu',
            'Wydawca dokumentu',
//            'Dokument data wydania',
//            'Kraj zamieszkania',
//            'Województwo',
//            'Powiat',
//            'Gmina',
            'Poczta',
            'Kod pocztowy',
            'Miasto',
            'Ulica',
            'Numer domu',
            'Typ miejscowści',
            'Świadectwo miejsce wydania',
            'Typ szkoy średniej',
            'Nazwa szkoły średniej',
            'Rok ukończenia',
            'Okręgowa komisja egzaminacyjna',
            'Szkoła średnia miasto',
            'Numer świadectwa dojrzałości',
            'Średnia ocen'
        ]
        for (var i = 0; i < columns.length; i++) {
            //Now convert each value to string and comma-separated
            row += columns[i] + separator;
        }
        row = row.slice(0, -1);
        //append Label row with line break
        str += row + '\r\n';

        for (var i = 0; i < users.length; i++) {
            var u = users[i]
            var line = '';
            line += u.name + separator
            line += u.surname + separator
            line += u.email + separator
            line += u.phone + separator

            var spec = ((!u.specialization) ? {} : u.specialization);
            spec.admission = ((!spec.admission) ? {} : spec.admission);
            var exInfo = ((!u.userExtraInfo) ? {} : u.userExtraInfo);
            exInfo.document = ((!exInfo.document) ? {} : exInfo.document);
            exInfo.livingAddress = ((!exInfo.livingAddress) ? {} : exInfo.livingAddress);
            exInfo.highSchoolInfo = ((!exInfo.highSchoolInfo) ? {} : exInfo.highSchoolInfo);

            line += spec.admission.name + separator
            line += spec.name + separator
//            line += spec.admission.type + separator
//            line += exInfo.familyName + separator
            line += exInfo.secondName + separator
//            line += exInfo.fatherName + separator
            line += exInfo.birthPlace + separator
            line += exInfo.polishCard + separator
//            line += exInfo.motherName + separator
            line += exInfo.promoCode + separator
            line += exInfo.nationality + separator
            line += exInfo.pesel + separator
            line += exInfo.sex + separator
            line += exInfo.document.type + separator
            line += exInfo.document.number + separator
            line += exInfo.document.issuer + separator
//            line += exInfo.document.issued + separator
//            line += exInfo.livingAddress.country + separator
//            line += exInfo.livingAddress.voivodeship + separator
//            line += exInfo.livingAddress.poviat + separator
//            line += exInfo.livingAddress.commune + separator
            line += exInfo.livingAddress.postOffice + separator
            line += exInfo.livingAddress.zipCode + separator
            line += exInfo.livingAddress.city + separator
            line += exInfo.livingAddress.street + separator
            line += exInfo.livingAddress.houseNumber + separator
            line += exInfo.livingAddress.addressType + separator
            line += exInfo.highSchoolInfo.certificatePlace + separator
            line += exInfo.highSchoolInfo.highSchoolType + separator
            line += exInfo.highSchoolInfo.highSchoolName + separator
            line += exInfo.highSchoolInfo.finishingYear + separator
            line += exInfo.highSchoolInfo.oke + separator
//            line += exInfo.highSchoolInfo.highSchoolCertificateCity + separator
            line += exInfo.highSchoolInfo.highSchoolCertificateNumber + separator
            line += exInfo.highSchoolInfo.gradeAverage + separator
            str += line + '\r\n';
        }
        return str;
    }
    $scope.updateUser = function($event) {
    $rootScope.cleanErrors()
        try {
            $http({
                url: '/admin/edit',
                method: 'PUT',
                data: $scope.user,
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function(data, status, headers, config) {
                $rootScope.notifySuccess(userMessages["update_ok"])
            }).error(function(data, status, headers, config) {
                if (data.errors) {
                    $rootScope.handleErrors(data.errors, "#error_")
                } else {
                    $rootScope.handleErrors(data, "#error_")
                }
            });


        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }

    }


    angular.element(document).ready(function() {
        try {
            $("#sidebar-nav").removeClass("active")
            $("#students-li").addClass("active")

            $scope.search.criteria[10] = {
                "key": "status",
                "type": "enum_UserStatus",
                "operation": "!=",
                "value": "ARCHIWALNY"
            }
                        $scope.search.criteria[11] = {
                "key": "status",
                "type": "enum_UserStatus",
                "operation": "!=",
                "value": "PRZYJETY"
            }
            $scope.searchUsers()
            loadAdmission()

        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }
    });

}]);