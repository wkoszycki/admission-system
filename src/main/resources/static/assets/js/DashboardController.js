angular.module('recruit-user-app').controller('DashboardController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {

    $scope.countBoxes = {
        box1: 0,
        box2: 0,
        box3: 0
    }
    $scope.legend = []
    $scope.countSurvey = {}
    $scope.countAddress = {
        village: {count: 0, percent: 0},
        city: {count: 0, percent: 0}
    }
    $scope.specializationsVsStudents = {}

    $scope.currentYear = new Date().getFullYear()
    $scope.admissionStart = $scope.currentYear + '-04-01'
    $scope.admissionStop = $scope.currentYear + '-10-31'
    $scope.search = {
        "criteria": [{
                "key": "userExtraInfo.livingAddress.addressType",
                "type": "enum_AddressType",
                "operation": "=",
                "value": ""
            },
            {
                "key": "specialization.id",
                "type": "long",
                "operation": "in",
                "value": []
            },
            {
                "key": "status",
                "type": "enum_UserStatus",
                "operation": "=",
                "value": ""
            },
            {
                "key": "role",
                "type": "enum_Role",
                "operation": "!=",
                "value": "ADMIN"
            },
            {
                "key": "created",
                "type": "date",
                "operation": ">",
                "value": $scope.admissionStart
            },
            {
                "key": "created",
                "type": "date",
                "operation": "<",
                "value": $scope.admissionStop
            }
        ]
    }
    $scope.countUsers = function(searchParam, callback) {
        $http({
            url: '/admin/count',
            method: 'POST',
            data: searchParam,
            headers: {
                "Content-Type": "application/json"
            }
        }).
        success(function(data, status, headers, config) {
            callback(data)
        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError(userMessages["unexpected_error"] + " " + data)
            console.log('negative response status:' + status + ' data: ' + data);
        });
    };

    $scope.countSpecializationsVsStudents = function(admissionStart, admissionStop, callback) {
        $http.get('/admin/countSpecializationsVsStudents/' + admissionStart + '/' + admissionStop).success(function(data, status, headers, config) {
//        $http.get('/data/chart.json').success(function(data, status, headers, config) {
            callback(data)
        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError(userMessages["unexpected_error"] + " " + data)
            console.log('negative response status:' + status + ' data: ' + data);
        });
    };

    $scope.countSurveyResults = function(admissionStart, admissionStop, callback) {
        $http.get('/admin/countSurveyResults/' + admissionStart + '/' + admissionStop).success(function(data, status, headers, config) {
            callback(data)
        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError(userMessages["unexpected_error"] + " " + data)
            console.log('negative response status:' + status + ' data: ' + data);
        });
    };

    angular.element(document).ready(function() {
        try {
            $("#sidebar-nav").removeClass("active")
            $("#index-li").addClass("active")
            var searchParamsBox1 = keepCloning($scope.search)
            var searchParamsBox2 = keepCloning($scope.search)
            var searchParamsBox3 = keepCloning($scope.search)
            searchParamsBox1.criteria[2].value = "ZAREJESTROWANY"
            searchParamsBox2.criteria[2].value = "KWESTIONARIUSZ_WYPELNIONY"
            searchParamsBox3.criteria[2].value = "PRZYJETY"
            $scope.countUsers(searchParamsBox1, function(resultData) {
                $scope.countBoxes.box1 = resultData.valueOf()
            })
            $scope.countUsers(searchParamsBox2, function(resultData) {
                $scope.countBoxes.box2 = resultData.valueOf()
            })
            $scope.countUsers(searchParamsBox3, function(resultData) {
                $scope.countBoxes.box3 = resultData.valueOf()
            })
            var fromCitySearchParam = keepCloning($scope.search)
            fromCitySearchParam.criteria[0].value = "MIASTO"
            var fromVillageSearchParam = keepCloning($scope.search)
            fromVillageSearchParam.criteria[0].value = "WIES"
            $scope.countUsers(fromCitySearchParam, function(resultData) {
                $scope.countAddress.city.count = resultData.valueOf()
                $scope.countUsers(fromVillageSearchParam, function(resultData2) {
                    $scope.countAddress.village.count = resultData2.valueOf()
                    var sum = $scope.countAddress.village.count +$scope.countAddress.city.count
                    $scope.countAddress.city.percent = Math.round($scope.countAddress.city.count / sum * 100)
                    $scope.countAddress.village.percent = Math.round($scope.countAddress.village.count / sum * 100)
                    Chartist.Pie('#chartAddressType', {
                        series: [$scope.countAddress.village.count, $scope.countAddress.city.count]
                    });
                })
            })
            $scope.countSpecializationsVsStudents($scope.admissionStart, $scope.admissionStop, function(resultData) {
                $scope.specializationsVsStudents = resultData.valueOf()
                $scope.initSpecializationChart()
            })
            $scope.countSurveyResults($scope.admissionStart, $scope.admissionStop, function(resultData) {
                $scope.countSurvey = resultData.valueOf()
//                $scope.countSurvey = {
//                                       "howInfo":[2,3,4,5,6,7,8],
//                                       "howInfoTotal":35,
//                                       "howInfoExtra":[2,3,4,5,6],
//                                       "howInfoExtraTotal":20,
//                                       "whyChoose":[2,3,4,5,5],
//                                       "whyChooseTotal":19
//                                     }
                $scope.initSurveyChart()
            })
        } catch (e) {
            console.error(e)
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }
    });

    function keepCloning(objectpassed) {
        if (objectpassed === null || typeof objectpassed !== 'object') {
            return objectpassed;
        }
        // give temporary-storage the original obj's constructor
        var temporaryStorage = objectpassed.constructor();
        for (var key in objectpassed) {
            temporaryStorage[key] = keepCloning(objectpassed[key]);
        }
        return temporaryStorage;
    }
    $scope.initSurveyChart = function() {
        $scope.countSurvey.howInfoPercent = []
        for (i = 0; i < $scope.countSurvey.howInfo.length; i++) {
           $scope.countSurvey.howInfoPercent[i] = Math.round($scope.countSurvey.howInfo[i] / $scope.countSurvey.howInfoTotal * 100)
        }
         Chartist.Pie('#chartHowInfo', {
             series: $scope.countSurvey.howInfo
         });

         $scope.countSurvey.howInfoExtraPercent = []
         for (i = 0; i < $scope.countSurvey.howInfoExtra.length; i++) {
            $scope.countSurvey.howInfoExtraPercent[i] = Math.round($scope.countSurvey.howInfoExtra[i] / $scope.countSurvey.howInfoExtraTotal * 100)
         }
          Chartist.Pie('#howInfoExtra', {
              series: $scope.countSurvey.howInfoExtra
          });
         $scope.countSurvey.whyChoosePercent = []
         for (i = 0; i < $scope.countSurvey.whyChoose.length; i++) {
            $scope.countSurvey.whyChoosePercent[i] = Math.round($scope.countSurvey.whyChoose[i] / $scope.countSurvey.whyChooseTotal * 100)
         }
         Chartist.Pie('#whyChoose', {
              series: $scope.countSurvey.whyChoose
         });
    }
    $scope.initSpecializationChart = function() {

        var data = {
            labels: ["2018-04","2018-05","2018-06","2018-07","2018-08","2018-09","2018-10"],
            series: [
            [0,0,0,0,0,0,0]
            ]
        };
        var d = $scope.specializationsVsStudents;
        var lastIndex = 0
        var idMapping = [

        ]
        var startingMonth = $scope.admissionStart.substring(5, 7)

        for (i = 0; i < d.length; i++) {
            var element = d[i]

            if (typeof idMapping[element.id] === 'undefined') {
                idMapping[element.id] = lastIndex
                $scope.legend[lastIndex] = {name: element.specName, index: lastIndex}
                lastIndex++
            }
            var month = element.yearMonth.substring(5, 7)
            if (month.startsWith(0)) {
                month = month.substring(1)
            }

            data.labels[month-startingMonth] = element.yearMonth
            if(typeof data.series[idMapping[element.id]] === 'undefined'){
              data.series[idMapping[element.id]] = [0,0,0,0,0,0,0]
            }
            data.series[idMapping[element.id]][month-startingMonth] = element.userCount
        }

        Chartist.Line('#chartSpecializationsVsStudents', data, {
                                                                 fullWidth: true,
                                                                 chartPadding: {
                                                                   right: 40
                                                                 }});

    }
}]);