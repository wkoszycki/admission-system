angular.module('recruit-user-app').controller('LoginController', ['$rootScope','$scope', '$http', function($rootScope, $scope, $http) {

    //Data objects
    $scope.loginData = {
        email: '',
        password: '',
    }

    $scope.resetData = {
        login: ''
    }

    $scope.user = {
        acceptTerms: true
    }



    $scope.resetPassword = function($event) {
        try {
            cleanErrors()
            $http({
                url: '/user-api/reset-password',
                method: 'POST',
                data: $scope.resetData,
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function(data, status, headers, config) {
                $rootScope.notifySuccess(userMessages["reset_ok"])
            }).error(function(data, status, headers, config) {
                $rootScope.notifyError(userMessages["reset_" + status])
                if (status == 400) {
                    $rootScope.handleErrors(data.errors, "#error_")
                }
            });

        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }

    }
    $scope.switchModals = function($event) {
        console.log('switchModals()');
        $("#loginModal").modal("hide")
        $("#resetModal").modal("show")
    }

    $scope.login = function($event) {
        try {
            $("#login_error").text()
            $http({
                url: '/login',
                method: 'POST',
                data: this.loginData
            }).success(function(data, status, headers, config) {
                $rootScope.notifySuccess('Zalogowano');
                setTimeout(function() {
                    Cookies.set("token", data.token, {
                        expires: 1,
                        path: "/"
                    })
                    if($rootScope.isAdminByToken(data.token)){
                        window.location = "admin/index.html";
                    }else{
                        window.location = "user/extraInfo.html";
                    }
                }, 1)
            }).error(function(data, status, headers, config) {
                if (status === 401) {
                    $("#login_error").text('Email oraz hasło są niepoprawne')
                } else {
                    $("#login_error").text('Wystąpił nie oczekiwany błąd')
                }
            });

        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }

    };

    angular.element(document).ready(function() {
        try {
            if (window.location.search == "?login") {
                $("#loginModal").modal("show")
            }
            if (window.location.search == "?reset-password") {
                $rootScope.notifySuccess(userMessages["reset-password"])
            }
            if (window.location.search == "?reset-password-failed") {
                $rootScope.notifySuccess(userMessages["reset-password-failed"])
            }

        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }
    });

}]);