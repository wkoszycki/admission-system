angular.module('recruit-user-app').controller('RegisterController', ['$rootScope','$scope', '$http', function($rootScope, $scope, $http) {

    $scope.countries = countries

    $scope.user = {
        acceptTerms: true
    }


    $scope.admissionList = [];

    function loadActiveAdmission() {

        $http.get('/api/admissions/search/findAllWhereDateInBetween?today=' + new Date().format('isoDate'))
        .success(function(data, status, headers, config) {
            $scope.admissionList = data._embedded
        }).
        error(function(data, status, headers, config) {
            $rootScope.notifyError("Wystąpił nie oczekiwany błąd")
        });
    };

    function cleanErrors() {
        $(".p_error").text("")
    }



    $scope.registerUser = function($event) {
        try {
            cleanErrors()
            $http({
                url: '/user-api/register',
                method: 'POST',
                data: $scope.user,
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function(data, status, headers, config) {
                $rootScope.notifySuccess(userMessages["rejestracja_ok"])
            }).error(function(data, status, headers, config) {
                if (status == 400) {
                    $rootScope.handleErrors(data.errors, "#error_")
                }
            });

        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }


    }

    angular.element(document).ready(function() {
        try {
            loadActiveAdmission()
        } catch (e) {
            $rootScope.sendErrrorToGA(e, e.lineNumber)
        }
    });
}]);