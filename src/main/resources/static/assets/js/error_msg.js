var userMessages = {};
userMessages["NotNull"] = "Pole wymagane";
userMessages["reset-password"] = "Nowe hasło zostało wysłane na podany adres email, zaloguj się używając nowego hasła";
userMessages["reset-password-failed"] = "Niepoprawny token resetu hasła";
//reset password
userMessages["Pattern.resetPasswordDTO.resetPhone"] = "Numer telefonu powinien składać się z 9 cyfr";
userMessages["Email.resetPasswordDTO.resetEmail"] = "Niepoprawny adres email";
userMessages["ExistInRepo.resetPasswordDTO.resetEmail"] = "Nieodnaleziono adresu email";
userMessages["reset_ok"] = "Na podany adres email został wysłany link aktywacyjny";
userMessages["reset_400"] = "Popraw dane formularza";
userMessages["reset_500"] = "Wystąpił nieoczekiwany błąd, spróbuj później";
//register
userMessages["rejestracja_ok"] = "Rejestracja przebiegła pomyślnie kliknij przycisk zaloguj.";
userMessages["rejestracja_400"] = "Popraw dane formularza";
userMessages["rejestracja_500"] = "Wystąpił nieoczekiwany błąd, spróbuj później";
userMessages["Pattern.userDTO.password"] = "Hasło powinno składać się z 8 znaków 1 cyfry i 1 dużej litery"
userMessages["Email.userDTO.email"] = "Niepoprawny adres email"
userMessages["Size.userDTO.surname"] = "Nazwisko musi zawierać min. 2 znaki"
userMessages["Pattern.userDTO.phone"] = "Numer telefonu powinien składać się z 9 cyfr"
userMessages["Size.userDTO.name"] = "Nieprawidłowe imię"
userMessages["ExistInRepo.userDTO.email"] = "Podany email już istnieje"
userMessages["AssertTrue.userDTO.acceptTerms"] = "Zgoda wymagana"
userMessages["Matches.userDTO.passwordRetype"] = "Hasło musi się zgadzać"

//    extraInfo form
userMessages["birthday"] = "Niepoprawna data urodzenia"
userMessages["name"] = "Błąd w polu Imię"
userMessages["surname"] = "Błąd w polu Nazwisko"
userMessages["email"] = "Błąd w polu E-mail"
userMessages["status"] = "Błędne dane"
userMessages["password"] = "Błędne dane"
userMessages["phone"] = "Błąd w polu Telefon"
userMessages["role"] = "Błędne dane"
userMessages["active"] = "Błędne dane"
userMessages["created"] = "Błędne dane"

userMessages["userExtraInfo_birthday"] = "Błąd w polu Data urodzenia"
userMessages["userExtraInfo_familyName"] = "Błąd w polu Nazwisko rodowe"
userMessages["userExtraInfo_secondName"] = "Błąd w polu Drugie imię"
userMessages["userExtraInfo_fatherName"] = "Błąd w polu Imię ojca"
userMessages["userExtraInfo_birthPlace"] = "Błąd w polu Państwo urodzenia"
userMessages["userExtraInfo_motherName"] = "Błąd w polu Imię matki"
userMessages["userExtraInfo_promoCode"] = "Błędne dane"
userMessages["userExtraInfo_nationality"] = "Błąd w polu Obywatelstwo"
userMessages["userExtraInfo_pesel"] = "Błąd w polu PESEL"
userMessages["userExtraInfo_polishCard"] = "Błąd w polu Karta Polaka"
userMessages["userExtraInfo_sex"] = "Błąd w polu Płeć"
userMessages["userExtraInfo_isOurStudent"] = "Błędne dane"
userMessages["userExtraInfo_genericInfo"] = "Błędne dane"
userMessages["userExtraInfo_isLocked"] = "Błędne dane"

userMessages["userExtraInfo_survey_id"] = "Błąd w polu Ankiety"
userMessages["userExtraInfo_survey_howInfo"] = "Błąd w polu Anliety"
userMessages["userExtraInfo_survey_howInfoExtra"] = "Błąd w polu Ankiety"
userMessages["userExtraInfo_survey_whyChoose"] = "Błąd w polu Ankiety"

userMessages["userExtraInfo_document_id"] = "Błędne dane"
userMessages["userExtraInfo_document_type"] = "Błąd w polu Dokument tożsamości"
userMessages["userExtraInfo_document_number"] = "Błąd w polu Seria i numer"
userMessages["userExtraInfo_document_issuer"] = "Błąd w polu Państwo wydające"
userMessages["userExtraInfo_document_issued"] = "Błąd w polu Data wydania dokumentu"
userMessages["type"] = "Błąd w polu Dokument tożsamości"
userMessages["number"] = "Błąd w polu Seria i numer"
userMessages["issuer"] = "Błąd w polu Państwo wydające"
userMessages["issued"] = "Błąd w polu Data wydania dokumentu"

userMessages["userExtraInfo_livingAddress_id"] = "Błędne dane"
userMessages["userExtraInfo_livingAddress_country"] = "Błąd w polu Adres zamieszkania - Kraj"
userMessages["userExtraInfo_livingAddress_voivodeship"] = "Błąd w polu Adres zamieszkania - Województwo"
userMessages["userExtraInfo_livingAddress_poviat"] = "Błąd w polu Adres zamieszkania - Powiat"
userMessages["userExtraInfo_livingAddress_commune"] = "Błąd w polu Adres zamieszkania - Gmina"
userMessages["userExtraInfo_livingAddress_postOffice"] = "Błąd w polu Adres zamieszkania - Poczta"
userMessages["userExtraInfo_livingAddress_zipCode"] = "Błąd w polu Adres zamieszkania - Kod pocztowy"
userMessages["userExtraInfo_livingAddress_city"] = "Błąd w polu Adres zamieszkania - Miasto"
userMessages["userExtraInfo_livingAddress_street"] = "Błąd w polu Adres zamieszkania - Ulica"
userMessages["userExtraInfo_livingAddress_houseNumber"] = "Błąd w polu Adres zamieszkania - Nr domu/nr mieszkania"
userMessages["userExtraInfo_livingAddress_addressType"] = "Błąd w polu Adres zamieszkania - Miejsce zamieszkania przed rozpoczęciem studiów"

userMessages["userExtraInfo_correspondenceAddress_id"] = "Błędne dane"
userMessages["userExtraInfo_correspondenceAddress_country"] = "Błąd w polu Adres zamieszkania - Kraj"
userMessages["userExtraInfo_correspondenceAddress_voivodeship"] = "Błąd w polu Adres zamieszkania - Województwo"
userMessages["userExtraInfo_correspondenceAddress_poviat"] = "Błąd w polu Adres zamieszkania - Powiat"
userMessages["userExtraInfo_correspondenceAddress_commune"] = "Błąd w polu Adres zamieszkania - Gmina"
userMessages["userExtraInfo_correspondenceAddress_postOffice"] = "Błąd w polu Adres zamieszkania - Poczta"
userMessages["userExtraInfo_correspondenceAddress_zipCode"] = "Błąd w polu Adres zamieszkania - Kod pocztowy"
userMessages["userExtraInfo_correspondenceAddress_city"] = "Błąd w polu Adres zamieszkania - Miasto"
userMessages["userExtraInfo_correspondenceAddress_street"] = "Błąd w polu Adres zamieszkania - Ulica"
userMessages["userExtraInfo_correspondenceAddress_houseNumber"] = "Błąd w polu Adres zamieszkania - Nr domu/nr mieszkania"
userMessages["userExtraInfo_correspondenceAddress_addressType"] = "Błąd w polu Adres zamieszkania - Miejsce zamieszkania przed rozpoczęciem studiów"

userMessages["userExtraInfo_highSchoolInfo_certificatePlace"] = "Błąd w polu Matura - Miejscowość"
userMessages["userExtraInfo_highSchoolInfo_highSchoolType"] = "Błąd w polu Rodzaj szkoły"
userMessages["userExtraInfo_highSchoolInfo_highSchoolName"] = "Błąd w polu Pełna nazwa szkoły"
userMessages["userExtraInfo_highSchoolInfo_finishingYear"] = "Błąd w polu Rok ukończenia szkoły"
userMessages["userExtraInfo_highSchoolInfo_oke"] = "Błąd w polu Okręgowa Komisja Egzaminacyjna"
userMessages["userExtraInfo_highSchoolInfo_highSchoolCertificateCity"] = "Błąd w polu Miejscowośc wydania świadectwa dojrzałości"
userMessages["userExtraInfo_highSchoolInfo_highSchoolCertificateNumber"] = "Błąd w polu Numer świadectwa dojrzałości"
userMessages["userExtraInfo_highSchoolInfo_gradeAverage"] = "Przykładowa średnia to: 4.0"
//extra info notifications
userMessages["update_ok"] = "Dane zostały poprawnie zapisane."
userMessages["pdf_ok"] = "Dane zostały poprawnie zapisane. Edycja została zablokowana."
//admin add update admission
userMessages["startDate"] = "Błędne dane"
userMessages["endDate"] = "Błędne dane"
userMessages["inaugurationDate"] = "Błędne dane"
userMessages["specializations[0]_name"] = "Błędne dane"
userMessages["specializations[1]_name"] = "Błędne dane"
userMessages["specializations[2]_name"] = "Błędne dane"
userMessages["specializations[3]_name"] = "Błędne dane"
userMessages["specializations[4]_name"] = "Błędne dane"
userMessages["specializations[5]_name"] = "Błędne dane"
userMessages["specializations[6]_name"] = "Błędne dane"
userMessages["specializations[7]_name"] = "Błędne dane"