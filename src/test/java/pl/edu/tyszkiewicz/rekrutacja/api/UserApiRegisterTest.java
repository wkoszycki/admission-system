package pl.edu.tyszkiewicz.rekrutacja.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.edu.tyszkiewicz.rekrutacja.TestUtil;
import pl.edu.tyszkiewicz.rekrutacja.domain.Specialization;
import pl.edu.tyszkiewicz.rekrutacja.dto.UserDTO;
import pl.edu.tyszkiewicz.rekrutacja.repository.SpecializationRepository;
import pl.edu.tyszkiewicz.rekrutacja.service.MailService;

import java.util.Iterator;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {"admission.mail.enabled=false"})
public class UserApiRegisterTest {

    @Autowired
    SpecializationRepository specializationRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    Long specializationId;

    @Autowired
    UserApi userApi;

    @Mock
    private MailService mailServiceMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        userApi.mailService = mailServiceMock;
        final Iterator<Specialization> specializationIterator = specializationRepository.findAll().iterator();
        assertTrue(specializationIterator.hasNext());
        specializationId = specializationIterator.next().getId();
    }

    @Test
    public void invalidEmail() throws Exception {
        UserDTO user = TestUtil.createUserDTO();
        user.setSpecializationId(specializationId);
        user.setEmail("joe.gmail@");


        final MvcResult result = mockMvc.perform(post("/user-api/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isBadRequest()).andReturn();

        assertTrue(result.getResolvedException().getMessage().contains("Email.userDTO.email"));
    }
    @Test
    public void onlySpecID() throws Exception {
        UserDTO user = UserDTO.builder()
                .specializationId(specializationId)
                .build();


        mockMvc.perform(post("/user-api/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isBadRequest());
    }
    @Test
    public void emailExists() throws Exception {
        UserDTO user = TestUtil.createUserDTO();
        user.setSpecializationId(specializationId);


        mockMvc.perform(post("/user-api/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isCreated());
        // post same user
        mockMvc.perform(post("/user-api/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void passwordDoesntMatch() throws Exception {
        UserDTO user = TestUtil.createUserDTO();
        user.setSpecializationId(specializationId);
        user.setPasswordRetype("otherPass");


        mockMvc.perform(post("/user-api/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void invalidPhone() throws Exception {
        UserDTO user = TestUtil.createUserDTO();
        user.setSpecializationId(specializationId);
        user.setPhone("111");


        mockMvc.perform(post("/user-api/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
    @Test
    public void shouldRegisterUser() throws Exception {
        UserDTO user = TestUtil.createUserDTO();
        user.setEmail(user.getEmail().toUpperCase());
        user.setSpecializationId(specializationId);

        mockMvc.perform(post("/user-api/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isCreated());

        verify(mailServiceMock, times(1)).notifyAdminAboutRegister(any());
        verify(mailServiceMock, times(1)).notifyUserAboutRegister(user.getEmail().toLowerCase(), user.getName());
    }
    @Test
    public void invalidPassword() throws Exception {
        String password ="mustHaveNUmber";
        UserDTO user = TestUtil.createUserDTO();
        user.setSpecializationId(specializationId);
        user.setPassword(password);
        user.setPasswordRetype(password);

        mockMvc.perform(post("/user-api/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(user)))
                .andDo(print())
                .andExpect(status().isBadRequest());
        verify(mailServiceMock, never()).notifyUserAboutRegister(any(), any());
        verify(mailServiceMock, never()).notifyAdminAboutRegister(any());

    }

}
