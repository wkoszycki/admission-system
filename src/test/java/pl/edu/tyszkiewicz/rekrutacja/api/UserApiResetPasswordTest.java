package pl.edu.tyszkiewicz.rekrutacja.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.edu.tyszkiewicz.rekrutacja.TestUtil;
import pl.edu.tyszkiewicz.rekrutacja.domain.ResetPassword;
import pl.edu.tyszkiewicz.rekrutacja.domain.Specialization;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;
import pl.edu.tyszkiewicz.rekrutacja.dto.ResetPasswordDTO;
import pl.edu.tyszkiewicz.rekrutacja.repository.ResetPasswordRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.Iterator;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {"admission.mail.enabled=false"})
public class UserApiResetPasswordTest {

    @Autowired
    ResetPasswordRepository resetPasswordRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private CrudRepository<Specialization, Long> specializationRepository;


    @Before
    public void setUp() throws Exception {


    }

    @Test
    public void invalidEmail() throws Exception {

        mockMvc.perform(post("/user-api/reset-password")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(new ResetPasswordDTO("asdasd", "111222333"))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void phoneNotMatchingDB() throws Exception {
        final Iterator<Specialization> specializationIterator = specializationRepository.findAll().iterator();
        assertTrue(specializationIterator.hasNext());

        User user=TestUtil.createUser().specialization(specializationIterator.next()).build();
        User validUser = userRepository.save(user);

        mockMvc.perform(post("/user-api/reset-password")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(new ResetPasswordDTO(validUser.getEmail(), "111222334"))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void happyPath() throws Exception {
        final Iterator<Specialization> specializationIterator = specializationRepository.findAll().iterator();
        assertTrue(specializationIterator.hasNext());


        User user=TestUtil.createUser().specialization(specializationIterator.next()).build();
        User validUser = userRepository.save(user);


        mockMvc.perform(post("/user-api/reset-password")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(new ResetPasswordDTO(user.getEmail().toUpperCase(), user.getPhone()))))
                .andExpect(status().isOk());


        final Iterator<ResetPassword> passwordIterator = resetPasswordRepository.findAllByUserIdOrderByRequestedDesc(validUser.getId()).iterator();
        assertTrue(passwordIterator.hasNext());
        final ResetPassword resetPassword = passwordIterator.next();
        assertTrue(resetPassword.getRequested().isAfter(LocalDateTime.now().minusMinutes(1L)));

        mockMvc.perform(get("/user-api/reset-password/" + resetPassword.getUuid()))
                .andExpect(status().is3xxRedirection());

        assertNotEquals(userRepository.findById(validUser.getId()).get().getPassword(), "eightLetter8");
    }

}
