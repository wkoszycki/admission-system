package pl.edu.tyszkiewicz.rekrutacja.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.edu.tyszkiewicz.rekrutacja.TestUtil;
import pl.edu.tyszkiewicz.rekrutacja.domain.*;
import pl.edu.tyszkiewicz.rekrutacja.repository.SpecializationRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.UserRepository;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {"admission.mail.enabled=false"})
public class LoggedUserApiEditTest {

    @Autowired
    SpecializationRepository specializationRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    Long specializationId;
    TestUtil testUtil = new TestUtil();

    @Before
    public void setUp() throws Exception {
        final Iterator<Specialization> specializationIterator = specializationRepository.findAll().iterator();
        assertTrue(specializationIterator.hasNext());
        specializationId = specializationIterator.next().getId();
    }


    //    @Ignore
    @Test
    public void shouldEditExistingUserWithExistingEntities() throws Exception {
        Address address = TestUtil.createAddress().build();

        Document document = TestUtil.createDocument().build();
        HighSchoolInfo highSchoolInfo = TestUtil.createHighSchoolInfo().build();

        UserExtraInfo userExtraInfo = TestUtil.createUserInfo().build();
        userExtraInfo.setDocument(document);
        userExtraInfo.setLivingAddress(address);
        userExtraInfo.setHighSchoolInfo(highSchoolInfo);

        final User user = TestUtil.createUser()
                .specialization(specializationRepository.findById(specializationId).get())
                .userExtraInfo(userExtraInfo)
                .build();
        User savedUser =userRepository.save(user);

        final User userFromRequest = User.builder()
                .id(savedUser.getId())
                .name(user.getName())
                .surname("Changed surname")
                .email(user.getEmail())
                .phone(user.getPhone())
                .userExtraInfo(userExtraInfo)
                .build();

//        userExtraInfo.getLivingAddress().setPoviat("Changed poviat");

        mockMvc.perform(put("/user/edit")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("Authorization", testUtil.getAuthorizationToken(mockMvc, objectMapper, savedUser.getEmail()))
                .content(objectMapper.writeValueAsString(userFromRequest)))
                .andExpect(status().isOk());
        final User assertedUser = userRepository.findById(savedUser.getId()).get();
        assertEquals("Changed surname", assertedUser.getSurname());
//        assertEquals("Should update changes on cascade objects", "Changed poviat", assertedUser.getUserExtraInfo().getLivingAddress().getPoviat());
    }

    @Test
    public void shouldFailOnValidation() throws Exception {
        Address address = TestUtil.createAddress().build();

        Document document = TestUtil.createDocument().build();
        HighSchoolInfo highSchoolInfo = TestUtil.createHighSchoolInfo().build();

        UserExtraInfo userExtraInfo = TestUtil.createUserInfo().build();
        userExtraInfo.setDocument(document);
        userExtraInfo.setLivingAddress(address);
        userExtraInfo.setHighSchoolInfo(highSchoolInfo);

        final User user = TestUtil.createUser()
                .specialization(specializationRepository.findById(specializationId).get())
                .userExtraInfo(userExtraInfo)
                .build();
        User savedUser =userRepository.save(user);

        final User userFromRequest = User.builder()
                .id(savedUser.getId())
                .name(user.getName())
                .surname(user.getSurname())
                .email(user.getEmail())
                .phone(user.getPhone())
                .userExtraInfo(userExtraInfo)
                .build();

        userExtraInfo.getLivingAddress().setZipCode("invalid zip");

        mockMvc.perform(put("/user/edit")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("Authorization", testUtil.getAuthorizationToken(mockMvc, objectMapper,savedUser.getEmail()))
                .content(objectMapper.writeValueAsString(userFromRequest)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

}
