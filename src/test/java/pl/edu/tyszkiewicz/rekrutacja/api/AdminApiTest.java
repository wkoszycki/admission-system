package pl.edu.tyszkiewicz.rekrutacja.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.edu.tyszkiewicz.rekrutacja.TestUtil;
import pl.edu.tyszkiewicz.rekrutacja.domain.*;
import pl.edu.tyszkiewicz.rekrutacja.dto.*;
import pl.edu.tyszkiewicz.rekrutacja.repository.AdmissionRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.ResetPasswordRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.SpecializationRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.UserRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.query.SearchCriteria;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {"admission.mail.enabled=false"})
public class AdminApiTest {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserRepository userRepository;
    @Autowired
    AdminApi adminApi;
    @Autowired
    SpecializationRepository specializationRepository;
    @Autowired
    AdmissionRepository admissionRepository;
    @Autowired
    ResetPasswordRepository resetPasswordRepository;
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;
    TestUtil testUtil = new TestUtil();

    @Ignore
    @Test
    public void encodePassword() {
//        String pass1 = "eightLetter8";
//        assertEquals("$2a$11$YMMkHOs43bQtzlSNUBKYvuATDwDfi85ze/KI5Ey5.KKk0n8WaCSSq", passwordEncoder.encode(pass1));
        String pass2 = "TySZk13WiCz";
        final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(11);
        System.out.println("passwordEncoder.encode(Ml3Z3nCs) =" + passwordEncoder.encode("Ml3Z3nCs"));
        System.out.println("passwordEncoder.encode(3C2oReDS) =" + passwordEncoder.encode("3C2oReDS"));
        System.out.println("passwordEncoder.encode(8q3CLM1y) =" + passwordEncoder.encode("8q3CLM1y"));
        System.out.println("passwordEncoder.encode(ruB4aW8N) =" + passwordEncoder.encode("ruB4aW8N"));
        System.out.println("passwordEncoder.encode(9D3b6dCS) =" + passwordEncoder.encode("9D3b6dCS"));

        assertTrue(passwordEncoder.matches(pass2, "$2a$11$fMiprHTYobhloCXZ5isd9.BecshSzg1U7mW2K8lFk8fCOMhekb/aO"));
        assertTrue(passwordEncoder.matches(pass2, "$2a$11$w34DSQQXFbTW0VDuUkcbfOLhtbbGl8FVaLp5Nsy2qB.KuBIhNSP8G"));
    }

    @Test
    public void testcountSpecializationsVsStudents() throws Exception {
        userRepository.deleteAll();
        final Iterator<Specialization> specializations = specializationRepository.findAll().iterator();
        final Specialization spec1 = specializations.next();
        final Specialization spec2 = specializations.next();
        LocalDate creationTime = LocalDate.of(2001, 3, 2);
        final User user1 = userRepository.save(TestUtil.createUser().specialization(spec1).created(creationTime).build());
        final User user2 = userRepository.save(TestUtil.createUser().specialization(spec1).created(creationTime.plusDays(2)).build());

        final User user3 = userRepository.save(TestUtil.createUser().specialization(spec2).created(creationTime.plusDays(3)).build());

        final User user4 = userRepository.save(TestUtil.createUser().specialization(spec1).created(creationTime.plusMonths(1)).build());

        final User user5 = userRepository.save(TestUtil.createUser().specialization(spec2).created(creationTime.plusMonths(1)).build());

        final List<SpecializationsVsStudentsDTO> result = adminApi.countSpecializationsVsStudents(creationTime, creationTime.plusMonths(2));
        result.sort(Comparator.comparing(SpecializationsVsStudentsDTO::getYearMonth));

        System.out.println("Arrays.toString(result.toArray()) = " + Arrays.toString(result.toArray()));
        Assert.assertEquals(4, result.size());
        Assert.assertEquals(2, result.get(0).getUserCount().longValue());
        Assert.assertEquals(spec1.getId(), result.get(0).getId());
        Assert.assertEquals(1, result.get(1).getUserCount().longValue());
    }

    @Test
    public void testcountSpecializationsVsStudentsBinding() throws Exception {

        final User user = userRepository.save(TestUtil.createUser().role(Role.ADMIN).build());

        SearchDTO search = new SearchDTO();
        search.setCriteria(Arrays.asList(new SearchCriteria("email", "string", ":", user.getEmail())
                , new SearchCriteria("surname", "string", ":", user.getSurname())));

        final MvcResult result = mockMvc.perform(get("/admin/countSpecializationsVsStudents/2018-04-01/2018-10-31")
                .header("Authorization", testUtil.getAuthorizationToken(mockMvc, objectMapper, user.getEmail()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
    }
    @Test
    public void testSearchFunction() throws Exception {

        final User user = userRepository.save(TestUtil.createUser().role(Role.ADMIN).build());

        SearchDTO search = new SearchDTO();
        search.setCriteria(Arrays.asList(new SearchCriteria("email", "string", ":", user.getEmail())
                , new SearchCriteria("surname", "string", ":", user.getSurname())));

        final MvcResult result = mockMvc.perform(post("/admin/search")
                .header("Authorization", testUtil.getAuthorizationToken(mockMvc, objectMapper, user.getEmail()))
                .content(objectMapper.writeValueAsString(search))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(objectMapper.writeValueAsString(new User[]{user}), result.getResponse().getContentAsString());
        // clean up
        userRepository.delete(user);
    }

    @Test
    public void testSearchFunction_InvalidParams() throws Exception {
        final User user = userRepository.save(TestUtil.createUser().role(Role.ADMIN).build());
        final MvcResult result = invokeSearch(Arrays.asList(new SearchCriteria("email", "string", ":", "invalid@invalid.com")
                , new SearchCriteria("surname", "string", ":", "invalid_surnam")), user);
        assertEquals(objectMapper.writeValueAsString(new User[0]), result.getResponse().getContentAsString());
    }

    @Test
    public void testSearchFunction_SearchByDate() throws Exception {
        LocalDate created = LocalDate.of(2018, 5, 20);
        final User user = userRepository.save(TestUtil.createUser().created(created).role(Role.ADMIN).build());

        final MvcResult result = invokeSearch(Arrays.asList(new SearchCriteria("email", "string", "=", user.getEmail())
                , new SearchCriteria("created", "date", "<", "2018-05-20T00:00:00.000Z")
                , new SearchCriteria("created", "date", ">", "2018-05-13T00:00:00.000Z")
        ), user);
        assertEquals(objectMapper.writeValueAsString(new User[]{user}), result.getResponse().getContentAsString());
    }

    @Test
    public void testSearchFunction_JsonBinding() throws Exception {
        final User user = userRepository.save(TestUtil.createUser().role(Role.ADMIN).build());

        final MvcResult result = mockMvc.perform(post("/admin/search")
                .header("Authorization", testUtil.getAuthorizationToken(mockMvc, objectMapper, user.getEmail()))
                .content(getJson())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(objectMapper.writeValueAsString(new User[0]), result.getResponse().getContentAsString());
    }

    @Test
    public void testAddAdmission_happyPath() throws Exception {
        final User user = userRepository.save(TestUtil.createUser().role(Role.ADMIN).build());
        String name = UUID.randomUUID().toString();

        final AdmissionDTO admission = AdmissionDTO.builder()
                .name(name)
                .endDate(LocalDate.now())
                .startDate(LocalDate.now())
                .inaugurationDate(LocalDate.now())
                .type(StudyType.NIESTACJONARNE)
                .specializations(Arrays.asList(SpecializationDTO.builder()
                        .name("spec1")
                        .build(),SpecializationDTO.builder()
                        .name("spec2")
                        .build()))
                .build();

        final MvcResult result = mockMvc.perform(post("/admin/addAdmission")
                .header("Authorization", testUtil.getAuthorizationToken(mockMvc, objectMapper, user.getEmail()))
                .content(objectMapper.writeValueAsString(admission))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();

        final Admission admissionEntity = admissionRepository.findByName(name).get();
        final Specialization spec1 = admissionEntity.getSpecializations().get(0);

        final AdmissionDTO updateAdmission = AdmissionDTO.builder()
                .id(admissionEntity.getId())
                .name(admissionEntity.getName())
                .endDate(admissionEntity.getEndDate())
                .startDate(admissionEntity.getStartDate())
                .inaugurationDate(admissionEntity.getStartDate())
                .type(admissionEntity.getType())
                .specializations(Collections.singletonList(SpecializationDTO.builder()
                        .id(spec1.getId())
                        .name(spec1.getName())
                        .build()))
                .build();

    }

    @Test
    public void testEditUser_happyPath() throws Exception {
        final User user = userRepository.save(TestUtil.createUser().role(Role.ADMIN)
                .userExtraInfo(TestUtil.createUserInfo().isLocked(false).build()).build());
        user.getUserExtraInfo().setIsLocked(true);
        user.getUserExtraInfo().setGenericInfo(Collections.emptyList());
        user.getUserExtraInfo().setCorrespondenceAddress(TestUtil.createAddress().build());


        final MvcResult result = mockMvc.perform(put("/admin/edit")
                .header("Authorization", testUtil.getAuthorizationToken(mockMvc, objectMapper, user.getEmail()))
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
        //set same for comparision id
        user.getUserExtraInfo().getCorrespondenceAddress().setId(userRepository.findById(user.getId()).get().getUserExtraInfo().getCorrespondenceAddress().getId());
        assertEquals(objectMapper.writeValueAsString(user), result.getResponse().getContentAsString());

    }

    @Test
    public void testUpdateBulk() throws Exception {
        final User admin = userRepository.save(TestUtil.createUser().role(Role.ADMIN).build());
        final User user1 = userRepository.save(TestUtil.createUser().role(Role.USER).build());
        final User user2 = userRepository.save(TestUtil.createUser().role(Role.USER).build());

        BulkUpdateDTO bulk = BulkUpdateDTO.builder()
                .status(UserStatus.PRZYJETY)
                .ids(Arrays.asList(user1.getId(), user2.getId()))
                .build();

        String bulkString = objectMapper.writeValueAsString(bulk);
        final MvcResult result = mockMvc.perform(put("/admin/updateBulk")
                .header("Authorization", testUtil.getAuthorizationToken(mockMvc, objectMapper, admin.getEmail()))
                .content(bulkString)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();

        user1.setStatus(bulk.getStatus());
        user2.setStatus(bulk.getStatus());
        assertEquals(objectMapper.writeValueAsString(new User[]{user1, user2}), result.getResponse().getContentAsString());
    }

    @Test
    public void testSearchFunction_SearchEnumsDatesSpecialization() throws Exception {
        resetPasswordRepository.deleteAll();
        userRepository.deleteAll();
        final Iterator<Specialization> allSpec = specializationRepository.findAll().iterator();
        Specialization spec1 = allSpec.next();
        Specialization spec2 = allSpec.next();
        Specialization spec3 = allSpec.next();

        LocalDate created1 = LocalDate.of(2018, 5, 10);
        LocalDate created2 = LocalDate.of(2018, 5, 15);
        LocalDate created3 = LocalDate.of(2018, 5, 20);

        final User adminUser = userRepository.save(TestUtil.createUser().role(Role.ADMIN).build());
        //different creation date
        final User user1 = userRepository.save(TestUtil.createUser()
                .surname("aa")
                .status(UserStatus.ARCHIWALNY)
                .specialization(spec1).created(created1).build());
        final User user2 = userRepository.save(TestUtil.createUser()
                .surname("bb")
                .status(UserStatus.ARCHIWALNY)
                .specialization(spec2)
                .created(created2).build());
        final User user3 = userRepository.save(TestUtil.createUser().status(UserStatus.ARCHIWALNY)
                .surname("cc")
                .specialization(spec2)
                .created(created3).build());
        //different status
        final User user4 = userRepository.save(TestUtil.createUser().status(UserStatus.ZAREJESTROWANY)
                .surname("dd")
                .specialization(spec2).created(created3).build());
        //different specId
        final User user5 = userRepository.save(TestUtil.createUser().status(UserStatus.ARCHIWALNY)
                .surname("ee")
                .specialization(spec3)
                .created(created3).build());

        MvcResult searchByStatus = invokeSearch(Arrays.asList(
                new SearchCriteria("status", "enum_UserStatus", "=", "ARCHIWALNY")
        ), adminUser);
        final List<User> users = Arrays.asList(user1, user2, user3, user5);
        assertEquals(objectMapper.writeValueAsString(users.toArray()), searchByStatus.getResponse().getContentAsString());


        MvcResult searchByStatusAndSpec = invokeSearch(Arrays.asList(
                new SearchCriteria("status", "enum_UserStatus", "=", "ARCHIWALNY")
                , new SearchCriteria("specialization.id", "long", "in", Arrays.asList(spec1.getId(), spec2.getId()))
        ), adminUser);
        final List<User> users2 = Arrays.asList(user1, user2, user3);
        assertEquals(objectMapper.writeValueAsString(users2.toArray()), searchByStatusAndSpec.getResponse().getContentAsString());
        MvcResult searchByStatusAndSpecAndDates = invokeSearch(Arrays.asList(
                new SearchCriteria("status", "enum_UserStatus", "=", "ARCHIWALNY")
                , new SearchCriteria("specialization.id", "long", "in", Arrays.asList(spec1.getId(), spec2.getId()))
                , new SearchCriteria("created", "date", ">", "2018-05-13T22:00:00.000Z")
                , new SearchCriteria("created", "date", "<", "2018-05-22T22:00:00.000Z")
        ), adminUser);

        final List<User> users3 = Arrays.asList(user2, user3);
        assertEquals("Searching by dates didn't return expected result", objectMapper.writeValueAsString(users3.toArray()), searchByStatusAndSpecAndDates.getResponse().getContentAsString());
    }

    private MvcResult invokeSearch(Iterable<SearchCriteria> criteria, User adminUser) throws Exception {
        SearchDTO search = new SearchDTO();
        search.setCriteria(criteria);
        return mockMvc.perform(post("/admin/search")
                .header("Authorization", testUtil.getAuthorizationToken(mockMvc, objectMapper, adminUser.getEmail()))
                .content(objectMapper.writeValueAsString(search))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
    }


    private String getJson() {
        return "{\n" +
                "  \"criteria\": [\n" +
                "    {\n" +
                "      \"key\": \"email\",\n" +
                "      \"type\": \"string\",\n" +
                "      \"operation\": \":\",\n" +
                "      \"value\": \"mail\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"key\": \"surname\",\n" +
                "      \"type\": \"string\",\n" +
                "      \"operation\": \":\",\n" +
                "      \"value\": \"naz\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"key\": \"userExtraInfo.pesel\",\n" +
                "      \"type\": \"string\",\n" +
                "      \"operation\": \":\",\n" +
                "      \"value\": \"pesel\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"key\": \"userExtraInfo.livingAddress.city\",\n" +
                "      \"type\": \"string\",\n" +
                "      \"operation\": \":\",\n" +
                "      \"value\": \"miejsc\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"key\": \"specialization.id\",\n" +
                "      \"type\": \"long\",\n" +
                "      \"operation\": \"in\",\n" +
                "      \"value\": [\n" +
                "        \"6\",\n" +
                "        \"7\",\n" +
                "        \"8\"\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"key\": \"status\",\n" +
                "      \"type\": \"enum_UserStatus\",\n" +
                "      \"operation\": \"=\",\n" +
                "      \"value\": \"ZAREJESTROWANY\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"key\": \"created\",\n" +
                "      \"type\": \"date\",\n" +
                "      \"operation\": \"<\",\n" +
                "      \"value\": \"2018-05-02T22:00:00.000Z\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"key\": \"created\",\n" +
                "      \"type\": \"date\",\n" +
                "      \"operation\": \">\",\n" +
                "      \"value\": \"2018-05-17T22:00:00.000Z\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
    }
}