/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.edu.tyszkiewicz.rekrutacja;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.edu.tyszkiewicz.rekrutacja.domain.User;
import pl.edu.tyszkiewicz.rekrutacja.security.common.LoginResponse;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Joe Grandja
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SecurityConfigTests {


    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    User validUser;

    @Before
    public void setUp() throws Exception {
        // create dummy user for authentication
        validUser = User.builder().email("mail@mail.com").password("eightLetter8").build();
    }

    @Test
    public void accessUnprotected() throws Exception {
        this.mockMvc.perform(get("/index.html"))
                .andExpect(status().isOk());
    }

    @Test
    public void accessProtectedRedirectsToLogin() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/user/extraInfo.html"))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }

    @Test
    public void loginUser() throws Exception {
        this.mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                        "    \"email\": \"MAIL@mail.com\",\n" +
                        "    \"password\": \"eightLetter8\"\n" +
                        "}"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void loginInvalidUser() throws Exception {
        this.mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                        "    \"email\": \"invalid\",\n" +
                        "    \"password\": \"invalid\"\n" +
                        "}"))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }

    @Test
    public void loginUserAccessProtected() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                        "    \"email\": \"mail@mail.com\",\n" +
                        "    \"password\": \"eightLetter8\"\n" +
                        "}"))
                .andExpect(status().isOk())
                .andReturn();

        MockHttpSession httpSession = (MockHttpSession) mvcResult.getRequest().getSession(true);
        String token = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), LoginResponse.class).getToken();
        this.mockMvc.perform(get("/user/extraInfo.html").header("Authorization", token)
                .session(httpSession))
                .andExpect(status().isOk());
    }
    @Test
    public void loginUserAccessProtectedAdminResources() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                        "    \"email\": \"mail@mail.com\",\n" +
                        "    \"password\": \"eightLetter8\"\n" +
                        "}"))
                .andExpect(status().isOk())
                .andReturn();

        MockHttpSession httpSession = (MockHttpSession) mvcResult.getRequest().getSession(true);
        String token = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), LoginResponse.class).getToken();
        this.mockMvc.perform(get("/admin/index.html")
                .header("Authorization", token)
                .session(httpSession))
                .andExpect(status().isForbidden());
    }
    @Test
    public void loginAdminAccessProtectedAdminResources() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                        "    \"email\": \"koszycki.wojciech@gmail.com\",\n" +
                        "    \"password\": \"eightLetter8\"\n" +
                        "}"))
                .andExpect(status().isOk())
                .andReturn();

        MockHttpSession httpSession = (MockHttpSession) mvcResult.getRequest().getSession(true);
        String token = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), LoginResponse.class).getToken();
        this.mockMvc.perform(get("/admin/index.html").header("Authorization", token)
                .session(httpSession))
                .andExpect(status().isOk());
    }

}
