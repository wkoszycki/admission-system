package pl.edu.tyszkiewicz.rekrutacja.domain;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import pl.edu.tyszkiewicz.rekrutacja.TestUtil;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class UserTest {

    @Test
    public void equalsTest() throws Exception {
        final User userBeforeUpdate = TestUtil.createUser().id(20L).build();
        final User updateUser = TestUtil.createUser().id(20L).name("blablabla").build();

        assertEquals(userBeforeUpdate, updateUser);
    }
}
