package pl.edu.tyszkiewicz.rekrutacja.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import pl.edu.tyszkiewicz.rekrutacja.TestUtil;
import pl.edu.tyszkiewicz.rekrutacja.domain.*;
import pl.edu.tyszkiewicz.rekrutacja.exception.ErrorData;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserIsLocked;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserNotFoundException;
import pl.edu.tyszkiewicz.rekrutacja.exception.UserValidationException;
import pl.edu.tyszkiewicz.rekrutacja.repository.query.MyUserPredicatesBuilder;
import pl.edu.tyszkiewicz.rekrutacja.service.UserService;

import javax.validation.ConstraintViolation;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {"admission.mail.enabled=false"})
public class UserServiceTest {

    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;

    @Autowired
    SpecializationRepository specializationRepository;


    @Test
    public void shouldFindUserUsingPredicate() {
        User testUser = TestUtil.createUser().surname(UUID.randomUUID().toString()).build();
        final User savedUser = userRepository.save(testUser);


        final Iterable<User> users = userRepository.findAll(QUser.user.surname.eq(testUser.getSurname()));
        final User userFound = users.iterator().next();
        assertEquals(savedUser.getId(), userFound.getId());
        assertEquals(testUser.getSurname(), userFound.getSurname());
    }

    @Test
    public void givenPartialFirst_whenGettingListOfUsers_thenCorrect() {
        User.UserBuilder testUser = TestUtil.createUser().surname(UUID.randomUUID().toString()).name("Joe");
        final User userJoe = userRepository.save(testUser.email("test@purpose.com").build());
        final User userTom = userRepository.save(testUser.name("Tom").build());

        MyUserPredicatesBuilder builder = new MyUserPredicatesBuilder().with("name", "string", ":", "jo");

        Iterable<User> results = userRepository.findAll(builder.build());

        assertThat(results, hasItem(userJoe));
        assertThat(results, not(contains(userTom)));

    }

    @Test
    public void givenWholePhone_whenGettingListOfUsers_thenExactOneUser() {
        User.UserBuilder testUser = TestUtil.createUser().surname(UUID.randomUUID().toString()).name("Joe");
        final User userJoe = userRepository.save(testUser.build());

        MyUserPredicatesBuilder builder = new MyUserPredicatesBuilder()
                .with("email", "string", ":", userJoe.getEmail())
                .with("surname", "string", ":", userJoe.getSurname());

        Iterator<User> results = userRepository.findAll(builder.build()).iterator();
        User foundedUser = results.next();
        assertEquals(foundedUser.getId(), userJoe.getId());
        assertFalse(results.hasNext());
    }

    @Test
    public void shouldUpdateSameEntity() throws UserNotFoundException, UserIsLocked, UserValidationException {
        final User savedUser = userRepository.save(TestUtil.createUser().build());

        Document document = TestUtil.createDocument().build();
        UserExtraInfo userExtraInfo = TestUtil.createUserInfo().build();
        userExtraInfo.setDocument(document);
        savedUser.setUserExtraInfo(userExtraInfo);

        userService.updateUserInfo(savedUser, false, UserStatus.KWESTIONARIUSZ_WYPELNIONY,false);

        final User byId = userRepository.findById(savedUser.getId()).get();

        assertNotNull(byId.getUserExtraInfo());
        assertNotNull(byId.getUserExtraInfo().getUser());
        Document documentFromDB = byId.getUserExtraInfo().getDocument();
        assertEquals(documentFromDB.getNumber(), document.getNumber());
        assertEquals(byId.getStatus(), UserStatus.KWESTIONARIUSZ_WYPELNIONY);
    }
    @Test
    public void shouldUpdateSameEntityWhenTopLevelNotChanged() throws UserNotFoundException, UserIsLocked, UserValidationException {
        final User savedUser = userRepository.save(TestUtil.createUser()
                .userExtraInfo(TestUtil.createUserInfo()
                        .isLocked(true)
                        .build())
                .build());

        savedUser.getUserExtraInfo().setIsLocked(false);
        final User user = userService.updateUserInfo(savedUser, false, UserStatus.KWESTIONARIUSZ_WYPELNIONY,true);

        assertFalse(user.getUserExtraInfo().getIsLocked());
    }

    @Test(expected = UserValidationException.class)
    public void shouldNotUpdateBaseUserInfo() throws UserNotFoundException, UserIsLocked, UserValidationException {
        final User savedUser = userRepository.save(TestUtil.createUser().build());

        UserExtraInfo userExtraInfo = new UserExtraInfo();
//        userExtraInfo.setFamilyName("familyname");
        userExtraInfo.setBirthday(LocalDate.of(2000,5,20));
        savedUser.setUserExtraInfo(userExtraInfo);

        userService.updateUserInfo(savedUser, false, UserStatus.ZAREJESTROWANY,false);
    }

    @Test
    public void shouldFailonValidationWhenSubObjectsNull() throws UserNotFoundException, UserIsLocked {
        final User savedUser = userRepository.save(TestUtil.createUser().build());
        try {
            userService.updateUserInfo(savedUser, true, UserStatus.KWESTIONARIUSZ_WYPELNIONY,false);
            Assert.fail("Should fail on validation");
        } catch (UserValidationException e) {
            assertThat(e.getErrors().getErrorData().stream()
                    .map(ErrorData::getField)
                    .collect(Collectors.toList()), containsInAnyOrder("object_nulls"));
        }

    }

    @Test
    public void shouldReturnCorrectValidationFields() throws UserNotFoundException, UserIsLocked {
        final User savedUser = userRepository.save(TestUtil.createUser().build());

        UserExtraInfo userExtraInfo = TestUtil
                .createUserInfo()
                .highSchoolInfo(TestUtil.createHighSchoolInfo().build())
                .livingAddress(TestUtil.createAddress().build())
                .document(TestUtil.createDocument().build())
                .build();
        // set invalid data in all sub objects
        savedUser.setSurname("1");
//        userExtraInfo.setFamilyName("1");
//        userExtraInfo.setFatherName("1");
//        userExtraInfo.getLivingAddress().setVoivodeship("1");
        userExtraInfo.getHighSchoolInfo().setCertificatePlace("1");
        userExtraInfo.getDocument().setIssuer("1");
        savedUser.setUserExtraInfo(userExtraInfo);
        List<ErrorData> expectedErrors = getErrorData(
                "surname"
//                , "userExtraInfo_familyName"
//                , "userExtraInfo_fatherName"
                , "userExtraInfo_livingAddress_voivodeship"
                , "userExtraInfo_highSchoolInfo_certificatePlace"
                , "userExtraInfo_document_issuer"
        );


        try {
            userService.updateUserInfo(savedUser, false, UserStatus.ZAREJESTROWANY,false);
            Assert.fail("should end up in catch clause");
        } catch (UserValidationException e) {
            assertThat(e.getErrors().getErrorData(), containsInAnyOrder(expectedErrors.toArray()));
            e.printStackTrace();
        }
    }

    @Test
    public void validationShouldValidateDeepObjects() throws Exception {
        UserExtraInfo userExtraInfo = TestUtil
                .createUserInfo()
                .highSchoolInfo(TestUtil.createHighSchoolInfo()
                        .highSchoolName("2")
                        .build())
                .livingAddress(TestUtil.createAddress()
                        .zipCode(null)
//                        .voivodeship("1")
                        .build())
                .document(TestUtil.createDocument().build())
                .build();

        final Set<ConstraintViolation<Serializable>> violations = userService.validate(userExtraInfo);

        assertThat(violations.stream()
                .map(v -> v.getPropertyPath().toString())
                .collect(Collectors.toList()), containsInAnyOrder(
                "highSchoolInfo.highSchoolName", "livingAddress.zipCode"));
    }

    @Test
    public void shouldPrintPdfForFullyFilledUser() throws Exception {
        User validUser = userRepository.save(
                TestUtil.createUser()
                        .specialization(specializationRepository.findAll().iterator().next())
                        .userExtraInfo(TestUtil
                                .createUserInfo()
                                .highSchoolInfo(TestUtil.createHighSchoolInfo().build())
                                .livingAddress(TestUtil.createAddress().build())
                                .document(TestUtil.createDocument().build())
                                .survey(TestUtil.createSurvey().build())
                                .build())
                        .build());
        User updatedUser = userService.updateUserInfo(validUser, true, UserStatus.KWESTIONARIUSZ_WYPELNIONY,false);
        assertTrue(updatedUser.getUserExtraInfo().getIsLocked());
        assertEquals(UserStatus.KWESTIONARIUSZ_WYPELNIONY, updatedUser.getStatus());
        assertNotNull(updatedUser.getSpecialization().getAdmission().getName());
    }

    private List<ErrorData> getErrorData(String... fields) {
        List<ErrorData> errorData = new ArrayList<>();
        for (String field : fields) {
            errorData.add(new ErrorData(field));
        }
        return errorData;
    }


}
