package pl.edu.tyszkiewicz.rekrutacja;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.edu.tyszkiewicz.rekrutacja.domain.*;
import pl.edu.tyszkiewicz.rekrutacja.dto.UserDTO;
import pl.edu.tyszkiewicz.rekrutacja.security.common.LoginRequest;
import pl.edu.tyszkiewicz.rekrutacja.security.common.LoginResponse;

import java.time.LocalDate;
import java.util.Random;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TestUtil {

    private String token = null;

    public static User.UserBuilder createUser() {
        return User.builder()
                .name("Joe")
                .surname(UUID.randomUUID().toString())
                .email(UUID.randomUUID() + "@unittest.com")
                .password("$2a$11$YMMkHOs43bQtzlSNUBKYvuATDwDfi85ze/KI5Ey5.KKk0n8WaCSSq")
                .phone("111222333")
                .role(Role.USER)
                .created(LocalDate.now())
                .status(UserStatus.ZAREJESTROWANY);
    }

    public static Survey.SurveyBuilder createSurvey() {
        return Survey.builder()
                .howInfo(2)
                .howInfoExtra(3)
                .whyChoose(2);
    }

    public static UserExtraInfo.UserExtraInfoBuilder createUserInfo() {
        return UserExtraInfo.builder()
                .birthday(LocalDate.of(1998, 2, 2))
//                .familyName("family name")
                .secondName("second")
//                .fatherName("fatherName")
                .birthPlace("country of birth")
//                .motherName("Urszula")
                .promoCode("AAABBB")
                .nationality("Polskie")
                .pesel("123881111000")
                .sex(Sex.MEZCZYZNA)
                .isOurStudent(false)
                .isLocked(false);
    }

    public static Document.DocumentBuilder createDocument() {
        return Document.builder()
                .type(DocumentType.DOWOD_OSOBISTY)
                .number("test data number")
                .issuer("test data issuer");
    }

    public static Address.AddressBuilder createAddress() {
        return Address.builder()
//                .country("POL")
//                .voivodeship("test data voivodeship")
//                .poviat("test data poviat")
//                .commune("test data commune")
                .postOffice("test data postOffice")
                .zipCode("00-000")
                .city("test data city")
                .street("test data street")
                .houseNumber("test data houseNumber")
                .addressType(AddressType.MIASTO);
    }

    public static HighSchoolInfo.HighSchoolInfoBuilder createHighSchoolInfo() {
        return HighSchoolInfo.builder()
                .certificatePlace("test data")
                .highSchoolType("test data")
                .highSchoolName("test data")
                .finishingYear("2009")
//                .oke("test data")
//                .highSchoolCertificateCity("test data")
                .highSchoolCertificateNumber("test data");
//                .gradeAverage("test data");
    }

    public static UserDTO createUserDTO() {
        return UserDTO.builder()
                .name("Joe")
                .surname("Lampart")
                .email(UUID.randomUUID() + "@unittest.com")
                .password("eightLetter8")
                .passwordRetype("eightLetter8")
                .phone("111222333")
                .role(Role.USER)
                .acceptTerms(true)
                .build();
    }

    public String getAuthorizationToken(MockMvc mockMvc, ObjectMapper objectMapper, String email) throws Exception {
        return getAuthorizationToken(mockMvc, objectMapper, email, "eightLetter8");
    }

    private String getAuthorizationToken(MockMvc mockMvc, ObjectMapper objectMapper, String email, String password) throws Exception {
        if (token == null) {
            MvcResult mvcResult = mockMvc.perform(post("/login")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(new LoginRequest(email, password))))
                    .andExpect(status().isOk())
                    .andReturn();

            token = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), LoginResponse.class).getToken();
        }
        return token;
    }

    static String generateRandomZip() {
        Random rand = new Random();
        String zip = "";
        while (!zip.matches("\\d{2}-\\d{3}")){
            zip = rand.nextInt(99) + "-" + rand.nextInt(999);
        }
        return zip;
    }

//    public static void main(String[] args) {
//        for (int i = 0; i < 1000; i++) {
//            System.out.println(TestUtil.generateRandomZip());
//        }
//    }

}
