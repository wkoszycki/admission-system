package pl.edu.tyszkiewicz.rekrutacja;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import pl.edu.tyszkiewicz.rekrutacja.domain.*;
import pl.edu.tyszkiewicz.rekrutacja.repository.ResetPasswordRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.SpecializationRepository;
import pl.edu.tyszkiewicz.rekrutacja.repository.UserRepository;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import static java.util.UUID.randomUUID;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.with;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static pl.edu.tyszkiewicz.rekrutacja.TestUtil.generateRandomZip;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"admission.mail.enabled=false"})
public class ApplicationBrowserIT extends FluentTest {

    @Override
    public WebDriver newWebDriver() {
        return new ChromeDriver();
    }

    @Autowired
    UserRepository userRepository;

    @LocalServerPort
    private int serverPort;

    @Autowired
    ResetPasswordRepository resetPasswordRepository;
    @Autowired
    SpecializationRepository specializationRepository;


    @BeforeClass
    public static void setUp() throws Exception {
        WebDriverManager.chromedriver().setup();
    }

    private String getUrl() {
        return "http://localhost:" + serverPort;
    }

    @Test
    public void loginExistingUser() {
        User validUser = userRepository.save(
                TestUtil.createUser()
                        .specialization(specializationRepository.findAll().iterator().next())
                        .userExtraInfo(TestUtil
                                .createUserInfo()
                                .highSchoolInfo(TestUtil.createHighSchoolInfo().build())
                                .livingAddress(TestUtil.createAddress().build())
                                .document(TestUtil.createDocument().build())
                                .survey(TestUtil.createSurvey().build())
                                .build())
                        .build());

        login(validUser.getEmail(), "eightLetter8");
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> el("#extra-info-title").conditions().enabled());
        assertThat(find("#topbar-logged").texts()).contains("Witaj "+validUser.getName()+" "+validUser.getSurname()+" !");
        Address address1 = validUser.getUserExtraInfo().getLivingAddress();
        await().atMost(15, TimeUnit.SECONDS).untilPredicate((fl) -> el("#livingAddress_zipCode").conditions().displayed());
        await().atMost(15, TimeUnit.SECONDS).untilPredicate((fl) -> $("#livingAddress_zipCode").values().contains(address1.getZipCode()));
//        await().atMost(5, TimeUnit.SECONDS).untilPredicate((fl) -> $("#livingAddress_poviat").values().contains(address1.getPoviat()));

        String zipCOde = generateRandomZip();
//        String poviat = randomUUID().toString();
        $("#livingAddress_zipCode").fill().with(zipCOde);
//        $("#livingAddress_poviat").fill().with(poviat);
        $("#saveButton").click();
        await().atMost(5, TimeUnit.SECONDS).untilPredicate((fl) ->
                $("span", with("data-notify")
                        .equalTo("message"))
                        .texts()
                        .contains("Dane zostały poprawnie zapisane."));
//        //page reload
        getDriver().navigate().refresh();
        await().atMost(15, TimeUnit.SECONDS).untilPredicate((fl) -> $("#livingAddress_zipCode").values().contains(zipCOde));
//        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $("#livingAddress_poviat").values().contains(poviat));
        $("#printButton").click();
        await().atMost(5, TimeUnit.SECONDS).untilPredicate((fl) ->
                $("span", with("data-notify")
                        .equalTo("message"))
                        .texts()
                        .contains("Dane zostały poprawnie zapisane. Edycja została zablokowana."));
        User userAfterUpdate = userRepository.findById(validUser.getId()).get();
        assertTrue(userAfterUpdate.getUserExtraInfo().getIsLocked());
        assertEquals(UserStatus.KWESTIONARIUSZ_WYPELNIONY, userAfterUpdate.getStatus());
    }


    void login(String username, String password) {
        goTo(getUrl());
        assertThat(find(".sel-page-header").texts()).contains("Rekrutacja Online");
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $("#login-modal").present());
        $("#login-modal").click();
        await().atMost(5, TimeUnit.SECONDS).untilPredicate((fl) -> el("#loginModal").conditions().displayed());
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> el("#login-modal-email").conditions().enabled());
        $("#login-modal-email").fill().with(username);
        $("#login-modal-password").fill().with(password);
        $("#loginButton").click();
    }

    @Test
    public void registerNewUser_wholeFLow() {
        goTo(getUrl());
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $(".admission-box").size() >= 4);
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $(".specialization-option").size() >= 10);
        String pass = "SUpaer81273";
        String mail = randomUUID().toString() + "@mail.com";
        $("#name").fill().with("abcc");
        $("#surname").fill().with("abcc");
        $("#phone").fill().with("111222333");
        $("#email").fill().with(mail);
        $("#password").fill().with(pass);
        $("#password-retype").fill().with(pass);
        $(".specialization-option").first().click();
        $("#registerSubmitButton").click();
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) ->
                $("span", with("data-notify")
                        .equalTo("message"))
                        .texts()
                        .contains("Rejestracja przebiegła pomyślnie kliknij przycisk zaloguj."));

        login(mail, pass);
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> el("#extra-info-title").conditions().enabled());
        assertThat(el("#email").conditions().enabled()).isFalse();
        $("#surname").fill().with("a");
        $("#phone").fill().with("12233");
        $("#document-number").fill().with("1");
//        $("#livingAddress_poviat").fill().with("1");
        $("#saveButton").click();
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $("#error_surname").texts().contains("Błąd w polu Nazwisko"));
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $("#error_phone").texts().contains("Błąd w polu Telefon"));
//        assertThat($("#error_userExtraInfo_livingAddress_poviat").texts()).contains("Błąd w polu Adres zamieszkania - Powiat");
        assertThat($("#error_userExtraInfo_document_number").texts()).contains("Błąd w polu Seria i numer");

    }

    @Test
    public void registerNewUser_errorMessages() {
        goTo(getUrl());
        //proper admission and specialization data loading
        await().atMost(5, TimeUnit.SECONDS).untilPredicate((fl) -> $(".admission-box").size() >= 4);
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $(".specialization-option").size() >= 10);

        //Required fields
        $("#acceptTerms").click();
        $("#registerSubmitButton").click();
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $("#error_name").texts().contains("Pole wymagane"));

        assertThat($("#error_name").texts()).contains("Pole wymagane");
        assertThat($("#error_surname").texts()).contains("Pole wymagane");
        assertThat($("#error_email").texts()).contains("Pole wymagane");
        assertThat($("#error_phone").texts()).contains("Pole wymagane");
        assertThat($("#error_password").texts()).contains("Pole wymagane");
        assertThat($("#error_acceptTerms").texts()).contains("Zgoda wymagana");
        //when invalid data filled
        $("#name").fill().with("a");
        $("#surname").fill().with("a");
        $("#password").fill().with("short");
        $("#email").fill().with("invalid@");
        $("#phone").fill().with("11122233");
        $("#registerSubmitButton").click();
        //then should show format check messages
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $("#error_name").texts().contains("Nieprawidłowe imię"));
//        data-notify="message"
        assertThat($("span", with("data-notify").equalTo("message")).present());
        assertThat($("#error_surname").texts()).contains("Nazwisko musi zawierać min. 2 znaki");
        assertThat($("#error_email").texts()).contains("Niepoprawny adres email");
        assertThat($("#error_phone").texts()).contains("Numer telefonu powinien składać się z 9 cyfr");
        assertThat($("#error_password").texts()).contains("Hasło powinno składać się z 8 znaków 1 cyfry i 1 dużej litery");
        assertThat($("#error_passwordRetype").texts()).contains("Hasło musi się zgadzać");

        $("#name").fill().with("abcc");
        $("#surname").fill().with("abcc");
        $("#phone").fill().with("111222333");
        $("#email").fill().with("mail@mail.com");
        $("#password").fill().with("SUpaer81273");
        $("#password-retype").fill().with("SUpaer81273");
        $("#acceptTerms").click();
        $(".specialization-option").first().click();
        //Bussiness logic checks
        $("#registerSubmitButton").click();
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $("#error_email").texts().contains("Podany email już istnieje"));
    }

    @Test
    public void admin_searchFunction() throws Exception {
        resetPasswordRepository.deleteAll();
        userRepository.deleteAll();
        final Iterator<Specialization> allSpec = specializationRepository.findAll().iterator();
        Specialization spec1 = allSpec.next();
        Specialization spec2 = allSpec.next();
        Specialization spec3 = allSpec.next();

        LocalDate created1 = LocalDate.of(2018, 5, 10);
        LocalDate created2 = LocalDate.of(2018, 5, 15);
        LocalDate created3 = LocalDate.of(2018, 5, 20);

        final User adminUser = userRepository.save(TestUtil.createUser().role(Role.ADMIN).build());
        //different creation date
        final User user1 = userRepository.save(TestUtil.createUser().status(UserStatus.ARCHIWALNY).specialization(spec1).created(created1).build());
        final User user2 = userRepository.save(TestUtil.createUser().status(UserStatus.ARCHIWALNY).specialization(spec2).created(created2).build());
        final User user3 = userRepository.save(TestUtil.createUser().status(UserStatus.ARCHIWALNY).specialization(spec2).created(created3).build());
        //different status
        final User user4 = userRepository.save(TestUtil.createUser().status(UserStatus.ZAREJESTROWANY).specialization(spec2).created(created3).build());
        //different spec
        final User user5 = userRepository.save(TestUtil.createUser().status(UserStatus.ARCHIWALNY).specialization(spec3).created(created3).build());

        login(adminUser.getEmail(), "eightLetter8");
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $("#students-link").present());
        $("#students-link").click();
        await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $("#filter-panel").texts().contains("Filtry wyszukujące"));
        //check if default not showing Archive users
        await().atMost(5, TimeUnit.SECONDS).untilPredicate((fl) -> $(".user-row").size() == 1);

        $(".specialization-option", with("value").equalTo(spec1.getId().toString())).click();
        $(".specialization-option", with("value").equalTo(spec2.getId().toString())).click();
        $(".status-option", with("value").equalTo("ARCHIWALNY")).click();
        final FluentWebElement dateFrom = $("#register_date_search_from").single();
        setAttribute(dateFrom.getElement(), "value", "13/05/2018");
        final FluentWebElement dateTo = $("#register_date_search_from").single();
        setAttribute(dateTo.getElement(), "value", "22/05/2018");
        $("#searchButton").click();
        // TODO: 14/05/2018 make this working
      //  await().atMost(3, TimeUnit.SECONDS).untilPredicate((fl) -> $(".user-row").size() == 2);

    }


    public void setAttribute(WebElement element, String attName, String attValue) {

        executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",
                element, attName, attValue);
    }

//    @Test
//    public void testSearchFunction_SearchEnumsDatesSpecialization() throws Exception {
//        resetPasswordRepository.deleteAll();
//        userRepository.deleteAll();
//        final Iterator<Specialization> allSpec = specializationRepository.findAll().iterator();
//        Specialization spec1 = allSpec.next();
//        Specialization spec2 = allSpec.next();
//        Specialization spec3 = allSpec.next();
//
//        LocalDate created1 = LocalDate.of(2018, 5, 10);
//        LocalDate created2 = LocalDate.of(2018, 5, 15);
//        LocalDate created3 = LocalDate.of(2018, 5, 20);
//
//        final User adminUser = userRepository.save(TestUtil.createUser().role(Role.ADMIN).build());
//        //different creation date
//        final User user1 = userRepository.save(TestUtil.createUser().status(UserStatus.ARCHIWALNY).specialization(spec1).created(created1).build());
//        final User user2 = userRepository.save(TestUtil.createUser().status(UserStatus.ARCHIWALNY).specialization(spec2).created(created2).build());
//        final User user3 = userRepository.save(TestUtil.createUser().status(UserStatus.ARCHIWALNY).specialization(spec2).created(created3).build());
//        //different status
//        final User user4 = userRepository.save(TestUtil.createUser().status(UserStatus.ZAREJESTROWANY).specialization(spec2).created(created3).build());
//        //different spec
//        final User user5 = userRepository.save(TestUtil.createUser().status(UserStatus.ARCHIWALNY).specialization(spec3).created(created3).build());
//
//        MvcResult searchByStatus = invokeSearch(Arrays.asList(
//                new SearchCriteria("status", "enum_UserStatus", "=", "ARCHIWALNY")
//        ), adminUser);
//
//        assertEquals(objectMapper.writeValueAsString(new User[]{user1, user2, user3, user5}), searchByStatus.getResponse().getContentAsString());
//
//
//        MvcResult searchByStatusAndSpec = invokeSearch(Arrays.asList(
//                new SearchCriteria("status", "enum_UserStatus", "=", "ARCHIWALNY")
//                , new SearchCriteria("specialization.id", "long", "in", Arrays.asList(spec1.getId(), spec2.getId()))
//        ), adminUser);
//
//        assertEquals(objectMapper.writeValueAsString(new User[]{user1, user2, user3}), searchByStatusAndSpec.getResponse().getContentAsString());
//        MvcResult searchByStatusAndSpecAndDates = invokeSearch(Arrays.asList(
//                new SearchCriteria("status", "enum_UserStatus", "=", "ARCHIWALNY")
//                , new SearchCriteria("specialization.id", "long", "in", Arrays.asList(spec1.getId(), spec2.getId()))
//                , new SearchCriteria("created", "date", ">", "2018-05-13T22:00:00.000Z")
//                , new SearchCriteria("created", "date", "<", "2018-05-22T22:00:00.000Z")
//        ), adminUser);
//
//        assertEquals("Searching by dates didn't return expected result", objectMapper.writeValueAsString(new User[]{user2, user3}), searchByStatusAndSpecAndDates.getResponse().getContentAsString());
//    }

    @Override
    protected void failed(Throwable e, Class<?> testClass, String testName) {
        super.failed(e, testClass, testName);
        try {
            String testNameNormalized = testName
                    .replace('(', '_')
                    .replace(')', '_');

            final String screenShoot = File.createTempFile("selenium_" + testNameNormalized, ".png").getAbsolutePath();
            final String html = File.createTempFile("selenium_" + testNameNormalized, ".html").getAbsolutePath();
            log.info("Screenshoot taken at:\nopen {}", screenShoot);
            log.info("Html dump taken at:\nopen {}", html);
            takeScreenShot(screenShoot);
            takeHtmlDump(html);
        } catch (IOException ex) {
            log.error("Unable to take screenshot and html dump", ex);
        }
    }


}
