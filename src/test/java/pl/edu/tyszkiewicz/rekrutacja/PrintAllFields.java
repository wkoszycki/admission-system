package pl.edu.tyszkiewicz.rekrutacja;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import pl.edu.tyszkiewicz.rekrutacja.domain.*;

import java.lang.reflect.Field;

@Slf4j
@RunWith(JUnit4.class)
public class PrintAllFields {


    @Test
    public void printAllFields() throws Exception {
        printFields(User.class);
//        printFields(Survey.class);
//        printFields(Document.class);
//        printFields(Address.class);
//        printFields(UserExtraInfo.class);
//        printFields(HighSchoolInfo.class);

    }
    void printFields(Class clazz){
        log.info("--------------- {} --------------------", clazz.getName());
        for (Field field : clazz.getDeclaredFields()) {
            System.out.println(field.getName());
        }
    }
}
